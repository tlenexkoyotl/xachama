const Xachama = require('../../src/Xachama');
let dir = 'data/Base';
const xachama = new Xachama(dir);

suite('Xachama tests 2', () => {
    suiteSetup(() => {
        let oldPath = xachama.getDir();
        let newPath = `${oldPath.replace('data/Base', 'test/data/Combinations')}`;

        xachama.makeDir(newPath);
        xachama.setDir(newPath);
        xachama.copyDir(oldPath, newPath);
    });

    suite('Form syllable combination sets', () => {
        test('Case all letters syllable combinations', function () {
            this.timeout(300000);
            this.slow(300000);
            xachama.formAllCharacterSets(false);
        });
    });

    suiteTeardown(() => {
        xachama.setDir('/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/');
        xachama.removeDir()
    });
});
