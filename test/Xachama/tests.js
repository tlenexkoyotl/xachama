const assert = require('chai').assert;
const Xachama = require('../../src/Xachama');
let dir = 'data/Base';
const xachama = new Xachama(dir);

suite('Xachama tests', () => {
    suiteSetup(() => {
        let oldPath = xachama.getDir();
        let newPath = `${oldPath.replace('data/Base', 'test/data/Base')}`;

        xachama.makeDir(newPath);
        xachama.setDir(newPath);
        xachama.copyDir(oldPath, newPath);

        oldPath = xachama.getDir();
        newPath = `${oldPath.replace('Base', '')}results`;

        xachama.makeDir(newPath);
        xachama.setDir(newPath);
        xachama.copyDir(oldPath, newPath);
    });

    suite('Find whether char is vowel', () => {
        test('Case is vowel', () => {
            for (const char of xachama.types.all.slice(0, 16))
                assert.isTrue(xachama.isVowel(char));

            for (const char of xachama.types.all.slice(89, 93))
                assert.isTrue(xachama.isVowel(char));
        });

        test('Case is not vowel', () => {
            for (const char of xachama.types.all.slice(16, 89))
                assert.isFalse(xachama.isVowel(char));

            for (const char of xachama.types.all.slice(93))
                assert.isFalse(xachama.isVowel(char));
        });
    });

    suite('Find whether char is consonant', () => {
        test('Case is consonant', () => {
            for (const char of xachama.types.all.slice(19, 64))
                assert.isTrue(xachama.isConsonant(char));

            for (const char of xachama.types.all.slice(65, 89))
                assert.isTrue(xachama.isConsonant(char));

            for (const char of xachama.types.all.slice(93))
                assert.isTrue(xachama.isConsonant(char));
        });

        test('Case is not consonant', () => {
            for (const char of xachama.types.all.slice(0, 16))
                assert.isFalse(xachama.isConsonant(char));

            assert.isFalse(xachama.isConsonant(xachama.types.all.slice(64, 65)[0]));

            for (const char of xachama.types.all.slice(89, 93))
                assert.isFalse(xachama.isConsonant(char));
        });
    });

    suite('Form combination', () => {
        test('Case combination formed correctly', () => {
            const expected = {
                name: 'tl_e_n',
                components: [
                    'tl',
                    '_e',
                    '_n'
                ]
            };
            const result = xachama.formCombination('tl_e_n', ['tl', '_e', '_n']);
            result.components = result.components.map(component => component.replace('.svg', ''));

            assert.deepEqual(result, expected);
        });
    });

    suite('Form large standalone vowel sets', () => {
        test('Case elaqiloli syllable combinations', () => {
            const expected = [{name: 'E\'', components: ['E', '\'']},
                {name: 'E!', components: ['E', '!']},
                {name: 'E!\'', components: ['E', '!', '\'']}];
            const results = xachama.formLargeStandaloneVowelSets('E');

            assert.strictEqual(results.length, expected.length);

            for (const x in results) {
                assert.strictEqual(results[x].name, expected[x].name);

                for (const y in results[x].components)
                    assert.strictEqual(results[x].components[y].replace('.svg', ''), expected[x].components[y]);
            }
        });
    });

    suite('Form upper standalone vowel sets', () => {
        test('Case elaqiloli syllable combinations', () => {
            const expected = [{"name": "E__m", "components": ["E_.svg", "_m.svg"]}, {
                "name": "E__!_m",
                "components": ["E_.svg", "_!.svg", "_m.svg"]
            }, {"name": "E__n", "components": ["E_.svg", "_n.svg"]}, {
                "name": "E__!_n",
                "components": ["E_.svg", "_!.svg", "_n.svg"]
            }, {"name": "E__p", "components": ["E_.svg", "_p.svg"]}, {
                "name": "E__!_p",
                "components": ["E_.svg", "_!.svg", "_p.svg"]
            }, {"name": "E__t", "components": ["E_.svg", "_t.svg"]}, {
                "name": "E__!_t",
                "components": ["E_.svg", "_!.svg", "_t.svg"]
            }, {"name": "E__tl", "components": ["E_.svg", "_tl.svg"]}, {
                "name": "E__!_tl",
                "components": ["E_.svg", "_!.svg", "_tl.svg"]
            }, {"name": "E__x", "components": ["E_.svg", "_x.svg"]}, {
                "name": "E__!_x",
                "components": ["E_.svg", "_!.svg", "_x.svg"]
            }, {"name": "E__c", "components": ["E_.svg", "_c.svg"]}, {
                "name": "E__!_c",
                "components": ["E_.svg", "_!.svg", "_c.svg"]
            }, {"name": "E__s", "components": ["E_.svg", "_s.svg"]}, {
                "name": "E__!_s",
                "components": ["E_.svg", "_!.svg", "_s.svg"]
            }, {"name": "E__z", "components": ["E_.svg", "_z.svg"]}, {
                "name": "E__!_z",
                "components": ["E_.svg", "_!.svg", "_z.svg"]
            }, {"name": "E__k", "components": ["E_.svg", "_k.svg"]}, {
                "name": "E__!_k",
                "components": ["E_.svg", "_!.svg", "_k.svg"]
            }, {"name": "E__q", "components": ["E_.svg", "_q.svg"]}, {
                "name": "E__!_q",
                "components": ["E_.svg", "_!.svg", "_q.svg"]
            }, {"name": "E__h", "components": ["E_.svg", "_h.svg"]}, {
                "name": "E__!_h",
                "components": ["E_.svg", "_!.svg", "_h.svg"]
            }, {"name": "E__l", "components": ["E_.svg", "_l.svg"]}, {
                "name": "E__!_l",
                "components": ["E_.svg", "_!.svg", "_l.svg"]
            }, {"name": "E__w", "components": ["E_.svg", "_w.svg"]}, {
                "name": "E__!_w",
                "components": ["E_.svg", "_!.svg", "_w.svg"]
            }, {"name": "E__y", "components": ["E_.svg", "_y.svg"]}, {
                "name": "E__!_y",
                "components": ["E_.svg", "_!.svg", "_y.svg"]
            }];
            const results = xachama.formUpperStandaloneVowelSets('E');

            assert.strictEqual(results.length, expected.length);

            for (const x in results) {
                assert.strictEqual(results[x].name, expected[x].name);

                for (const y in results[x].components)
                    assert.strictEqual(results[x].components[y], expected[x].components[y]);
            }
        });
    });

    suite('Form upper standalone vowel sets with foreign consonants', () => {
        test('Case elaqiloli syllable combinations', () => {
            const expected = [{"name": "E__b", "components": ["E_.svg", "_b.svg"]}, {
                "name": "E__!_b",
                "components": ["E_.svg", "_!.svg", "_b.svg"]
            }, {"name": "E__d", "components": ["E_.svg", "_d.svg"]}, {
                "name": "E__!_d",
                "components": ["E_.svg", "_!.svg", "_d.svg"]
            }, {"name": "E__f", "components": ["E_.svg", "_f.svg"]}, {
                "name": "E__!_f",
                "components": ["E_.svg", "_!.svg", "_f.svg"]
            }, {"name": "E__g", "components": ["E_.svg", "_g.svg"]}, {
                "name": "E__!_g",
                "components": ["E_.svg", "_!.svg", "_g.svg"]
            }, {"name": "E__kh", "components": ["E_.svg", "_kh.svg"]}, {
                "name": "E__!_kh",
                "components": ["E_.svg", "_!.svg", "_kh.svg"]
            }, {"name": "E__j", "components": ["E_.svg", "_j.svg"]}, {
                "name": "E__!_j",
                "components": ["E_.svg", "_!.svg", "_j.svg"]
            }, {"name": "E__r", "components": ["E_.svg", "_r.svg"]}, {
                "name": "E__!_r",
                "components": ["E_.svg", "_!.svg", "_r.svg"]
            }, {"name": "E__z'", "components": ["E_.svg", "_z'.svg"]}, {
                "name": "E__!_z'",
                "components": ["E_.svg", "_!.svg", "_z'.svg"]
            }, {"name": "E__v", "components": ["E_.svg", "_v.svg"]}, {
                "name": "E__!_v",
                "components": ["E_.svg", "_!.svg", "_v.svg"]
            }, {"name": "E__ks", "components": ["E_.svg", "_ks.svg"]}, {
                "name": "E__!_ks",
                "components": ["E_.svg", "_!.svg", "_ks.svg"]
            }, {"name": "E__ny", "components": ["E_.svg", "_ny.svg"]}, {
                "name": "E__!_ny",
                "components": ["E_.svg", "_!.svg", "_ny.svg"]
            }];
            const results = xachama.formUpperStandaloneVowelSetsWithForeignConsonants('E');

            assert.strictEqual(results.length, expected.length);

            for (const x in results) {
                assert.strictEqual(results[x].name, expected[x].name);

                for (const y in results[x].components)
                    assert.strictEqual(results[x].components[y], expected[x].components[y]);
            }
        });
    });

    suite('Form vowel combination sets', () => {
        test('Case akalqiloli syllable combinations', () => {
            const expected = [{"name": "e'", "components": ["e.svg", "'4.svg"]}, {
                "name": "e!",
                "components": ["e.svg", "!3.svg"]
            }, {"name": "e!'", "components": ["e.svg", "!3.svg", "'4.svg"]}, {
                "name": "e__m",
                "components": ["e_.svg", "_m.svg"]
            }, {"name": "e__!_m", "components": ["e_.svg", "_!3.svg", "_m.svg"]}, {
                "name": "e__n",
                "components": ["e_.svg", "_n.svg"]
            }, {"name": "e__!_n", "components": ["e_.svg", "_!3.svg", "_n.svg"]}, {
                "name": "e__p",
                "components": ["e_.svg", "_p.svg"]
            }, {"name": "e__!_p", "components": ["e_.svg", "_!3.svg", "_p.svg"]}, {
                "name": "e__t",
                "components": ["e_.svg", "_t.svg"]
            }, {"name": "e__!_t", "components": ["e_.svg", "_!3.svg", "_t.svg"]}, {
                "name": "e__tl",
                "components": ["e_.svg", "_tl.svg"]
            }, {"name": "e__!_tl", "components": ["e_.svg", "_!3.svg", "_tl.svg"]}, {
                "name": "e__x",
                "components": ["e_.svg", "_x.svg"]
            }, {"name": "e__!_x", "components": ["e_.svg", "_!3.svg", "_x.svg"]}, {
                "name": "e__c",
                "components": ["e_.svg", "_c.svg"]
            }, {"name": "e__!_c", "components": ["e_.svg", "_!3.svg", "_c.svg"]}, {
                "name": "e__s",
                "components": ["e_.svg", "_s.svg"]
            }, {"name": "e__!_s", "components": ["e_.svg", "_!3.svg", "_s.svg"]}, {
                "name": "e__z",
                "components": ["e_.svg", "_z.svg"]
            }, {"name": "e__!_z", "components": ["e_.svg", "_!3.svg", "_z.svg"]}, {
                "name": "e__k",
                "components": ["e_.svg", "_k.svg"]
            }, {"name": "e__!_k", "components": ["e_.svg", "_!3.svg", "_k.svg"]}, {
                "name": "e__q",
                "components": ["e_.svg", "_q.svg"]
            }, {"name": "e__!_q", "components": ["e_.svg", "_!3.svg", "_q.svg"]}, {
                "name": "e__h",
                "components": ["e_.svg", "_h.svg"]
            }, {"name": "e__!_h", "components": ["e_.svg", "_!3.svg", "_h.svg"]}, {
                "name": "e__l",
                "components": ["e_.svg", "_l.svg"]
            }, {"name": "e__!_l", "components": ["e_.svg", "_!3.svg", "_l.svg"]}, {
                "name": "e__w",
                "components": ["e_.svg", "_w.svg"]
            }, {"name": "e__!_w", "components": ["e_.svg", "_!3.svg", "_w.svg"]}, {
                "name": "e__y",
                "components": ["e_.svg", "_y.svg"]
            }, {"name": "e__!_y", "components": ["e_.svg", "_!3.svg", "_y.svg"]}, {
                "name": "e__b",
                "components": ["e_.svg", "_b.svg"]
            }, {"name": "e__!_b", "components": ["e_.svg", "_!3.svg", "_b.svg"]}, {
                "name": "e__d",
                "components": ["e_.svg", "_d.svg"]
            }, {"name": "e__!_d", "components": ["e_.svg", "_!3.svg", "_d.svg"]}, {
                "name": "e__f",
                "components": ["e_.svg", "_f.svg"]
            }, {"name": "e__!_f", "components": ["e_.svg", "_!3.svg", "_f.svg"]}, {
                "name": "e__g",
                "components": ["e_.svg", "_g.svg"]
            }, {"name": "e__!_g", "components": ["e_.svg", "_!3.svg", "_g.svg"]}, {
                "name": "e__kh",
                "components": ["e_.svg", "_kh.svg"]
            }, {"name": "e__!_kh", "components": ["e_.svg", "_!3.svg", "_kh.svg"]}, {
                "name": "e__j",
                "components": ["e_.svg", "_j.svg"]
            }, {"name": "e__!_j", "components": ["e_.svg", "_!3.svg", "_j.svg"]}, {
                "name": "e__r",
                "components": ["e_.svg", "_r.svg"]
            }, {"name": "e__!_r", "components": ["e_.svg", "_!3.svg", "_r.svg"]}, {
                "name": "e__z'",
                "components": ["e_.svg", "_z'.svg"]
            }, {"name": "e__!_z'", "components": ["e_.svg", "_!3.svg", "_z'.svg"]}, {
                "name": "e__v",
                "components": ["e_.svg", "_v.svg"]
            }, {"name": "e__!_v", "components": ["e_.svg", "_!3.svg", "_v.svg"]}, {
                "name": "e__ks",
                "components": ["e_.svg", "_ks.svg"]
            }, {"name": "e__!_ks", "components": ["e_.svg", "_!3.svg", "_ks.svg"]}, {
                "name": "e__ny",
                "components": ["e_.svg", "_ny.svg"]
            }, {"name": "e__!_ny", "components": ["e_.svg", "_!3.svg", "_ny.svg"]}];
            const results = xachama.formVowelSets('E');

            assert.strictEqual(results.length, expected.length);

            for (const x in results) {
                assert.strictEqual(results[x].name, expected[x].name);

                for (const y in results[x].components)
                    assert.strictEqual(results[x].components[y], expected[x].components[y]);
            }
        });
    });

    suite('Form large standalone consonant sets', () => {
        test('Case tekolqiloli syllable combinations', () => {
            const expected = [{"name": "t_a", "components": ["t.svg", "_a.svg"]}, {
                "name": "t_a'",
                "components": ["t.svg", "_a.svg", "'.svg"]
            }, {"name": "t_a!", "components": ["t.svg", "_a.svg", "!.svg"]}, {
                "name": "t_a!'",
                "components": ["t.svg", "_a.svg", "!.svg", "'.svg"]
            }];
            const results = xachama.formLargeStandaloneConsonantSets('T', '_A');

            assert.strictEqual(results.length, expected.length);

            for (const x in results) {
                assert.strictEqual(results[x].name, expected[x].name);

                for (const y in results[x].components)
                    assert.strictEqual(results[x].components[y], expected[x].components[y]);
            }
        });
    });

    suite('Form upper standalone consonant sets', () => {
        test('Case tekolqiloli syllable combinations', () => {
            const expected = [
                {name: 't__a_m', components: ['t_', '_a', '_m']},
                {name: 't__a_!_m', components: ['t_', '_a', '_!', '_m']},
                {name: 't__a_n', components: ['t_', '_a', '_n']},
                {name: 't__a_!_n', components: ['t_', '_a', '_!', '_n']},
                {name: 't__a_p', components: ['t_', '_a', '_p']},
                {name: 't__a_!_p', components: ['t_', '_a', '_!', '_p']},
                {name: 't__a_t', components: ['t_', '_a', '_t']},
                {name: 't__a_!_t', components: ['t_', '_a', '_!', '_t']},
                {name: 't__a_tl', components: ['t_', '_a', '_tl']},
                {name: 't__a_!_tl', components: ['t_', '_a', '_!', '_tl']},
                {name: 't__a_x', components: ['t_', '_a', '_x']},
                {name: 't__a_!_x', components: ['t_', '_a', '_!', '_x']},
                {name: 't__a_c', components: ['t_', '_a', '_c']},
                {name: 't__a_!_c', components: ['t_', '_a', '_!', '_c']},
                {name: 't__a_s', components: ['t_', '_a', '_s']},
                {name: 't__a_!_s', components: ['t_', '_a', '_!', '_s']},
                {name: 't__a_z', components: ['t_', '_a', '_z']},
                {name: 't__a_!_z', components: ['t_', '_a', '_!', '_z']},
                {name: 't__a_k', components: ['t_', '_a', '_k']},
                {name: 't__a_!_k', components: ['t_', '_a', '_!', '_k']},
                {name: 't__a_q', components: ['t_', '_a', '_q']},
                {name: 't__a_!_q', components: ['t_', '_a', '_!', '_q']},
                {name: 't__a_h', components: ['t_', '_a', '_h']},
                {name: 't__a_!_h', components: ['t_', '_a', '_!', '_h']},
                {name: 't__a_l', components: ['t_', '_a', '_l']},
                {name: 't__a_!_l', components: ['t_', '_a', '_!', '_l']},
                {name: 't__a_w', components: ['t_', '_a', '_w']},
                {name: 't__a_!_w', components: ['t_', '_a', '_!', '_w']},
                {name: 't__a_y', components: ['t_', '_a', '_y']},
                {name: 't__a_!_y', components: ['t_', '_a', '_!', '_y']}
            ];
            const results = xachama.formUpperStandaloneConsonantSets('t_', '_a');

            assert.strictEqual(results.length, expected.length);

            for (const x in results) {
                assert.strictEqual(results[x].name, expected[x].name);

                for (const y in results[x].components)
                    assert.strictEqual(results[x].components[y].replace('.svg', ''), expected[x].components[y]);
            }
        });
    });

    suite('Form upper standalone consonant sets with foreign consonants', () => {
        test('Case tekolqiloli syllable combinations', () => {
            const expected = [
                {name: 't__a_b', components: ['t_', '_a', '_b']},
                {name: 't__a_!_b', components: ['t_', '_a', '_!', '_b']},
                {name: 't__a_d', components: ['t_', '_a', '_d']},
                {name: 't__a_!_d', components: ['t_', '_a', '_!', '_d']},
                {name: 't__a_f', components: ['t_', '_a', '_f']},
                {name: 't__a_!_f', components: ['t_', '_a', '_!', '_f']},
                {name: 't__a_g', components: ['t_', '_a', '_g']},
                {name: 't__a_!_g', components: ['t_', '_a', '_!', '_g']},
                {name: 't__a_kh', components: ['t_', '_a', '_kh']},
                {name: 't__a_!_kh', components: ['t_', '_a', '_!', '_kh']},
                {name: 't__a_j', components: ['t_', '_a', '_j']},
                {name: 't__a_!_j', components: ['t_', '_a', '_!', '_j']},
                {name: 't__a_r', components: ['t_', '_a', '_r']},
                {name: 't__a_!_r', components: ['t_', '_a', '_!', '_r']},
                {name: 't__a_z\'', components: ['t_', '_a', '_z\'']},
                {name: 't__a_!_z\'', components: ['t_', '_a', '_!', '_z\'']},
                {name: 't__a_v', components: ['t_', '_a', '_v']},
                {name: 't__a_!_v', components: ['t_', '_a', '_!', '_v']},
                {name: 't__a_ks', components: ['t_', '_a', '_ks']},
                {name: 't__a_!_ks', components: ['t_', '_a', '_!', '_ks']},
                {name: 't__a_ny', components: ['t_', '_a', '_ny']},
                {name: 't__a_!_ny', components: ['t_', '_a', '_!', '_ny']}
            ];
            const results = xachama.formUpperStandaloneConsonantSetsWithForeignConsonants('t_', '_a');

            assert.strictEqual(results.length, expected.length);

            for (const x in results) {
                assert.strictEqual(results[x].name, expected[x].name);

                for (const y in results[x].components)
                    assert.strictEqual(results[x].components[y].replace('.svg', ''), expected[x].components[y]);
            }
        });
    });

    suite('Form consonant combination sets', () => {
        test('Case tekolqiloli syllable combinations for given vowel A', () => {
            const expected = [{"name": "t_a", "components": ["t.svg", "_a.svg"]}, {
                "name": "t_a'",
                "components": ["t.svg", "_a.svg", "'5a.svg"]
            }, {"name": "t_a!", "components": ["t.svg", "_a.svg", "!3.svg"]}, {
                "name": "t_a!'",
                "components": ["t.svg", "_a.svg", "!3.svg", "'5a.svg"]
            }, {"name": "t__a__m", "components": ["t_.svg", "_a_.svg", "_m.svg"]}, {
                "name": "t__a__!_m",
                "components": ["t_.svg", "_a_.svg", "_!3.svg", "_m.svg"]
            }, {"name": "t__a__n", "components": ["t_.svg", "_a_.svg", "_n.svg"]}, {
                "name": "t__a__!_n",
                "components": ["t_.svg", "_a_.svg", "_!3.svg", "_n.svg"]
            }, {"name": "t__a__p", "components": ["t_.svg", "_a_.svg", "_p.svg"]}, {
                "name": "t__a__!_p",
                "components": ["t_.svg", "_a_.svg", "_!3.svg", "_p.svg"]
            }, {"name": "t__a__t", "components": ["t_.svg", "_a_.svg", "_t.svg"]}, {
                "name": "t__a__!_t",
                "components": ["t_.svg", "_a_.svg", "_!3.svg", "_t.svg"]
            }, {"name": "t__a__tl", "components": ["t_.svg", "_a_.svg", "_tl.svg"]}, {
                "name": "t__a__!_tl",
                "components": ["t_.svg", "_a_.svg", "_!3.svg", "_tl.svg"]
            }, {"name": "t__a__x", "components": ["t_.svg", "_a_.svg", "_x.svg"]}, {
                "name": "t__a__!_x",
                "components": ["t_.svg", "_a_.svg", "_!3.svg", "_x.svg"]
            }, {"name": "t__a__c", "components": ["t_.svg", "_a_.svg", "_c.svg"]}, {
                "name": "t__a__!_c",
                "components": ["t_.svg", "_a_.svg", "_!3.svg", "_c.svg"]
            }, {"name": "t__a__s", "components": ["t_.svg", "_a_.svg", "_s.svg"]}, {
                "name": "t__a__!_s",
                "components": ["t_.svg", "_a_.svg", "_!3.svg", "_s.svg"]
            }, {"name": "t__a__z", "components": ["t_.svg", "_a_.svg", "_z.svg"]}, {
                "name": "t__a__!_z",
                "components": ["t_.svg", "_a_.svg", "_!3.svg", "_z.svg"]
            }, {"name": "t__a__k", "components": ["t_.svg", "_a_.svg", "_k.svg"]}, {
                "name": "t__a__!_k",
                "components": ["t_.svg", "_a_.svg", "_!3.svg", "_k.svg"]
            }, {"name": "t__a__q", "components": ["t_.svg", "_a_.svg", "_q.svg"]}, {
                "name": "t__a__!_q",
                "components": ["t_.svg", "_a_.svg", "_!3.svg", "_q.svg"]
            }, {"name": "t__a__h", "components": ["t_.svg", "_a_.svg", "_h.svg"]}, {
                "name": "t__a__!_h",
                "components": ["t_.svg", "_a_.svg", "_!3.svg", "_h.svg"]
            }, {"name": "t__a__l", "components": ["t_.svg", "_a_.svg", "_l.svg"]}, {
                "name": "t__a__!_l",
                "components": ["t_.svg", "_a_.svg", "_!3.svg", "_l.svg"]
            }, {"name": "t__a__w", "components": ["t_.svg", "_a_.svg", "_w.svg"]}, {
                "name": "t__a__!_w",
                "components": ["t_.svg", "_a_.svg", "_!3.svg", "_w.svg"]
            }, {"name": "t__a__y", "components": ["t_.svg", "_a_.svg", "_y.svg"]}, {
                "name": "t__a__!_y",
                "components": ["t_.svg", "_a_.svg", "_!3.svg", "_y.svg"]
            }, {"name": "t__a__b", "components": ["t_.svg", "_a_.svg", "_b.svg"]}, {
                "name": "t__a__!_b",
                "components": ["t_.svg", "_a_.svg", "_!3.svg", "_b.svg"]
            }, {"name": "t__a__d", "components": ["t_.svg", "_a_.svg", "_d.svg"]}, {
                "name": "t__a__!_d",
                "components": ["t_.svg", "_a_.svg", "_!3.svg", "_d.svg"]
            }, {"name": "t__a__f", "components": ["t_.svg", "_a_.svg", "_f.svg"]}, {
                "name": "t__a__!_f",
                "components": ["t_.svg", "_a_.svg", "_!3.svg", "_f.svg"]
            }, {"name": "t__a__g", "components": ["t_.svg", "_a_.svg", "_g.svg"]}, {
                "name": "t__a__!_g",
                "components": ["t_.svg", "_a_.svg", "_!3.svg", "_g.svg"]
            }, {"name": "t__a__kh", "components": ["t_.svg", "_a_.svg", "_kh.svg"]}, {
                "name": "t__a__!_kh",
                "components": ["t_.svg", "_a_.svg", "_!3.svg", "_kh.svg"]
            }, {"name": "t__a__j", "components": ["t_.svg", "_a_.svg", "_j.svg"]}, {
                "name": "t__a__!_j",
                "components": ["t_.svg", "_a_.svg", "_!3.svg", "_j.svg"]
            }, {"name": "t__a__r", "components": ["t_.svg", "_a_.svg", "_r.svg"]}, {
                "name": "t__a__!_r",
                "components": ["t_.svg", "_a_.svg", "_!3.svg", "_r.svg"]
            }, {"name": "t__a__z'", "components": ["t_.svg", "_a_.svg", "_z'.svg"]}, {
                "name": "t__a__!_z'",
                "components": ["t_.svg", "_a_.svg", "_!3.svg", "_z'.svg"]
            }, {"name": "t__a__v", "components": ["t_.svg", "_a_.svg", "_v.svg"]}, {
                "name": "t__a__!_v",
                "components": ["t_.svg", "_a_.svg", "_!3.svg", "_v.svg"]
            }, {"name": "t__a__ks", "components": ["t_.svg", "_a_.svg", "_ks.svg"]}, {
                "name": "t__a__!_ks",
                "components": ["t_.svg", "_a_.svg", "_!3.svg", "_ks.svg"]
            }, {"name": "t__a__ny", "components": ["t_.svg", "_a_.svg", "_ny.svg"]}, {
                "name": "t__a__!_ny",
                "components": ["t_.svg", "_a_.svg", "_!3.svg", "_ny.svg"]
            }];
            const results = xachama.formConsonantSets('t', 'a');

            assert.strictEqual(results.length, expected.length);

            for (const x in results) {
                assert.strictEqual(results[x].name, expected[x].name);

                for (const y in results[x].components)
                    assert.strictEqual(results[x].components[y], expected[x].components[y]);
            }
        });

        test('Case tekolqiloli syllable combinations', () => {
            const expected = [{
                "directory": "001 Akaqilotontli",
                "combinations": [{"name": "t_a", "components": ["t.svg", "_a.svg"]}, {
                    "name": "t_a'",
                    "components": ["t.svg", "_a.svg", "'5a.svg"]
                }, {"name": "t_a!", "components": ["t.svg", "_a.svg", "!3.svg"]}, {
                    "name": "t_a!'",
                    "components": ["t.svg", "_a.svg", "!3.svg", "'5a.svg"]
                }, {"name": "t__a__m", "components": ["t_.svg", "_a_.svg", "_m.svg"]}, {
                    "name": "t__a__!_m",
                    "components": ["t_.svg", "_a_.svg", "_!3.svg", "_m.svg"]
                }, {"name": "t__a__n", "components": ["t_.svg", "_a_.svg", "_n.svg"]}, {
                    "name": "t__a__!_n",
                    "components": ["t_.svg", "_a_.svg", "_!3.svg", "_n.svg"]
                }, {"name": "t__a__p", "components": ["t_.svg", "_a_.svg", "_p.svg"]}, {
                    "name": "t__a__!_p",
                    "components": ["t_.svg", "_a_.svg", "_!3.svg", "_p.svg"]
                }, {"name": "t__a__t", "components": ["t_.svg", "_a_.svg", "_t.svg"]}, {
                    "name": "t__a__!_t",
                    "components": ["t_.svg", "_a_.svg", "_!3.svg", "_t.svg"]
                }, {"name": "t__a__tl", "components": ["t_.svg", "_a_.svg", "_tl.svg"]}, {
                    "name": "t__a__!_tl",
                    "components": ["t_.svg", "_a_.svg", "_!3.svg", "_tl.svg"]
                }, {"name": "t__a__x", "components": ["t_.svg", "_a_.svg", "_x.svg"]}, {
                    "name": "t__a__!_x",
                    "components": ["t_.svg", "_a_.svg", "_!3.svg", "_x.svg"]
                }, {"name": "t__a__c", "components": ["t_.svg", "_a_.svg", "_c.svg"]}, {
                    "name": "t__a__!_c",
                    "components": ["t_.svg", "_a_.svg", "_!3.svg", "_c.svg"]
                }, {"name": "t__a__s", "components": ["t_.svg", "_a_.svg", "_s.svg"]}, {
                    "name": "t__a__!_s",
                    "components": ["t_.svg", "_a_.svg", "_!3.svg", "_s.svg"]
                }, {"name": "t__a__z", "components": ["t_.svg", "_a_.svg", "_z.svg"]}, {
                    "name": "t__a__!_z",
                    "components": ["t_.svg", "_a_.svg", "_!3.svg", "_z.svg"]
                }, {"name": "t__a__k", "components": ["t_.svg", "_a_.svg", "_k.svg"]}, {
                    "name": "t__a__!_k",
                    "components": ["t_.svg", "_a_.svg", "_!3.svg", "_k.svg"]
                }, {"name": "t__a__q", "components": ["t_.svg", "_a_.svg", "_q.svg"]}, {
                    "name": "t__a__!_q",
                    "components": ["t_.svg", "_a_.svg", "_!3.svg", "_q.svg"]
                }, {"name": "t__a__h", "components": ["t_.svg", "_a_.svg", "_h.svg"]}, {
                    "name": "t__a__!_h",
                    "components": ["t_.svg", "_a_.svg", "_!3.svg", "_h.svg"]
                }, {"name": "t__a__l", "components": ["t_.svg", "_a_.svg", "_l.svg"]}, {
                    "name": "t__a__!_l",
                    "components": ["t_.svg", "_a_.svg", "_!3.svg", "_l.svg"]
                }, {"name": "t__a__w", "components": ["t_.svg", "_a_.svg", "_w.svg"]}, {
                    "name": "t__a__!_w",
                    "components": ["t_.svg", "_a_.svg", "_!3.svg", "_w.svg"]
                }, {"name": "t__a__y", "components": ["t_.svg", "_a_.svg", "_y.svg"]}, {
                    "name": "t__a__!_y",
                    "components": ["t_.svg", "_a_.svg", "_!3.svg", "_y.svg"]
                }, {"name": "t__a__b", "components": ["t_.svg", "_a_.svg", "_b.svg"]}, {
                    "name": "t__a__!_b",
                    "components": ["t_.svg", "_a_.svg", "_!3.svg", "_b.svg"]
                }, {"name": "t__a__d", "components": ["t_.svg", "_a_.svg", "_d.svg"]}, {
                    "name": "t__a__!_d",
                    "components": ["t_.svg", "_a_.svg", "_!3.svg", "_d.svg"]
                }, {"name": "t__a__f", "components": ["t_.svg", "_a_.svg", "_f.svg"]}, {
                    "name": "t__a__!_f",
                    "components": ["t_.svg", "_a_.svg", "_!3.svg", "_f.svg"]
                }, {"name": "t__a__g", "components": ["t_.svg", "_a_.svg", "_g.svg"]}, {
                    "name": "t__a__!_g",
                    "components": ["t_.svg", "_a_.svg", "_!3.svg", "_g.svg"]
                }, {"name": "t__a__kh", "components": ["t_.svg", "_a_.svg", "_kh.svg"]}, {
                    "name": "t__a__!_kh",
                    "components": ["t_.svg", "_a_.svg", "_!3.svg", "_kh.svg"]
                }, {"name": "t__a__j", "components": ["t_.svg", "_a_.svg", "_j.svg"]}, {
                    "name": "t__a__!_j",
                    "components": ["t_.svg", "_a_.svg", "_!3.svg", "_j.svg"]
                }, {"name": "t__a__r", "components": ["t_.svg", "_a_.svg", "_r.svg"]}, {
                    "name": "t__a__!_r",
                    "components": ["t_.svg", "_a_.svg", "_!3.svg", "_r.svg"]
                }, {"name": "t__a__z'", "components": ["t_.svg", "_a_.svg", "_z'.svg"]}, {
                    "name": "t__a__!_z'",
                    "components": ["t_.svg", "_a_.svg", "_!3.svg", "_z'.svg"]
                }, {"name": "t__a__v", "components": ["t_.svg", "_a_.svg", "_v.svg"]}, {
                    "name": "t__a__!_v",
                    "components": ["t_.svg", "_a_.svg", "_!3.svg", "_v.svg"]
                }, {"name": "t__a__ks", "components": ["t_.svg", "_a_.svg", "_ks.svg"]}, {
                    "name": "t__a__!_ks",
                    "components": ["t_.svg", "_a_.svg", "_!3.svg", "_ks.svg"]
                }, {"name": "t__a__ny", "components": ["t_.svg", "_a_.svg", "_ny.svg"]}, {
                    "name": "t__a__!_ny",
                    "components": ["t_.svg", "_a_.svg", "_!3.svg", "_ny.svg"]
                }]
            }, {
                "directory": "002 Otonqilotontli",
                "combinations": [{"name": "t_o", "components": ["t.svg", "_o3.svg"]}, {
                    "name": "t_o'",
                    "components": ["t.svg", "_o3.svg", "'5a.svg"]
                }, {"name": "t_o!", "components": ["t.svg", "_o3.svg", "!3.svg"]}, {
                    "name": "t_o!'",
                    "components": ["t.svg", "_o3.svg", "!3.svg", "'5a.svg"]
                }, {"name": "t__o__m", "components": ["t_.svg", "_o_4.svg", "_m.svg"]}, {
                    "name": "t__o__!_m",
                    "components": ["t_.svg", "_o_4.svg", "_!3.svg", "_m.svg"]
                }, {"name": "t__o__n", "components": ["t_.svg", "_o_4.svg", "_n.svg"]}, {
                    "name": "t__o__!_n",
                    "components": ["t_.svg", "_o_4.svg", "_!3.svg", "_n.svg"]
                }, {"name": "t__o__p", "components": ["t_.svg", "_o_4.svg", "_p.svg"]}, {
                    "name": "t__o__!_p",
                    "components": ["t_.svg", "_o_4.svg", "_!3.svg", "_p.svg"]
                }, {"name": "t__o__t", "components": ["t_.svg", "_o_4.svg", "_t.svg"]}, {
                    "name": "t__o__!_t",
                    "components": ["t_.svg", "_o_4.svg", "_!3.svg", "_t.svg"]
                }, {"name": "t__o__tl", "components": ["t_.svg", "_o_4.svg", "_tl.svg"]}, {
                    "name": "t__o__!_tl",
                    "components": ["t_.svg", "_o_4.svg", "_!3.svg", "_tl.svg"]
                }, {"name": "t__o__x", "components": ["t_.svg", "_o_4.svg", "_x.svg"]}, {
                    "name": "t__o__!_x",
                    "components": ["t_.svg", "_o_4.svg", "_!3.svg", "_x.svg"]
                }, {"name": "t__o__c", "components": ["t_.svg", "_o_4.svg", "_c.svg"]}, {
                    "name": "t__o__!_c",
                    "components": ["t_.svg", "_o_4.svg", "_!3.svg", "_c.svg"]
                }, {"name": "t__o__s", "components": ["t_.svg", "_o_4.svg", "_s.svg"]}, {
                    "name": "t__o__!_s",
                    "components": ["t_.svg", "_o_4.svg", "_!3.svg", "_s.svg"]
                }, {"name": "t__o__z", "components": ["t_.svg", "_o_4.svg", "_z.svg"]}, {
                    "name": "t__o__!_z",
                    "components": ["t_.svg", "_o_4.svg", "_!3.svg", "_z.svg"]
                }, {"name": "t__o__k", "components": ["t_.svg", "_o_4.svg", "_k.svg"]}, {
                    "name": "t__o__!_k",
                    "components": ["t_.svg", "_o_4.svg", "_!3.svg", "_k.svg"]
                }, {"name": "t__o__q", "components": ["t_.svg", "_o_4.svg", "_q.svg"]}, {
                    "name": "t__o__!_q",
                    "components": ["t_.svg", "_o_4.svg", "_!3.svg", "_q.svg"]
                }, {"name": "t__o__h", "components": ["t_.svg", "_o_4.svg", "_h.svg"]}, {
                    "name": "t__o__!_h",
                    "components": ["t_.svg", "_o_4.svg", "_!3.svg", "_h.svg"]
                }, {"name": "t__o__l", "components": ["t_.svg", "_o_4.svg", "_l.svg"]}, {
                    "name": "t__o__!_l",
                    "components": ["t_.svg", "_o_4.svg", "_!3.svg", "_l.svg"]
                }, {"name": "t__o__w", "components": ["t_.svg", "_o_4.svg", "_w.svg"]}, {
                    "name": "t__o__!_w",
                    "components": ["t_.svg", "_o_4.svg", "_!3.svg", "_w.svg"]
                }, {"name": "t__o__y", "components": ["t_.svg", "_o_4.svg", "_y.svg"]}, {
                    "name": "t__o__!_y",
                    "components": ["t_.svg", "_o_4.svg", "_!3.svg", "_y.svg"]
                }, {"name": "t__o__b", "components": ["t_.svg", "_o_4.svg", "_b.svg"]}, {
                    "name": "t__o__!_b",
                    "components": ["t_.svg", "_o_4.svg", "_!3.svg", "_b.svg"]
                }, {"name": "t__o__d", "components": ["t_.svg", "_o_4.svg", "_d.svg"]}, {
                    "name": "t__o__!_d",
                    "components": ["t_.svg", "_o_4.svg", "_!3.svg", "_d.svg"]
                }, {"name": "t__o__f", "components": ["t_.svg", "_o_4.svg", "_f.svg"]}, {
                    "name": "t__o__!_f",
                    "components": ["t_.svg", "_o_4.svg", "_!3.svg", "_f.svg"]
                }, {"name": "t__o__g", "components": ["t_.svg", "_o_4.svg", "_g.svg"]}, {
                    "name": "t__o__!_g",
                    "components": ["t_.svg", "_o_4.svg", "_!3.svg", "_g.svg"]
                }, {"name": "t__o__kh", "components": ["t_.svg", "_o_4.svg", "_kh.svg"]}, {
                    "name": "t__o__!_kh",
                    "components": ["t_.svg", "_o_4.svg", "_!3.svg", "_kh.svg"]
                }, {"name": "t__o__j", "components": ["t_.svg", "_o_4.svg", "_j.svg"]}, {
                    "name": "t__o__!_j",
                    "components": ["t_.svg", "_o_4.svg", "_!3.svg", "_j.svg"]
                }, {"name": "t__o__r", "components": ["t_.svg", "_o_4.svg", "_r.svg"]}, {
                    "name": "t__o__!_r",
                    "components": ["t_.svg", "_o_4.svg", "_!3.svg", "_r.svg"]
                }, {"name": "t__o__z'", "components": ["t_.svg", "_o_4.svg", "_z'.svg"]}, {
                    "name": "t__o__!_z'",
                    "components": ["t_.svg", "_o_4.svg", "_!3.svg", "_z'.svg"]
                }, {"name": "t__o__v", "components": ["t_.svg", "_o_4.svg", "_v.svg"]}, {
                    "name": "t__o__!_v",
                    "components": ["t_.svg", "_o_4.svg", "_!3.svg", "_v.svg"]
                }, {"name": "t__o__ks", "components": ["t_.svg", "_o_4.svg", "_ks.svg"]}, {
                    "name": "t__o__!_ks",
                    "components": ["t_.svg", "_o_4.svg", "_!3.svg", "_ks.svg"]
                }, {"name": "t__o__ny", "components": ["t_.svg", "_o_4.svg", "_ny.svg"]}, {
                    "name": "t__o__!_ny",
                    "components": ["t_.svg", "_o_4.svg", "_!3.svg", "_ny.svg"]
                }]
            }, {
                "directory": "003 Imezqilotontli",
                "combinations": [{"name": "t_i", "components": ["t.svg", "_i3.svg"]}, {
                    "name": "t_i'",
                    "components": ["t.svg", "_i3.svg", "'5a.svg"]
                }, {"name": "t_i!", "components": ["t.svg", "_i3.svg", "!3.svg"]}, {
                    "name": "t_i!'",
                    "components": ["t.svg", "_i3.svg", "!3.svg", "'5a.svg"]
                }, {"name": "t__i__m", "components": ["t_.svg", "_i_4.svg", "_m.svg"]}, {
                    "name": "t__i__!_m",
                    "components": ["t_.svg", "_i_4.svg", "_!3.svg", "_m.svg"]
                }, {"name": "t__i__n", "components": ["t_.svg", "_i_4.svg", "_n.svg"]}, {
                    "name": "t__i__!_n",
                    "components": ["t_.svg", "_i_4.svg", "_!3.svg", "_n.svg"]
                }, {"name": "t__i__p", "components": ["t_.svg", "_i_4.svg", "_p.svg"]}, {
                    "name": "t__i__!_p",
                    "components": ["t_.svg", "_i_4.svg", "_!3.svg", "_p.svg"]
                }, {"name": "t__i__t", "components": ["t_.svg", "_i_4.svg", "_t.svg"]}, {
                    "name": "t__i__!_t",
                    "components": ["t_.svg", "_i_4.svg", "_!3.svg", "_t.svg"]
                }, {"name": "t__i__tl", "components": ["t_.svg", "_i_4.svg", "_tl.svg"]}, {
                    "name": "t__i__!_tl",
                    "components": ["t_.svg", "_i_4.svg", "_!3.svg", "_tl.svg"]
                }, {"name": "t__i__x", "components": ["t_.svg", "_i_4.svg", "_x.svg"]}, {
                    "name": "t__i__!_x",
                    "components": ["t_.svg", "_i_4.svg", "_!3.svg", "_x.svg"]
                }, {"name": "t__i__c", "components": ["t_.svg", "_i_4.svg", "_c.svg"]}, {
                    "name": "t__i__!_c",
                    "components": ["t_.svg", "_i_4.svg", "_!3.svg", "_c.svg"]
                }, {"name": "t__i__s", "components": ["t_.svg", "_i_4.svg", "_s.svg"]}, {
                    "name": "t__i__!_s",
                    "components": ["t_.svg", "_i_4.svg", "_!3.svg", "_s.svg"]
                }, {"name": "t__i__z", "components": ["t_.svg", "_i_4.svg", "_z.svg"]}, {
                    "name": "t__i__!_z",
                    "components": ["t_.svg", "_i_4.svg", "_!3.svg", "_z.svg"]
                }, {"name": "t__i__k", "components": ["t_.svg", "_i_4.svg", "_k.svg"]}, {
                    "name": "t__i__!_k",
                    "components": ["t_.svg", "_i_4.svg", "_!3.svg", "_k.svg"]
                }, {"name": "t__i__q", "components": ["t_.svg", "_i_4.svg", "_q.svg"]}, {
                    "name": "t__i__!_q",
                    "components": ["t_.svg", "_i_4.svg", "_!3.svg", "_q.svg"]
                }, {"name": "t__i__h", "components": ["t_.svg", "_i_4.svg", "_h.svg"]}, {
                    "name": "t__i__!_h",
                    "components": ["t_.svg", "_i_4.svg", "_!3.svg", "_h.svg"]
                }, {"name": "t__i__l", "components": ["t_.svg", "_i_4.svg", "_l.svg"]}, {
                    "name": "t__i__!_l",
                    "components": ["t_.svg", "_i_4.svg", "_!3.svg", "_l.svg"]
                }, {"name": "t__i__w", "components": ["t_.svg", "_i_4.svg", "_w.svg"]}, {
                    "name": "t__i__!_w",
                    "components": ["t_.svg", "_i_4.svg", "_!3.svg", "_w.svg"]
                }, {"name": "t__i__y", "components": ["t_.svg", "_i_4.svg", "_y.svg"]}, {
                    "name": "t__i__!_y",
                    "components": ["t_.svg", "_i_4.svg", "_!3.svg", "_y.svg"]
                }, {"name": "t__i__b", "components": ["t_.svg", "_i_4.svg", "_b.svg"]}, {
                    "name": "t__i__!_b",
                    "components": ["t_.svg", "_i_4.svg", "_!3.svg", "_b.svg"]
                }, {"name": "t__i__d", "components": ["t_.svg", "_i_4.svg", "_d.svg"]}, {
                    "name": "t__i__!_d",
                    "components": ["t_.svg", "_i_4.svg", "_!3.svg", "_d.svg"]
                }, {"name": "t__i__f", "components": ["t_.svg", "_i_4.svg", "_f.svg"]}, {
                    "name": "t__i__!_f",
                    "components": ["t_.svg", "_i_4.svg", "_!3.svg", "_f.svg"]
                }, {"name": "t__i__g", "components": ["t_.svg", "_i_4.svg", "_g.svg"]}, {
                    "name": "t__i__!_g",
                    "components": ["t_.svg", "_i_4.svg", "_!3.svg", "_g.svg"]
                }, {"name": "t__i__kh", "components": ["t_.svg", "_i_4.svg", "_kh.svg"]}, {
                    "name": "t__i__!_kh",
                    "components": ["t_.svg", "_i_4.svg", "_!3.svg", "_kh.svg"]
                }, {"name": "t__i__j", "components": ["t_.svg", "_i_4.svg", "_j.svg"]}, {
                    "name": "t__i__!_j",
                    "components": ["t_.svg", "_i_4.svg", "_!3.svg", "_j.svg"]
                }, {"name": "t__i__r", "components": ["t_.svg", "_i_4.svg", "_r.svg"]}, {
                    "name": "t__i__!_r",
                    "components": ["t_.svg", "_i_4.svg", "_!3.svg", "_r.svg"]
                }, {"name": "t__i__z'", "components": ["t_.svg", "_i_4.svg", "_z'.svg"]}, {
                    "name": "t__i__!_z'",
                    "components": ["t_.svg", "_i_4.svg", "_!3.svg", "_z'.svg"]
                }, {"name": "t__i__v", "components": ["t_.svg", "_i_4.svg", "_v.svg"]}, {
                    "name": "t__i__!_v",
                    "components": ["t_.svg", "_i_4.svg", "_!3.svg", "_v.svg"]
                }, {"name": "t__i__ks", "components": ["t_.svg", "_i_4.svg", "_ks.svg"]}, {
                    "name": "t__i__!_ks",
                    "components": ["t_.svg", "_i_4.svg", "_!3.svg", "_ks.svg"]
                }, {"name": "t__i__ny", "components": ["t_.svg", "_i_4.svg", "_ny.svg"]}, {
                    "name": "t__i__!_ny",
                    "components": ["t_.svg", "_i_4.svg", "_!3.svg", "_ny.svg"]
                }]
            }, {
                "directory": "004 Elaqilotontli",
                "combinations": [{"name": "t_e", "components": ["t.svg", "_e3.svg"]}, {
                    "name": "t_e'",
                    "components": ["t.svg", "_e3.svg", "'5a.svg"]
                }, {"name": "t_e!", "components": ["t.svg", "_e3.svg", "!3.svg"]}, {
                    "name": "t_e!'",
                    "components": ["t.svg", "_e3.svg", "!3.svg", "'5a.svg"]
                }, {"name": "t__e__m", "components": ["t_.svg", "_e_4.svg", "_m.svg"]}, {
                    "name": "t__e__!_m",
                    "components": ["t_.svg", "_e_4.svg", "_!3.svg", "_m.svg"]
                }, {"name": "t__e__n", "components": ["t_.svg", "_e_4.svg", "_n.svg"]}, {
                    "name": "t__e__!_n",
                    "components": ["t_.svg", "_e_4.svg", "_!3.svg", "_n.svg"]
                }, {"name": "t__e__p", "components": ["t_.svg", "_e_4.svg", "_p.svg"]}, {
                    "name": "t__e__!_p",
                    "components": ["t_.svg", "_e_4.svg", "_!3.svg", "_p.svg"]
                }, {"name": "t__e__t", "components": ["t_.svg", "_e_4.svg", "_t.svg"]}, {
                    "name": "t__e__!_t",
                    "components": ["t_.svg", "_e_4.svg", "_!3.svg", "_t.svg"]
                }, {"name": "t__e__tl", "components": ["t_.svg", "_e_4.svg", "_tl.svg"]}, {
                    "name": "t__e__!_tl",
                    "components": ["t_.svg", "_e_4.svg", "_!3.svg", "_tl.svg"]
                }, {"name": "t__e__x", "components": ["t_.svg", "_e_4.svg", "_x.svg"]}, {
                    "name": "t__e__!_x",
                    "components": ["t_.svg", "_e_4.svg", "_!3.svg", "_x.svg"]
                }, {"name": "t__e__c", "components": ["t_.svg", "_e_4.svg", "_c.svg"]}, {
                    "name": "t__e__!_c",
                    "components": ["t_.svg", "_e_4.svg", "_!3.svg", "_c.svg"]
                }, {"name": "t__e__s", "components": ["t_.svg", "_e_4.svg", "_s.svg"]}, {
                    "name": "t__e__!_s",
                    "components": ["t_.svg", "_e_4.svg", "_!3.svg", "_s.svg"]
                }, {"name": "t__e__z", "components": ["t_.svg", "_e_4.svg", "_z.svg"]}, {
                    "name": "t__e__!_z",
                    "components": ["t_.svg", "_e_4.svg", "_!3.svg", "_z.svg"]
                }, {"name": "t__e__k", "components": ["t_.svg", "_e_4.svg", "_k.svg"]}, {
                    "name": "t__e__!_k",
                    "components": ["t_.svg", "_e_4.svg", "_!3.svg", "_k.svg"]
                }, {"name": "t__e__q", "components": ["t_.svg", "_e_4.svg", "_q.svg"]}, {
                    "name": "t__e__!_q",
                    "components": ["t_.svg", "_e_4.svg", "_!3.svg", "_q.svg"]
                }, {"name": "t__e__h", "components": ["t_.svg", "_e_4.svg", "_h.svg"]}, {
                    "name": "t__e__!_h",
                    "components": ["t_.svg", "_e_4.svg", "_!3.svg", "_h.svg"]
                }, {"name": "t__e__l", "components": ["t_.svg", "_e_4.svg", "_l.svg"]}, {
                    "name": "t__e__!_l",
                    "components": ["t_.svg", "_e_4.svg", "_!3.svg", "_l.svg"]
                }, {"name": "t__e__w", "components": ["t_.svg", "_e_4.svg", "_w.svg"]}, {
                    "name": "t__e__!_w",
                    "components": ["t_.svg", "_e_4.svg", "_!3.svg", "_w.svg"]
                }, {"name": "t__e__y", "components": ["t_.svg", "_e_4.svg", "_y.svg"]}, {
                    "name": "t__e__!_y",
                    "components": ["t_.svg", "_e_4.svg", "_!3.svg", "_y.svg"]
                }, {"name": "t__e__b", "components": ["t_.svg", "_e_4.svg", "_b.svg"]}, {
                    "name": "t__e__!_b",
                    "components": ["t_.svg", "_e_4.svg", "_!3.svg", "_b.svg"]
                }, {"name": "t__e__d", "components": ["t_.svg", "_e_4.svg", "_d.svg"]}, {
                    "name": "t__e__!_d",
                    "components": ["t_.svg", "_e_4.svg", "_!3.svg", "_d.svg"]
                }, {"name": "t__e__f", "components": ["t_.svg", "_e_4.svg", "_f.svg"]}, {
                    "name": "t__e__!_f",
                    "components": ["t_.svg", "_e_4.svg", "_!3.svg", "_f.svg"]
                }, {"name": "t__e__g", "components": ["t_.svg", "_e_4.svg", "_g.svg"]}, {
                    "name": "t__e__!_g",
                    "components": ["t_.svg", "_e_4.svg", "_!3.svg", "_g.svg"]
                }, {"name": "t__e__kh", "components": ["t_.svg", "_e_4.svg", "_kh.svg"]}, {
                    "name": "t__e__!_kh",
                    "components": ["t_.svg", "_e_4.svg", "_!3.svg", "_kh.svg"]
                }, {"name": "t__e__j", "components": ["t_.svg", "_e_4.svg", "_j.svg"]}, {
                    "name": "t__e__!_j",
                    "components": ["t_.svg", "_e_4.svg", "_!3.svg", "_j.svg"]
                }, {"name": "t__e__r", "components": ["t_.svg", "_e_4.svg", "_r.svg"]}, {
                    "name": "t__e__!_r",
                    "components": ["t_.svg", "_e_4.svg", "_!3.svg", "_r.svg"]
                }, {"name": "t__e__z'", "components": ["t_.svg", "_e_4.svg", "_z'.svg"]}, {
                    "name": "t__e__!_z'",
                    "components": ["t_.svg", "_e_4.svg", "_!3.svg", "_z'.svg"]
                }, {"name": "t__e__v", "components": ["t_.svg", "_e_4.svg", "_v.svg"]}, {
                    "name": "t__e__!_v",
                    "components": ["t_.svg", "_e_4.svg", "_!3.svg", "_v.svg"]
                }, {"name": "t__e__ks", "components": ["t_.svg", "_e_4.svg", "_ks.svg"]}, {
                    "name": "t__e__!_ks",
                    "components": ["t_.svg", "_e_4.svg", "_!3.svg", "_ks.svg"]
                }, {"name": "t__e__ny", "components": ["t_.svg", "_e_4.svg", "_ny.svg"]}, {
                    "name": "t__e__!_ny",
                    "components": ["t_.svg", "_e_4.svg", "_!3.svg", "_ny.svg"]
                }]
            }, {
                "directory": "005 Mokawaqilotontli",
                "combinations": [{"name": "t~", "components": ["t.svg", "~3.svg"]}]
            }, {
                "directory": "006 Unaliqilotontli",
                "combinations": [{"name": "t_u", "components": ["t.svg", "_u3.svg"]}, {
                    "name": "t_u'",
                    "components": ["t.svg", "_u3.svg", "'5a.svg"]
                }, {"name": "t_u!", "components": ["t.svg", "_u3.svg", "!3.svg"]}, {
                    "name": "t_u!'",
                    "components": ["t.svg", "_u3.svg", "!3.svg", "'5a.svg"]
                }, {"name": "t__u__m", "components": ["t_.svg", "_u_4.svg", "_m.svg"]}, {
                    "name": "t__u__!_m",
                    "components": ["t_.svg", "_u_4.svg", "_!3.svg", "_m.svg"]
                }, {"name": "t__u__n", "components": ["t_.svg", "_u_4.svg", "_n.svg"]}, {
                    "name": "t__u__!_n",
                    "components": ["t_.svg", "_u_4.svg", "_!3.svg", "_n.svg"]
                }, {"name": "t__u__p", "components": ["t_.svg", "_u_4.svg", "_p.svg"]}, {
                    "name": "t__u__!_p",
                    "components": ["t_.svg", "_u_4.svg", "_!3.svg", "_p.svg"]
                }, {"name": "t__u__t", "components": ["t_.svg", "_u_4.svg", "_t.svg"]}, {
                    "name": "t__u__!_t",
                    "components": ["t_.svg", "_u_4.svg", "_!3.svg", "_t.svg"]
                }, {"name": "t__u__tl", "components": ["t_.svg", "_u_4.svg", "_tl.svg"]}, {
                    "name": "t__u__!_tl",
                    "components": ["t_.svg", "_u_4.svg", "_!3.svg", "_tl.svg"]
                }, {"name": "t__u__x", "components": ["t_.svg", "_u_4.svg", "_x.svg"]}, {
                    "name": "t__u__!_x",
                    "components": ["t_.svg", "_u_4.svg", "_!3.svg", "_x.svg"]
                }, {"name": "t__u__c", "components": ["t_.svg", "_u_4.svg", "_c.svg"]}, {
                    "name": "t__u__!_c",
                    "components": ["t_.svg", "_u_4.svg", "_!3.svg", "_c.svg"]
                }, {"name": "t__u__s", "components": ["t_.svg", "_u_4.svg", "_s.svg"]}, {
                    "name": "t__u__!_s",
                    "components": ["t_.svg", "_u_4.svg", "_!3.svg", "_s.svg"]
                }, {"name": "t__u__z", "components": ["t_.svg", "_u_4.svg", "_z.svg"]}, {
                    "name": "t__u__!_z",
                    "components": ["t_.svg", "_u_4.svg", "_!3.svg", "_z.svg"]
                }, {"name": "t__u__k", "components": ["t_.svg", "_u_4.svg", "_k.svg"]}, {
                    "name": "t__u__!_k",
                    "components": ["t_.svg", "_u_4.svg", "_!3.svg", "_k.svg"]
                }, {"name": "t__u__q", "components": ["t_.svg", "_u_4.svg", "_q.svg"]}, {
                    "name": "t__u__!_q",
                    "components": ["t_.svg", "_u_4.svg", "_!3.svg", "_q.svg"]
                }, {"name": "t__u__h", "components": ["t_.svg", "_u_4.svg", "_h.svg"]}, {
                    "name": "t__u__!_h",
                    "components": ["t_.svg", "_u_4.svg", "_!3.svg", "_h.svg"]
                }, {"name": "t__u__l", "components": ["t_.svg", "_u_4.svg", "_l.svg"]}, {
                    "name": "t__u__!_l",
                    "components": ["t_.svg", "_u_4.svg", "_!3.svg", "_l.svg"]
                }, {"name": "t__u__w", "components": ["t_.svg", "_u_4.svg", "_w.svg"]}, {
                    "name": "t__u__!_w",
                    "components": ["t_.svg", "_u_4.svg", "_!3.svg", "_w.svg"]
                }, {"name": "t__u__y", "components": ["t_.svg", "_u_4.svg", "_y.svg"]}, {
                    "name": "t__u__!_y",
                    "components": ["t_.svg", "_u_4.svg", "_!3.svg", "_y.svg"]
                }, {"name": "t__u__b", "components": ["t_.svg", "_u_4.svg", "_b.svg"]}, {
                    "name": "t__u__!_b",
                    "components": ["t_.svg", "_u_4.svg", "_!3.svg", "_b.svg"]
                }, {"name": "t__u__d", "components": ["t_.svg", "_u_4.svg", "_d.svg"]}, {
                    "name": "t__u__!_d",
                    "components": ["t_.svg", "_u_4.svg", "_!3.svg", "_d.svg"]
                }, {"name": "t__u__f", "components": ["t_.svg", "_u_4.svg", "_f.svg"]}, {
                    "name": "t__u__!_f",
                    "components": ["t_.svg", "_u_4.svg", "_!3.svg", "_f.svg"]
                }, {"name": "t__u__g", "components": ["t_.svg", "_u_4.svg", "_g.svg"]}, {
                    "name": "t__u__!_g",
                    "components": ["t_.svg", "_u_4.svg", "_!3.svg", "_g.svg"]
                }, {"name": "t__u__kh", "components": ["t_.svg", "_u_4.svg", "_kh.svg"]}, {
                    "name": "t__u__!_kh",
                    "components": ["t_.svg", "_u_4.svg", "_!3.svg", "_kh.svg"]
                }, {"name": "t__u__j", "components": ["t_.svg", "_u_4.svg", "_j.svg"]}, {
                    "name": "t__u__!_j",
                    "components": ["t_.svg", "_u_4.svg", "_!3.svg", "_j.svg"]
                }, {"name": "t__u__r", "components": ["t_.svg", "_u_4.svg", "_r.svg"]}, {
                    "name": "t__u__!_r",
                    "components": ["t_.svg", "_u_4.svg", "_!3.svg", "_r.svg"]
                }, {"name": "t__u__z'", "components": ["t_.svg", "_u_4.svg", "_z'.svg"]}, {
                    "name": "t__u__!_z'",
                    "components": ["t_.svg", "_u_4.svg", "_!3.svg", "_z'.svg"]
                }, {"name": "t__u__v", "components": ["t_.svg", "_u_4.svg", "_v.svg"]}, {
                    "name": "t__u__!_v",
                    "components": ["t_.svg", "_u_4.svg", "_!3.svg", "_v.svg"]
                }, {"name": "t__u__ks", "components": ["t_.svg", "_u_4.svg", "_ks.svg"]}, {
                    "name": "t__u__!_ks",
                    "components": ["t_.svg", "_u_4.svg", "_!3.svg", "_ks.svg"]
                }, {"name": "t__u__ny", "components": ["t_.svg", "_u_4.svg", "_ny.svg"]}, {
                    "name": "t__u__!_ny",
                    "components": ["t_.svg", "_u_4.svg", "_!3.svg", "_ny.svg"]
                }]
            }];
            const results = xachama.formConsonantFullSets('t');

            assert.strictEqual(results.length, expected.length);

            for (const z in results) {
                assert.strictEqual(results[z].directory, expected[z].directory);

                for (const x in results[z].combinations)
                    for (const y in results[z].combinations[x].components)
                        assert.strictEqual(results[z].combinations[x].components[y], expected[z].combinations[x].components[y]);
            }
        });
    });

    suite('Form syllable combination sets', () => {
        suiteSetup(() => {
            const oldPath = xachama.getDir();
            const newPath = `${oldPath.replace('results', 'T')}`;

            xachama.makeDir(newPath);
            xachama.setDir(newPath);
            xachama.copyDir(oldPath, newPath);
        });

        test('Case tekoli syllable combinations', () => {
            const fullSets = [
                {
                    directory: '001 Akaqilotontli',
                    combinations: []
                }
            ];
            const expectedResult = [{
                "fileName": "001 Akaqiloli/00000-t_a.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/001 Akaqiloli/00000-t_a.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 380621,
                    "size": 401,
                    "blocks": 1,
                    "atimeMs": 1582147969432.4111,
                    "mtimeMs": 1582148061218.3948,
                    "ctimeMs": 1582148061218.3948,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:32:49.432Z",
                    "mtime": "2020-02-19T21:34:21.218Z",
                    "ctime": "2020-02-19T21:34:21.218Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Tekolqiloli\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 90.71191 98.22174\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "001 Akaqiloli/00001-t_a'.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/001 Akaqiloli/00001-t_a'.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 380624,
                    "size": 425,
                    "blocks": 1,
                    "atimeMs": 1582148061219.5623,
                    "mtimeMs": 1582148061220.6555,
                    "ctimeMs": 1582148061220.6555,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:34:21.220Z",
                    "mtime": "2020-02-19T21:34:21.221Z",
                    "ctime": "2020-02-19T21:34:21.221Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Hekalqilotontli_1\" data-name=\"Hekalqilotontli 1\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 33 33\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "001 Akaqiloli/00002-t_a!.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/001 Akaqiloli/00002-t_a!.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 380628,
                    "size": 416,
                    "blocks": 1,
                    "atimeMs": 1582148061221.7576,
                    "mtimeMs": 1582148061222.614,
                    "ctimeMs": 1582148061222.614,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:34:21.222Z",
                    "mtime": "2020-02-19T21:34:21.223Z",
                    "ctime": "2020-02-19T21:34:21.223Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Weyakqiloli_1\" data-name=\"Weyakqiloli 1\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 80 6\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "001 Akaqiloli/00003-t_a!_'.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/001 Akaqiloli/00003-t_a!_'.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 380646,
                    "size": 425,
                    "blocks": 1,
                    "atimeMs": 1582148061224.5925,
                    "mtimeMs": 1582148061225.4536,
                    "ctimeMs": 1582148061225.4536,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:34:21.225Z",
                    "mtime": "2020-02-19T21:34:21.225Z",
                    "ctime": "2020-02-19T21:34:21.225Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Hekalqilotontli_1\" data-name=\"Hekalqilotontli 1\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 33 33\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "001 Akaqiloli/00004-t__a__m.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/001 Akaqiloli/00004-t__a__m.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 381236,
                    "size": 433,
                    "blocks": 1,
                    "atimeMs": 1582148061226.5005,
                    "mtimeMs": 1582148061227.5518,
                    "ctimeMs": 1582148061227.5518,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:34:21.227Z",
                    "mtime": "2020-02-19T21:34:21.228Z",
                    "ctime": "2020-02-19T21:34:21.228Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Mailqiloli_itik\" data-name=\"Mailqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 75.41407 65.86328\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "001 Akaqiloli/00005-t__a__!_m.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/001 Akaqiloli/00005-t__a__!_m.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 381237,
                    "size": 433,
                    "blocks": 1,
                    "atimeMs": 1582148061229.6973,
                    "mtimeMs": 1582148061230.957,
                    "ctimeMs": 1582148061230.957,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:34:21.230Z",
                    "mtime": "2020-02-19T21:34:21.231Z",
                    "ctime": "2020-02-19T21:34:21.231Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Mailqiloli_itik\" data-name=\"Mailqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 75.41407 65.86328\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "001 Akaqiloli/00006-t__a__n.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/001 Akaqiloli/00006-t__a__n.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 381265,
                    "size": 434,
                    "blocks": 1,
                    "atimeMs": 1582148061232.1306,
                    "mtimeMs": 1582148061232.946,
                    "ctimeMs": 1582148061232.946,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:34:21.232Z",
                    "mtime": "2020-02-19T21:34:21.233Z",
                    "ctime": "2020-02-19T21:34:21.233Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Neliwqiloli_itik\" data-name=\"Neliwqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 65.4248 65.38281\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "001 Akaqiloli/00007-t__a__!_n.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/001 Akaqiloli/00007-t__a__!_n.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 381266,
                    "size": 434,
                    "blocks": 1,
                    "atimeMs": 1582148061234.629,
                    "mtimeMs": 1582148061235.3694,
                    "ctimeMs": 1582148061235.3694,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:34:21.235Z",
                    "mtime": "2020-02-19T21:34:21.235Z",
                    "ctime": "2020-02-19T21:34:21.235Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Neliwqiloli_itik\" data-name=\"Neliwqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 65.4248 65.38281\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "001 Akaqiloli/00008-t__a__p.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/001 Akaqiloli/00008-t__a__p.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 381267,
                    "size": 435,
                    "blocks": 1,
                    "atimeMs": 1582148061236.2515,
                    "mtimeMs": 1582148061237.0332,
                    "ctimeMs": 1582148061237.0332,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:34:21.236Z",
                    "mtime": "2020-02-19T21:34:21.237Z",
                    "ctime": "2020-02-19T21:34:21.237Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Pokalqiloli_itik\" data-name=\"Pokalqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 55.57676 69.98151\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "001 Akaqiloli/00009-t__a__!_p.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/001 Akaqiloli/00009-t__a__!_p.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 381284,
                    "size": 435,
                    "blocks": 1,
                    "atimeMs": 1582148061238.7192,
                    "mtimeMs": 1582148061239.4143,
                    "ctimeMs": 1582148061239.4143,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:34:21.239Z",
                    "mtime": "2020-02-19T21:34:21.239Z",
                    "ctime": "2020-02-19T21:34:21.239Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Pokalqiloli_itik\" data-name=\"Pokalqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 55.57676 69.98151\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "001 Akaqiloli/00010-t__a__t.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/001 Akaqiloli/00010-t__a__t.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 381285,
                    "size": 435,
                    "blocks": 1,
                    "atimeMs": 1582148061240.1587,
                    "mtimeMs": 1582148061240.724,
                    "ctimeMs": 1582148061240.724,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:34:21.240Z",
                    "mtime": "2020-02-19T21:34:21.241Z",
                    "ctime": "2020-02-19T21:34:21.241Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Tekolqiloli_itik\" data-name=\"Tekolqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 70.61133 68.50488\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "001 Akaqiloli/00011-t__a__!_t.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/001 Akaqiloli/00011-t__a__!_t.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 381287,
                    "size": 435,
                    "blocks": 1,
                    "atimeMs": 1582148061242.0513,
                    "mtimeMs": 1582148061242.6006,
                    "ctimeMs": 1582148061242.6006,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:34:21.242Z",
                    "mtime": "2020-02-19T21:34:21.243Z",
                    "ctime": "2020-02-19T21:34:21.243Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Tekolqiloli_itik\" data-name=\"Tekolqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 70.61133 68.50488\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "001 Akaqiloli/00012-t__a__tl.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/001 Akaqiloli/00012-t__a__tl.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 381289,
                    "size": 439,
                    "blocks": 1,
                    "atimeMs": 1582148061243.2932,
                    "mtimeMs": 1582148061243.8599,
                    "ctimeMs": 1582148061243.8599,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:34:21.243Z",
                    "mtime": "2020-02-19T21:34:21.244Z",
                    "ctime": "2020-02-19T21:34:21.244Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Tlachalqiloli_itik\" data-name=\"Tlachalqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 73.98493 69.98828\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "001 Akaqiloli/00013-t__a__!_tl.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/001 Akaqiloli/00013-t__a__!_tl.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 381290,
                    "size": 439,
                    "blocks": 1,
                    "atimeMs": 1582148061245.4106,
                    "mtimeMs": 1582148061246.0127,
                    "ctimeMs": 1582148061246.0127,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:34:21.245Z",
                    "mtime": "2020-02-19T21:34:21.246Z",
                    "ctime": "2020-02-19T21:34:21.246Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Tlachalqiloli_itik\" data-name=\"Tlachalqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 73.98493 69.98828\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "001 Akaqiloli/00014-t__a__x.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/001 Akaqiloli/00014-t__a__x.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 381314,
                    "size": 435,
                    "blocks": 1,
                    "atimeMs": 1582148061246.6514,
                    "mtimeMs": 1582148061247.2144,
                    "ctimeMs": 1582148061247.2144,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:34:21.247Z",
                    "mtime": "2020-02-19T21:34:21.247Z",
                    "ctime": "2020-02-19T21:34:21.247Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Xocilqiloli_itik\" data-name=\"Xocilqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 58.64222 66.89453\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "001 Akaqiloli/00015-t__a__!_x.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/001 Akaqiloli/00015-t__a__!_x.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 381319,
                    "size": 435,
                    "blocks": 1,
                    "atimeMs": 1582148061248.4065,
                    "mtimeMs": 1582148061248.97,
                    "ctimeMs": 1582148061248.97,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:34:21.248Z",
                    "mtime": "2020-02-19T21:34:21.249Z",
                    "ctime": "2020-02-19T21:34:21.249Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Xocilqiloli_itik\" data-name=\"Xocilqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 58.64222 66.89453\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "001 Akaqiloli/00016-t__a__c.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/001 Akaqiloli/00016-t__a__c.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 381320,
                    "size": 437,
                    "blocks": 1,
                    "atimeMs": 1582148061249.6294,
                    "mtimeMs": 1582148061250.2026,
                    "ctimeMs": 1582148061250.2026,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:34:21.250Z",
                    "mtime": "2020-02-19T21:34:21.250Z",
                    "ctime": "2020-02-19T21:34:21.250Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Chakalqiloli_itik\" data-name=\"Chakalqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 58.00381 69.90327\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "001 Akaqiloli/00017-t__a__!_c.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/001 Akaqiloli/00017-t__a__!_c.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 381321,
                    "size": 437,
                    "blocks": 1,
                    "atimeMs": 1582148061251.5046,
                    "mtimeMs": 1582148061252.0635,
                    "ctimeMs": 1582148061252.0635,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:34:21.252Z",
                    "mtime": "2020-02-19T21:34:21.252Z",
                    "ctime": "2020-02-19T21:34:21.252Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Chakalqiloli_itik\" data-name=\"Chakalqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 58.00381 69.90327\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "001 Akaqiloli/00018-t__a__s.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/001 Akaqiloli/00018-t__a__s.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 381323,
                    "size": 437,
                    "blocks": 1,
                    "atimeMs": 1582148061252.705,
                    "mtimeMs": 1582148061253.2173,
                    "ctimeMs": 1582148061253.2173,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:34:21.253Z",
                    "mtime": "2020-02-19T21:34:21.253Z",
                    "ctime": "2020-02-19T21:34:21.253Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Senzonqiloli_itik\" data-name=\"Senzonqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 77.00781 67.02148\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "001 Akaqiloli/00019-t__a__!_s.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/001 Akaqiloli/00019-t__a__!_s.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 381324,
                    "size": 437,
                    "blocks": 1,
                    "atimeMs": 1582148061254.4062,
                    "mtimeMs": 1582148061254.9001,
                    "ctimeMs": 1582148061254.9001,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:34:21.254Z",
                    "mtime": "2020-02-19T21:34:21.255Z",
                    "ctime": "2020-02-19T21:34:21.255Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Senzonqiloli_itik\" data-name=\"Senzonqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 77.00781 67.02148\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "001 Akaqiloli/00020-t__a__z.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/001 Akaqiloli/00020-t__a__z.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 381326,
                    "size": 433,
                    "blocks": 1,
                    "atimeMs": 1582148061255.5637,
                    "mtimeMs": 1582148061256.0796,
                    "ctimeMs": 1582148061256.0796,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:34:21.256Z",
                    "mtime": "2020-02-19T21:34:21.256Z",
                    "ctime": "2020-02-19T21:34:21.256Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Zopilqiloli_itik\" data-name=\"Zopilqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 77.00781 68.625\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "001 Akaqiloli/00021-t__a__!_z.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/001 Akaqiloli/00021-t__a__!_z.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 381333,
                    "size": 433,
                    "blocks": 1,
                    "atimeMs": 1582148061257.3918,
                    "mtimeMs": 1582148061257.9363,
                    "ctimeMs": 1582148061257.9363,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:34:21.257Z",
                    "mtime": "2020-02-19T21:34:21.258Z",
                    "ctime": "2020-02-19T21:34:21.258Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Zopilqiloli_itik\" data-name=\"Zopilqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 77.00781 68.625\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "001 Akaqiloli/00022-t__a__k.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/001 Akaqiloli/00022-t__a__k.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 381335,
                    "size": 433,
                    "blocks": 1,
                    "atimeMs": 1582148061258.5996,
                    "mtimeMs": 1582148061259.1519,
                    "ctimeMs": 1582148061259.1519,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:34:21.259Z",
                    "mtime": "2020-02-19T21:34:21.259Z",
                    "ctime": "2020-02-19T21:34:21.259Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Coalqiloli_itik\" data-name=\"Coalqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 68.84783 69.99839\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "001 Akaqiloli/00023-t__a__!_k.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/001 Akaqiloli/00023-t__a__!_k.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 381338,
                    "size": 433,
                    "blocks": 1,
                    "atimeMs": 1582148061261.6726,
                    "mtimeMs": 1582148061262.3425,
                    "ctimeMs": 1582148061262.3425,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:34:21.262Z",
                    "mtime": "2020-02-19T21:34:21.262Z",
                    "ctime": "2020-02-19T21:34:21.262Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Coalqiloli_itik\" data-name=\"Coalqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 68.84783 69.99839\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "001 Akaqiloli/00024-t__a__q.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/001 Akaqiloli/00024-t__a__q.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 381339,
                    "size": 435,
                    "blocks": 1,
                    "atimeMs": 1582148061262.9949,
                    "mtimeMs": 1582148061263.5781,
                    "ctimeMs": 1582148061263.5781,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:34:21.263Z",
                    "mtime": "2020-02-19T21:34:21.264Z",
                    "ctime": "2020-02-19T21:34:21.264Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Qaqawqiloli_itik\" data-name=\"Qaqawqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 67.13916 68.67383\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "001 Akaqiloli/00025-t__a__!_q.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/001 Akaqiloli/00025-t__a__!_q.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 381340,
                    "size": 435,
                    "blocks": 1,
                    "atimeMs": 1582148061265.0713,
                    "mtimeMs": 1582148061265.5989,
                    "ctimeMs": 1582148061265.5989,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:34:21.265Z",
                    "mtime": "2020-02-19T21:34:21.266Z",
                    "ctime": "2020-02-19T21:34:21.266Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Qaqawqiloli_itik\" data-name=\"Qaqawqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 67.13916 68.67383\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "001 Akaqiloli/00026-t__a__h.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/001 Akaqiloli/00026-t__a__h.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 381343,
                    "size": 433,
                    "blocks": 1,
                    "atimeMs": 1582148061266.2092,
                    "mtimeMs": 1582148061266.7253,
                    "ctimeMs": 1582148061266.7253,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:34:21.266Z",
                    "mtime": "2020-02-19T21:34:21.267Z",
                    "ctime": "2020-02-19T21:34:21.267Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Hekalqiloli_itik\" data-name=\"Hekalqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 60.0387 63.1527\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "001 Akaqiloli/00027-t__a__!_h.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/001 Akaqiloli/00027-t__a__!_h.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 381345,
                    "size": 433,
                    "blocks": 1,
                    "atimeMs": 1582148061268.0393,
                    "mtimeMs": 1582148061268.7131,
                    "ctimeMs": 1582148061268.7131,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:34:21.268Z",
                    "mtime": "2020-02-19T21:34:21.269Z",
                    "ctime": "2020-02-19T21:34:21.269Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Hekalqiloli_itik\" data-name=\"Hekalqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 60.0387 63.1527\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "001 Akaqiloli/00028-t__a__l.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/001 Akaqiloli/00028-t__a__l.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 381349,
                    "size": 435,
                    "blocks": 1,
                    "atimeMs": 1582148061269.4297,
                    "mtimeMs": 1582148061270.0066,
                    "ctimeMs": 1582148061270.0066,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:34:21.269Z",
                    "mtime": "2020-02-19T21:34:21.270Z",
                    "ctime": "2020-02-19T21:34:21.270Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Limonqiloli_itik\" data-name=\"Limonqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 56.86193 67.47461\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "001 Akaqiloli/00029-t__a__!_l.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/001 Akaqiloli/00029-t__a__!_l.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 381361,
                    "size": 435,
                    "blocks": 1,
                    "atimeMs": 1582148061271.388,
                    "mtimeMs": 1582148061272.3154,
                    "ctimeMs": 1582148061272.3154,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:34:21.271Z",
                    "mtime": "2020-02-19T21:34:21.272Z",
                    "ctime": "2020-02-19T21:34:21.272Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Limonqiloli_itik\" data-name=\"Limonqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 56.86193 67.47461\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "001 Akaqiloli/00030-t__a__w.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/001 Akaqiloli/00030-t__a__w.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 381367,
                    "size": 435,
                    "blocks": 1,
                    "atimeMs": 1582148061273.2537,
                    "mtimeMs": 1582148061275.1697,
                    "ctimeMs": 1582148061275.1697,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:34:21.273Z",
                    "mtime": "2020-02-19T21:34:21.275Z",
                    "ctime": "2020-02-19T21:34:21.275Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Weyilqiloli_itik\" data-name=\"Weyilqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 77.05322 69.88477\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "001 Akaqiloli/00031-t__a__!_w.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/001 Akaqiloli/00031-t__a__!_w.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 381368,
                    "size": 435,
                    "blocks": 1,
                    "atimeMs": 1582148061277.134,
                    "mtimeMs": 1582148061277.9912,
                    "ctimeMs": 1582148061277.9912,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:34:21.277Z",
                    "mtime": "2020-02-19T21:34:21.278Z",
                    "ctime": "2020-02-19T21:34:21.278Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Weyilqiloli_itik\" data-name=\"Weyilqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 77.05322 69.88477\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "001 Akaqiloli/00032-t__a__y.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/001 Akaqiloli/00032-t__a__y.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 381370,
                    "size": 435,
                    "blocks": 1,
                    "atimeMs": 1582148061278.9712,
                    "mtimeMs": 1582148061280.034,
                    "ctimeMs": 1582148061280.034,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:34:21.279Z",
                    "mtime": "2020-02-19T21:34:21.280Z",
                    "ctime": "2020-02-19T21:34:21.280Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Yololqiloli_itik\" data-name=\"Yololqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 59.96725 67.44629\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "001 Akaqiloli/00033-t__a__!_y.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/001 Akaqiloli/00033-t__a__!_y.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 381372,
                    "size": 435,
                    "blocks": 1,
                    "atimeMs": 1582148061281.783,
                    "mtimeMs": 1582148061282.36,
                    "ctimeMs": 1582148061282.36,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:34:21.282Z",
                    "mtime": "2020-02-19T21:34:21.282Z",
                    "ctime": "2020-02-19T21:34:21.282Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Yololqiloli_itik\" data-name=\"Yololqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 59.96725 67.44629\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "001 Akaqiloli/00034-t__a__b.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/001 Akaqiloli/00034-t__a__b.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 381373,
                    "size": 435,
                    "blocks": 1,
                    "atimeMs": 1582148061283.0698,
                    "mtimeMs": 1582148061283.7715,
                    "ctimeMs": 1582148061283.7715,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:34:21.283Z",
                    "mtime": "2020-02-19T21:34:21.284Z",
                    "ctime": "2020-02-19T21:34:21.284Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Balamqiloli_itik\" data-name=\"Balamqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 75.41407 65.86328\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "001 Akaqiloli/00035-t__a__!_b.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/001 Akaqiloli/00035-t__a__!_b.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 381375,
                    "size": 435,
                    "blocks": 1,
                    "atimeMs": 1582148061285.127,
                    "mtimeMs": 1582148061285.8357,
                    "ctimeMs": 1582148061285.8357,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:34:21.285Z",
                    "mtime": "2020-02-19T21:34:21.286Z",
                    "ctime": "2020-02-19T21:34:21.286Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Balamqiloli_itik\" data-name=\"Balamqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 75.41407 65.86328\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "001 Akaqiloli/00036-t__a__d.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/001 Akaqiloli/00036-t__a__d.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 381379,
                    "size": 435,
                    "blocks": 1,
                    "atimeMs": 1582148061286.5642,
                    "mtimeMs": 1582148061287.1794,
                    "ctimeMs": 1582148061287.1794,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:34:21.287Z",
                    "mtime": "2020-02-19T21:34:21.287Z",
                    "ctime": "2020-02-19T21:34:21.287Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Dunilqiloli_itik\" data-name=\"Dunilqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 70.61133 68.50488\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "001 Akaqiloli/00037-t__a__!_d.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/001 Akaqiloli/00037-t__a__!_d.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 381380,
                    "size": 435,
                    "blocks": 1,
                    "atimeMs": 1582148061288.8008,
                    "mtimeMs": 1582148061289.6038,
                    "ctimeMs": 1582148061289.6038,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:34:21.289Z",
                    "mtime": "2020-02-19T21:34:21.290Z",
                    "ctime": "2020-02-19T21:34:21.290Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Dunilqiloli_itik\" data-name=\"Dunilqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 70.61133 68.50488\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "001 Akaqiloli/00038-t__a__f.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/001 Akaqiloli/00038-t__a__f.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 381381,
                    "size": 437,
                    "blocks": 1,
                    "atimeMs": 1582148061290.2854,
                    "mtimeMs": 1582148061290.8767,
                    "ctimeMs": 1582148061290.8767,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:34:21.290Z",
                    "mtime": "2020-02-19T21:34:21.291Z",
                    "ctime": "2020-02-19T21:34:21.291Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Thamilqiloli_itik\" data-name=\"Thamilqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 55.57676 69.98151\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "001 Akaqiloli/00039-t__a__!_f.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/001 Akaqiloli/00039-t__a__!_f.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 381385,
                    "size": 437,
                    "blocks": 1,
                    "atimeMs": 1582148061292.0813,
                    "mtimeMs": 1582148061292.7122,
                    "ctimeMs": 1582148061292.7122,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:34:21.292Z",
                    "mtime": "2020-02-19T21:34:21.293Z",
                    "ctime": "2020-02-19T21:34:21.293Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Thamilqiloli_itik\" data-name=\"Thamilqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 55.57676 69.98151\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "001 Akaqiloli/00040-t__a__g.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/001 Akaqiloli/00040-t__a__g.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 381386,
                    "size": 435,
                    "blocks": 1,
                    "atimeMs": 1582148061293.4019,
                    "mtimeMs": 1582148061294.2322,
                    "ctimeMs": 1582148061294.2322,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:34:21.293Z",
                    "mtime": "2020-02-19T21:34:21.294Z",
                    "ctime": "2020-02-19T21:34:21.294Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Galilqiloli_itik\" data-name=\"Galilqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 68.84685 69.99854\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "001 Akaqiloli/00041-t__a__!_g.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/001 Akaqiloli/00041-t__a__!_g.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 381387,
                    "size": 435,
                    "blocks": 1,
                    "atimeMs": 1582148061295.4614,
                    "mtimeMs": 1582148061296.5422,
                    "ctimeMs": 1582148061296.5422,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:34:21.295Z",
                    "mtime": "2020-02-19T21:34:21.297Z",
                    "ctime": "2020-02-19T21:34:21.297Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Galilqiloli_itik\" data-name=\"Galilqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 68.84685 69.99854\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "001 Akaqiloli/00042-t__a__kh.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/001 Akaqiloli/00042-t__a__kh.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 381388,
                    "size": 437,
                    "blocks": 1,
                    "atimeMs": 1582148061297.2056,
                    "mtimeMs": 1582148061297.8337,
                    "ctimeMs": 1582148061297.8337,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:34:21.297Z",
                    "mtime": "2020-02-19T21:34:21.298Z",
                    "ctime": "2020-02-19T21:34:21.298Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Khamalqiloli_itik\" data-name=\"Khamalqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 65.60791 63.15566\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "001 Akaqiloli/00043-t__a__!_kh.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/001 Akaqiloli/00043-t__a__!_kh.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 381392,
                    "size": 437,
                    "blocks": 1,
                    "atimeMs": 1582148061299.091,
                    "mtimeMs": 1582148061300.2576,
                    "ctimeMs": 1582148061300.2576,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:34:21.299Z",
                    "mtime": "2020-02-19T21:34:21.300Z",
                    "ctime": "2020-02-19T21:34:21.300Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Khamalqiloli_itik\" data-name=\"Khamalqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 65.60791 63.15566\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "001 Akaqiloli/00044-t__a__j.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/001 Akaqiloli/00044-t__a__j.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 381396,
                    "size": 435,
                    "blocks": 1,
                    "atimeMs": 1582148061301.0698,
                    "mtimeMs": 1582148061301.7334,
                    "ctimeMs": 1582148061301.7334,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:34:21.301Z",
                    "mtime": "2020-02-19T21:34:21.302Z",
                    "ctime": "2020-02-19T21:34:21.302Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Jokilqiloli_itik\" data-name=\"Jokilqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 66.64662 67.44434\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "001 Akaqiloli/00045-t__a__!_j.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/001 Akaqiloli/00045-t__a__!_j.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 381408,
                    "size": 435,
                    "blocks": 1,
                    "atimeMs": 1582148061302.928,
                    "mtimeMs": 1582148061303.4744,
                    "ctimeMs": 1582148061303.4744,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:34:21.303Z",
                    "mtime": "2020-02-19T21:34:21.303Z",
                    "ctime": "2020-02-19T21:34:21.303Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Jokilqiloli_itik\" data-name=\"Jokilqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 66.64662 67.44434\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "001 Akaqiloli/00046-t__a__r.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/001 Akaqiloli/00046-t__a__r.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 381419,
                    "size": 435,
                    "blocks": 1,
                    "atimeMs": 1582148061304.1084,
                    "mtimeMs": 1582148061304.667,
                    "ctimeMs": 1582148061304.667,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:34:21.304Z",
                    "mtime": "2020-02-19T21:34:21.305Z",
                    "ctime": "2020-02-19T21:34:21.305Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Runalqiloli_itik\" data-name=\"Runalqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 69.46289 67.47461\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "001 Akaqiloli/00047-t__a__!_r.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/001 Akaqiloli/00047-t__a__!_r.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 381420,
                    "size": 435,
                    "blocks": 1,
                    "atimeMs": 1582148061305.815,
                    "mtimeMs": 1582148061306.6255,
                    "ctimeMs": 1582148061306.6255,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:34:21.306Z",
                    "mtime": "2020-02-19T21:34:21.307Z",
                    "ctime": "2020-02-19T21:34:21.307Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Runalqiloli_itik\" data-name=\"Runalqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 69.46289 67.47461\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "001 Akaqiloli/00048-t__a__z'.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/001 Akaqiloli/00048-t__a__z'.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 381424,
                    "size": 435,
                    "blocks": 1,
                    "atimeMs": 1582148061307.3433,
                    "mtimeMs": 1582148061307.8674,
                    "ctimeMs": 1582148061307.8674,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:34:21.307Z",
                    "mtime": "2020-02-19T21:34:21.308Z",
                    "ctime": "2020-02-19T21:34:21.308Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Zawilqiloli_itik\" data-name=\"Zawilqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 76.99609 66.84766\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "001 Akaqiloli/00049-t__a__!_z'.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/001 Akaqiloli/00049-t__a__!_z'.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 381425,
                    "size": 435,
                    "blocks": 1,
                    "atimeMs": 1582148061309.2158,
                    "mtimeMs": 1582148061310.2717,
                    "ctimeMs": 1582148061310.2717,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:34:21.309Z",
                    "mtime": "2020-02-19T21:34:21.310Z",
                    "ctime": "2020-02-19T21:34:21.310Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Zawilqiloli_itik\" data-name=\"Zawilqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 76.99609 66.84766\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "001 Akaqiloli/00050-t__a__v.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/001 Akaqiloli/00050-t__a__v.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 381427,
                    "size": 435,
                    "blocks": 1,
                    "atimeMs": 1582148061310.9214,
                    "mtimeMs": 1582148061311.4517,
                    "ctimeMs": 1582148061311.4517,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:34:21.311Z",
                    "mtime": "2020-02-19T21:34:21.311Z",
                    "ctime": "2020-02-19T21:34:21.311Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Verolqiloli_itik\" data-name=\"Verolqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 77.05322 69.88477\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "001 Akaqiloli/00051-t__a__!_v.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/001 Akaqiloli/00051-t__a__!_v.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 381428,
                    "size": 435,
                    "blocks": 1,
                    "atimeMs": 1582148061315.2039,
                    "mtimeMs": 1582148061315.9424,
                    "ctimeMs": 1582148061315.9424,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:34:21.315Z",
                    "mtime": "2020-02-19T21:34:21.316Z",
                    "ctime": "2020-02-19T21:34:21.316Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Verolqiloli_itik\" data-name=\"Verolqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 77.05322 69.88477\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "001 Akaqiloli/00052-t__a__ks.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/001 Akaqiloli/00052-t__a__ks.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 381430,
                    "size": 437,
                    "blocks": 1,
                    "atimeMs": 1582148061316.8254,
                    "mtimeMs": 1582148061317.5457,
                    "ctimeMs": 1582148061317.5457,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:34:21.317Z",
                    "mtime": "2020-02-19T21:34:21.318Z",
                    "ctime": "2020-02-19T21:34:21.318Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Ksimalqiloli_itik\" data-name=\"Ksimalqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 76.99609 68.44629\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "001 Akaqiloli/00053-t__a__!_ks.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/001 Akaqiloli/00053-t__a__!_ks.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 381431,
                    "size": 437,
                    "blocks": 1,
                    "atimeMs": 1582148061319.0525,
                    "mtimeMs": 1582148061319.7957,
                    "ctimeMs": 1582148061319.7957,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:34:21.319Z",
                    "mtime": "2020-02-19T21:34:21.320Z",
                    "ctime": "2020-02-19T21:34:21.320Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Ksimalqiloli_itik\" data-name=\"Ksimalqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 76.99609 68.44629\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "001 Akaqiloli/00054-t__a__ny.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/001 Akaqiloli/00054-t__a__ny.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 381435,
                    "size": 437,
                    "blocks": 1,
                    "atimeMs": 1582148061320.6118,
                    "mtimeMs": 1582148061321.278,
                    "ctimeMs": 1582148061321.278,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:34:21.321Z",
                    "mtime": "2020-02-19T21:34:21.321Z",
                    "ctime": "2020-02-19T21:34:21.321Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Nyonilqiloli_itik\" data-name=\"Nyonilqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 75.40479 65.39844\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "001 Akaqiloli/00055-t__a__!_ny.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/001 Akaqiloli/00055-t__a__!_ny.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 381439,
                    "size": 437,
                    "blocks": 1,
                    "atimeMs": 1582148061322.7754,
                    "mtimeMs": 1582148061323.5642,
                    "ctimeMs": 1582148061323.5642,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:34:21.323Z",
                    "mtime": "2020-02-19T21:34:21.324Z",
                    "ctime": "2020-02-19T21:34:21.324Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Nyonilqiloli_itik\" data-name=\"Nyonilqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 75.40479 65.39844\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }];
            fullSets[0].combinations = xachama.formConsonantSets('t', 'a');
            const results = xachama.formCharacterSets('t', fullSets);

            assert.strictEqual(results.length, expectedResult.length);

            for (const i of results)
                assert.deepEqual(results[i], expectedResult[i]);
        });

        test('Case all tekoli syllable combinations', () => {
            const fullSets = xachama.formConsonantFullSets('t');
            const expectedResult = [{
                "fileName": "001 Akaqilotontli/00000-t_a.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/001 Akaqilotontli/00000-t_a.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 619295,
                    "size": 401,
                    "blocks": 1,
                    "atimeMs": 1582149112250.1118,
                    "mtimeMs": 1582149188565.8086,
                    "ctimeMs": 1582149188565.8086,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:51:52.250Z",
                    "mtime": "2020-02-19T21:53:08.566Z",
                    "ctime": "2020-02-19T21:53:08.566Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Tekolqiloli\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 90.71191 98.22174\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "001 Akaqilotontli/00001-t_a'.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/001 Akaqilotontli/00001-t_a'.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 619300,
                    "size": 425,
                    "blocks": 1,
                    "atimeMs": 1582149188566.3523,
                    "mtimeMs": 1582149188566.7322,
                    "ctimeMs": 1582149188566.7322,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.566Z",
                    "mtime": "2020-02-19T21:53:08.567Z",
                    "ctime": "2020-02-19T21:53:08.567Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Hekalqilotontli_1\" data-name=\"Hekalqilotontli 1\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 33 33\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "001 Akaqilotontli/00002-t_a!.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/001 Akaqilotontli/00002-t_a!.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 619301,
                    "size": 416,
                    "blocks": 1,
                    "atimeMs": 1582149188567.2585,
                    "mtimeMs": 1582149188567.6255,
                    "ctimeMs": 1582149188567.6255,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.567Z",
                    "mtime": "2020-02-19T21:53:08.568Z",
                    "ctime": "2020-02-19T21:53:08.568Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Weyakqiloli_1\" data-name=\"Weyakqiloli 1\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 80 6\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "001 Akaqilotontli/00003-t_a!_'.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/001 Akaqilotontli/00003-t_a!_'.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 619302,
                    "size": 425,
                    "blocks": 1,
                    "atimeMs": 1582149188568.5923,
                    "mtimeMs": 1582149188568.9795,
                    "ctimeMs": 1582149188568.9795,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.569Z",
                    "mtime": "2020-02-19T21:53:08.569Z",
                    "ctime": "2020-02-19T21:53:08.569Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Hekalqilotontli_1\" data-name=\"Hekalqilotontli 1\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 33 33\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "001 Akaqilotontli/00004-t__a__m.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/001 Akaqilotontli/00004-t__a__m.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 619303,
                    "size": 433,
                    "blocks": 1,
                    "atimeMs": 1582149188569.5295,
                    "mtimeMs": 1582149188569.974,
                    "ctimeMs": 1582149188569.974,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.570Z",
                    "mtime": "2020-02-19T21:53:08.570Z",
                    "ctime": "2020-02-19T21:53:08.570Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Mailqiloli_itik\" data-name=\"Mailqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 75.41407 65.86328\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "001 Akaqilotontli/00005-t__a__!_m.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/001 Akaqilotontli/00005-t__a__!_m.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 619306,
                    "size": 433,
                    "blocks": 1,
                    "atimeMs": 1582149188570.9204,
                    "mtimeMs": 1582149188571.3523,
                    "ctimeMs": 1582149188571.3523,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.571Z",
                    "mtime": "2020-02-19T21:53:08.571Z",
                    "ctime": "2020-02-19T21:53:08.571Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Mailqiloli_itik\" data-name=\"Mailqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 75.41407 65.86328\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "001 Akaqilotontli/00006-t__a__n.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/001 Akaqilotontli/00006-t__a__n.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 619307,
                    "size": 434,
                    "blocks": 1,
                    "atimeMs": 1582149188571.858,
                    "mtimeMs": 1582149188572.2488,
                    "ctimeMs": 1582149188572.2488,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.572Z",
                    "mtime": "2020-02-19T21:53:08.572Z",
                    "ctime": "2020-02-19T21:53:08.572Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Neliwqiloli_itik\" data-name=\"Neliwqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 65.4248 65.38281\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "001 Akaqilotontli/00007-t__a__!_n.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/001 Akaqilotontli/00007-t__a__!_n.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 619308,
                    "size": 434,
                    "blocks": 1,
                    "atimeMs": 1582149188573.187,
                    "mtimeMs": 1582149188573.8875,
                    "ctimeMs": 1582149188573.8875,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.573Z",
                    "mtime": "2020-02-19T21:53:08.574Z",
                    "ctime": "2020-02-19T21:53:08.574Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Neliwqiloli_itik\" data-name=\"Neliwqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 65.4248 65.38281\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "001 Akaqilotontli/00008-t__a__p.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/001 Akaqilotontli/00008-t__a__p.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 619309,
                    "size": 435,
                    "blocks": 1,
                    "atimeMs": 1582149188574.439,
                    "mtimeMs": 1582149188574.8916,
                    "ctimeMs": 1582149188574.8916,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.574Z",
                    "mtime": "2020-02-19T21:53:08.575Z",
                    "ctime": "2020-02-19T21:53:08.575Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Pokalqiloli_itik\" data-name=\"Pokalqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 55.57676 69.98151\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "001 Akaqilotontli/00009-t__a__!_p.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/001 Akaqilotontli/00009-t__a__!_p.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 619310,
                    "size": 435,
                    "blocks": 1,
                    "atimeMs": 1582149188575.88,
                    "mtimeMs": 1582149188576.2961,
                    "ctimeMs": 1582149188576.2961,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.576Z",
                    "mtime": "2020-02-19T21:53:08.576Z",
                    "ctime": "2020-02-19T21:53:08.576Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Pokalqiloli_itik\" data-name=\"Pokalqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 55.57676 69.98151\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "001 Akaqilotontli/00010-t__a__t.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/001 Akaqilotontli/00010-t__a__t.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 619311,
                    "size": 435,
                    "blocks": 1,
                    "atimeMs": 1582149188576.825,
                    "mtimeMs": 1582149188577.238,
                    "ctimeMs": 1582149188577.238,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.577Z",
                    "mtime": "2020-02-19T21:53:08.577Z",
                    "ctime": "2020-02-19T21:53:08.577Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Tekolqiloli_itik\" data-name=\"Tekolqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 70.61133 68.50488\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "001 Akaqilotontli/00011-t__a__!_t.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/001 Akaqilotontli/00011-t__a__!_t.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 619312,
                    "size": 435,
                    "blocks": 1,
                    "atimeMs": 1582149188578.2305,
                    "mtimeMs": 1582149188578.6287,
                    "ctimeMs": 1582149188578.6287,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.578Z",
                    "mtime": "2020-02-19T21:53:08.579Z",
                    "ctime": "2020-02-19T21:53:08.579Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Tekolqiloli_itik\" data-name=\"Tekolqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 70.61133 68.50488\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "001 Akaqilotontli/00012-t__a__tl.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/001 Akaqilotontli/00012-t__a__tl.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 619313,
                    "size": 439,
                    "blocks": 1,
                    "atimeMs": 1582149188579.1577,
                    "mtimeMs": 1582149188579.599,
                    "ctimeMs": 1582149188579.599,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.579Z",
                    "mtime": "2020-02-19T21:53:08.580Z",
                    "ctime": "2020-02-19T21:53:08.580Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Tlachalqiloli_itik\" data-name=\"Tlachalqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 73.98493 69.98828\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "001 Akaqilotontli/00013-t__a__!_tl.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/001 Akaqilotontli/00013-t__a__!_tl.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 619314,
                    "size": 439,
                    "blocks": 1,
                    "atimeMs": 1582149188580.5642,
                    "mtimeMs": 1582149188580.9873,
                    "ctimeMs": 1582149188580.9873,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.581Z",
                    "mtime": "2020-02-19T21:53:08.581Z",
                    "ctime": "2020-02-19T21:53:08.581Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Tlachalqiloli_itik\" data-name=\"Tlachalqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 73.98493 69.98828\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "001 Akaqilotontli/00014-t__a__x.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/001 Akaqilotontli/00014-t__a__x.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 619315,
                    "size": 435,
                    "blocks": 1,
                    "atimeMs": 1582149188581.54,
                    "mtimeMs": 1582149188581.9812,
                    "ctimeMs": 1582149188581.9812,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.582Z",
                    "mtime": "2020-02-19T21:53:08.582Z",
                    "ctime": "2020-02-19T21:53:08.582Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Xocilqiloli_itik\" data-name=\"Xocilqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 58.64222 66.89453\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "001 Akaqilotontli/00015-t__a__!_x.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/001 Akaqilotontli/00015-t__a__!_x.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 619316,
                    "size": 435,
                    "blocks": 1,
                    "atimeMs": 1582149188582.9536,
                    "mtimeMs": 1582149188583.3882,
                    "ctimeMs": 1582149188583.3882,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.583Z",
                    "mtime": "2020-02-19T21:53:08.583Z",
                    "ctime": "2020-02-19T21:53:08.583Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Xocilqiloli_itik\" data-name=\"Xocilqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 58.64222 66.89453\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "001 Akaqilotontli/00016-t__a__c.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/001 Akaqilotontli/00016-t__a__c.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 619317,
                    "size": 437,
                    "blocks": 1,
                    "atimeMs": 1582149188583.91,
                    "mtimeMs": 1582149188584.356,
                    "ctimeMs": 1582149188584.356,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.584Z",
                    "mtime": "2020-02-19T21:53:08.584Z",
                    "ctime": "2020-02-19T21:53:08.584Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Chakalqiloli_itik\" data-name=\"Chakalqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 58.00381 69.90327\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "001 Akaqilotontli/00017-t__a__!_c.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/001 Akaqilotontli/00017-t__a__!_c.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 619318,
                    "size": 437,
                    "blocks": 1,
                    "atimeMs": 1582149188585.345,
                    "mtimeMs": 1582149188585.7944,
                    "ctimeMs": 1582149188585.7944,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.585Z",
                    "mtime": "2020-02-19T21:53:08.586Z",
                    "ctime": "2020-02-19T21:53:08.586Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Chakalqiloli_itik\" data-name=\"Chakalqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 58.00381 69.90327\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "001 Akaqilotontli/00018-t__a__s.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/001 Akaqilotontli/00018-t__a__s.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 619319,
                    "size": 437,
                    "blocks": 1,
                    "atimeMs": 1582149188586.323,
                    "mtimeMs": 1582149188586.7231,
                    "ctimeMs": 1582149188586.7231,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.586Z",
                    "mtime": "2020-02-19T21:53:08.587Z",
                    "ctime": "2020-02-19T21:53:08.587Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Senzonqiloli_itik\" data-name=\"Senzonqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 77.00781 67.02148\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "001 Akaqilotontli/00019-t__a__!_s.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/001 Akaqilotontli/00019-t__a__!_s.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 619321,
                    "size": 437,
                    "blocks": 1,
                    "atimeMs": 1582149188587.701,
                    "mtimeMs": 1582149188588.0908,
                    "ctimeMs": 1582149188588.0908,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.588Z",
                    "mtime": "2020-02-19T21:53:08.588Z",
                    "ctime": "2020-02-19T21:53:08.588Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Senzonqiloli_itik\" data-name=\"Senzonqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 77.00781 67.02148\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "001 Akaqilotontli/00020-t__a__z.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/001 Akaqilotontli/00020-t__a__z.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 619323,
                    "size": 433,
                    "blocks": 1,
                    "atimeMs": 1582149188588.6177,
                    "mtimeMs": 1582149188589.0168,
                    "ctimeMs": 1582149188589.0168,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.589Z",
                    "mtime": "2020-02-19T21:53:08.589Z",
                    "ctime": "2020-02-19T21:53:08.589Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Zopilqiloli_itik\" data-name=\"Zopilqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 77.00781 68.625\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "001 Akaqilotontli/00021-t__a__!_z.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/001 Akaqilotontli/00021-t__a__!_z.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 619324,
                    "size": 433,
                    "blocks": 1,
                    "atimeMs": 1582149188590.0166,
                    "mtimeMs": 1582149188590.4072,
                    "ctimeMs": 1582149188590.4072,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.590Z",
                    "mtime": "2020-02-19T21:53:08.590Z",
                    "ctime": "2020-02-19T21:53:08.590Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Zopilqiloli_itik\" data-name=\"Zopilqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 77.00781 68.625\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "001 Akaqilotontli/00022-t__a__k.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/001 Akaqilotontli/00022-t__a__k.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 619325,
                    "size": 433,
                    "blocks": 1,
                    "atimeMs": 1582149188590.9287,
                    "mtimeMs": 1582149188591.3425,
                    "ctimeMs": 1582149188591.3425,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.591Z",
                    "mtime": "2020-02-19T21:53:08.591Z",
                    "ctime": "2020-02-19T21:53:08.591Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Coalqiloli_itik\" data-name=\"Coalqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 68.84783 69.99839\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "001 Akaqilotontli/00023-t__a__!_k.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/001 Akaqilotontli/00023-t__a__!_k.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 619326,
                    "size": 433,
                    "blocks": 1,
                    "atimeMs": 1582149188592.3086,
                    "mtimeMs": 1582149188592.732,
                    "ctimeMs": 1582149188592.732,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.592Z",
                    "mtime": "2020-02-19T21:53:08.593Z",
                    "ctime": "2020-02-19T21:53:08.593Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Coalqiloli_itik\" data-name=\"Coalqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 68.84783 69.99839\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "001 Akaqilotontli/00024-t__a__q.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/001 Akaqilotontli/00024-t__a__q.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 619328,
                    "size": 435,
                    "blocks": 1,
                    "atimeMs": 1582149188593.3389,
                    "mtimeMs": 1582149188593.7744,
                    "ctimeMs": 1582149188593.7744,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.593Z",
                    "mtime": "2020-02-19T21:53:08.594Z",
                    "ctime": "2020-02-19T21:53:08.594Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Qaqawqiloli_itik\" data-name=\"Qaqawqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 67.13916 68.67383\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "001 Akaqilotontli/00025-t__a__!_q.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/001 Akaqilotontli/00025-t__a__!_q.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 619329,
                    "size": 435,
                    "blocks": 1,
                    "atimeMs": 1582149188594.777,
                    "mtimeMs": 1582149188595.1824,
                    "ctimeMs": 1582149188595.1824,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.595Z",
                    "mtime": "2020-02-19T21:53:08.595Z",
                    "ctime": "2020-02-19T21:53:08.595Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Qaqawqiloli_itik\" data-name=\"Qaqawqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 67.13916 68.67383\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "001 Akaqilotontli/00026-t__a__h.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/001 Akaqilotontli/00026-t__a__h.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 619330,
                    "size": 433,
                    "blocks": 1,
                    "atimeMs": 1582149188595.7065,
                    "mtimeMs": 1582149188596.1272,
                    "ctimeMs": 1582149188596.1272,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.596Z",
                    "mtime": "2020-02-19T21:53:08.596Z",
                    "ctime": "2020-02-19T21:53:08.596Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Hekalqiloli_itik\" data-name=\"Hekalqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 60.0387 63.1527\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "001 Akaqilotontli/00027-t__a__!_h.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/001 Akaqilotontli/00027-t__a__!_h.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 619331,
                    "size": 433,
                    "blocks": 1,
                    "atimeMs": 1582149188597.9988,
                    "mtimeMs": 1582149188598.635,
                    "ctimeMs": 1582149188598.635,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.598Z",
                    "mtime": "2020-02-19T21:53:08.599Z",
                    "ctime": "2020-02-19T21:53:08.599Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Hekalqiloli_itik\" data-name=\"Hekalqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 60.0387 63.1527\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "001 Akaqilotontli/00028-t__a__l.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/001 Akaqilotontli/00028-t__a__l.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 619333,
                    "size": 435,
                    "blocks": 1,
                    "atimeMs": 1582149188599.4888,
                    "mtimeMs": 1582149188599.983,
                    "ctimeMs": 1582149188599.983,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.599Z",
                    "mtime": "2020-02-19T21:53:08.600Z",
                    "ctime": "2020-02-19T21:53:08.600Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Limonqiloli_itik\" data-name=\"Limonqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 56.86193 67.47461\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "001 Akaqilotontli/00029-t__a__!_l.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/001 Akaqilotontli/00029-t__a__!_l.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 619334,
                    "size": 435,
                    "blocks": 1,
                    "atimeMs": 1582149188600.978,
                    "mtimeMs": 1582149188601.4092,
                    "ctimeMs": 1582149188601.4092,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.601Z",
                    "mtime": "2020-02-19T21:53:08.601Z",
                    "ctime": "2020-02-19T21:53:08.601Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Limonqiloli_itik\" data-name=\"Limonqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 56.86193 67.47461\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "001 Akaqilotontli/00030-t__a__w.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/001 Akaqilotontli/00030-t__a__w.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 619336,
                    "size": 435,
                    "blocks": 1,
                    "atimeMs": 1582149188601.947,
                    "mtimeMs": 1582149188602.348,
                    "ctimeMs": 1582149188602.348,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.602Z",
                    "mtime": "2020-02-19T21:53:08.602Z",
                    "ctime": "2020-02-19T21:53:08.602Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Weyilqiloli_itik\" data-name=\"Weyilqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 77.05322 69.88477\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "001 Akaqilotontli/00031-t__a__!_w.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/001 Akaqilotontli/00031-t__a__!_w.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 619337,
                    "size": 435,
                    "blocks": 1,
                    "atimeMs": 1582149188603.3198,
                    "mtimeMs": 1582149188603.7039,
                    "ctimeMs": 1582149188603.7039,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.603Z",
                    "mtime": "2020-02-19T21:53:08.604Z",
                    "ctime": "2020-02-19T21:53:08.604Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Weyilqiloli_itik\" data-name=\"Weyilqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 77.05322 69.88477\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "001 Akaqilotontli/00032-t__a__y.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/001 Akaqilotontli/00032-t__a__y.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 619361,
                    "size": 435,
                    "blocks": 1,
                    "atimeMs": 1582149188604.222,
                    "mtimeMs": 1582149188604.6416,
                    "ctimeMs": 1582149188604.6416,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.604Z",
                    "mtime": "2020-02-19T21:53:08.605Z",
                    "ctime": "2020-02-19T21:53:08.605Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Yololqiloli_itik\" data-name=\"Yololqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 59.96725 67.44629\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "001 Akaqilotontli/00033-t__a__!_y.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/001 Akaqilotontli/00033-t__a__!_y.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 619362,
                    "size": 435,
                    "blocks": 1,
                    "atimeMs": 1582149188605.6257,
                    "mtimeMs": 1582149188606.031,
                    "ctimeMs": 1582149188606.031,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.606Z",
                    "mtime": "2020-02-19T21:53:08.606Z",
                    "ctime": "2020-02-19T21:53:08.606Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Yololqiloli_itik\" data-name=\"Yololqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 59.96725 67.44629\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "001 Akaqilotontli/00034-t__a__b.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/001 Akaqilotontli/00034-t__a__b.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 619363,
                    "size": 435,
                    "blocks": 1,
                    "atimeMs": 1582149188606.5557,
                    "mtimeMs": 1582149188607.0317,
                    "ctimeMs": 1582149188607.0317,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.607Z",
                    "mtime": "2020-02-19T21:53:08.607Z",
                    "ctime": "2020-02-19T21:53:08.607Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Balamqiloli_itik\" data-name=\"Balamqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 75.41407 65.86328\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "001 Akaqilotontli/00035-t__a__!_b.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/001 Akaqilotontli/00035-t__a__!_b.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 619375,
                    "size": 435,
                    "blocks": 1,
                    "atimeMs": 1582149188607.998,
                    "mtimeMs": 1582149188608.4712,
                    "ctimeMs": 1582149188608.4712,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.608Z",
                    "mtime": "2020-02-19T21:53:08.608Z",
                    "ctime": "2020-02-19T21:53:08.608Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Balamqiloli_itik\" data-name=\"Balamqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 75.41407 65.86328\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "001 Akaqilotontli/00036-t__a__d.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/001 Akaqilotontli/00036-t__a__d.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 619378,
                    "size": 435,
                    "blocks": 1,
                    "atimeMs": 1582149188608.9968,
                    "mtimeMs": 1582149188609.4614,
                    "ctimeMs": 1582149188609.4614,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.609Z",
                    "mtime": "2020-02-19T21:53:08.609Z",
                    "ctime": "2020-02-19T21:53:08.609Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Dunilqiloli_itik\" data-name=\"Dunilqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 70.61133 68.50488\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "001 Akaqilotontli/00037-t__a__!_d.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/001 Akaqilotontli/00037-t__a__!_d.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 619380,
                    "size": 435,
                    "blocks": 1,
                    "atimeMs": 1582149188610.4348,
                    "mtimeMs": 1582149188610.871,
                    "ctimeMs": 1582149188610.871,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.610Z",
                    "mtime": "2020-02-19T21:53:08.611Z",
                    "ctime": "2020-02-19T21:53:08.611Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Dunilqiloli_itik\" data-name=\"Dunilqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 70.61133 68.50488\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "001 Akaqilotontli/00038-t__a__f.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/001 Akaqilotontli/00038-t__a__f.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 619416,
                    "size": 437,
                    "blocks": 1,
                    "atimeMs": 1582149188611.407,
                    "mtimeMs": 1582149188611.854,
                    "ctimeMs": 1582149188611.854,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.611Z",
                    "mtime": "2020-02-19T21:53:08.612Z",
                    "ctime": "2020-02-19T21:53:08.612Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Thamilqiloli_itik\" data-name=\"Thamilqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 55.57676 69.98151\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "001 Akaqilotontli/00039-t__a__!_f.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/001 Akaqilotontli/00039-t__a__!_f.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 619417,
                    "size": 437,
                    "blocks": 1,
                    "atimeMs": 1582149188612.827,
                    "mtimeMs": 1582149188613.2764,
                    "ctimeMs": 1582149188613.2764,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.613Z",
                    "mtime": "2020-02-19T21:53:08.613Z",
                    "ctime": "2020-02-19T21:53:08.613Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Thamilqiloli_itik\" data-name=\"Thamilqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 55.57676 69.98151\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "001 Akaqilotontli/00040-t__a__g.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/001 Akaqilotontli/00040-t__a__g.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 619418,
                    "size": 435,
                    "blocks": 1,
                    "atimeMs": 1582149188613.96,
                    "mtimeMs": 1582149188614.4175,
                    "ctimeMs": 1582149188614.4175,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.614Z",
                    "mtime": "2020-02-19T21:53:08.614Z",
                    "ctime": "2020-02-19T21:53:08.614Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Galilqiloli_itik\" data-name=\"Galilqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 68.84685 69.99854\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "001 Akaqilotontli/00041-t__a__!_g.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/001 Akaqilotontli/00041-t__a__!_g.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 619421,
                    "size": 435,
                    "blocks": 1,
                    "atimeMs": 1582149188615.3938,
                    "mtimeMs": 1582149188615.819,
                    "ctimeMs": 1582149188615.819,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.615Z",
                    "mtime": "2020-02-19T21:53:08.616Z",
                    "ctime": "2020-02-19T21:53:08.616Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Galilqiloli_itik\" data-name=\"Galilqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 68.84685 69.99854\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "001 Akaqilotontli/00042-t__a__kh.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/001 Akaqilotontli/00042-t__a__kh.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 619439,
                    "size": 437,
                    "blocks": 1,
                    "atimeMs": 1582149188616.3406,
                    "mtimeMs": 1582149188616.7773,
                    "ctimeMs": 1582149188616.7773,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.616Z",
                    "mtime": "2020-02-19T21:53:08.617Z",
                    "ctime": "2020-02-19T21:53:08.617Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Khamalqiloli_itik\" data-name=\"Khamalqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 65.60791 63.15566\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "001 Akaqilotontli/00043-t__a__!_kh.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/001 Akaqilotontli/00043-t__a__!_kh.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 619443,
                    "size": 437,
                    "blocks": 1,
                    "atimeMs": 1582149188617.7668,
                    "mtimeMs": 1582149188618.2317,
                    "ctimeMs": 1582149188618.2317,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.618Z",
                    "mtime": "2020-02-19T21:53:08.618Z",
                    "ctime": "2020-02-19T21:53:08.618Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Khamalqiloli_itik\" data-name=\"Khamalqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 65.60791 63.15566\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "001 Akaqilotontli/00044-t__a__j.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/001 Akaqilotontli/00044-t__a__j.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 619796,
                    "size": 435,
                    "blocks": 1,
                    "atimeMs": 1582149188618.7617,
                    "mtimeMs": 1582149188619.2004,
                    "ctimeMs": 1582149188619.2004,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.619Z",
                    "mtime": "2020-02-19T21:53:08.619Z",
                    "ctime": "2020-02-19T21:53:08.619Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Jokilqiloli_itik\" data-name=\"Jokilqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 66.64662 67.44434\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "001 Akaqilotontli/00045-t__a__!_j.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/001 Akaqilotontli/00045-t__a__!_j.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 619802,
                    "size": 435,
                    "blocks": 1,
                    "atimeMs": 1582149188620.1653,
                    "mtimeMs": 1582149188620.596,
                    "ctimeMs": 1582149188620.596,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.620Z",
                    "mtime": "2020-02-19T21:53:08.621Z",
                    "ctime": "2020-02-19T21:53:08.621Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Jokilqiloli_itik\" data-name=\"Jokilqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 66.64662 67.44434\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "001 Akaqilotontli/00046-t__a__r.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/001 Akaqilotontli/00046-t__a__r.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 619805,
                    "size": 435,
                    "blocks": 1,
                    "atimeMs": 1582149188621.12,
                    "mtimeMs": 1582149188624.6455,
                    "ctimeMs": 1582149188624.6455,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.621Z",
                    "mtime": "2020-02-19T21:53:08.625Z",
                    "ctime": "2020-02-19T21:53:08.625Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Runalqiloli_itik\" data-name=\"Runalqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 69.46289 67.47461\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "001 Akaqilotontli/00047-t__a__!_r.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/001 Akaqilotontli/00047-t__a__!_r.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 619806,
                    "size": 435,
                    "blocks": 1,
                    "atimeMs": 1582149188625.694,
                    "mtimeMs": 1582149188626.3801,
                    "ctimeMs": 1582149188626.3801,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.626Z",
                    "mtime": "2020-02-19T21:53:08.626Z",
                    "ctime": "2020-02-19T21:53:08.626Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Runalqiloli_itik\" data-name=\"Runalqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 69.46289 67.47461\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "001 Akaqilotontli/00048-t__a__z'.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/001 Akaqilotontli/00048-t__a__z'.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 619935,
                    "size": 435,
                    "blocks": 1,
                    "atimeMs": 1582149188626.923,
                    "mtimeMs": 1582149188627.3474,
                    "ctimeMs": 1582149188627.3474,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.627Z",
                    "mtime": "2020-02-19T21:53:08.627Z",
                    "ctime": "2020-02-19T21:53:08.627Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Zawilqiloli_itik\" data-name=\"Zawilqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 76.99609 66.84766\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "001 Akaqilotontli/00049-t__a__!_z'.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/001 Akaqilotontli/00049-t__a__!_z'.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 619983,
                    "size": 435,
                    "blocks": 1,
                    "atimeMs": 1582149188628.3215,
                    "mtimeMs": 1582149188628.733,
                    "ctimeMs": 1582149188628.733,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.628Z",
                    "mtime": "2020-02-19T21:53:08.629Z",
                    "ctime": "2020-02-19T21:53:08.629Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Zawilqiloli_itik\" data-name=\"Zawilqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 76.99609 66.84766\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "001 Akaqilotontli/00050-t__a__v.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/001 Akaqilotontli/00050-t__a__v.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 619997,
                    "size": 435,
                    "blocks": 1,
                    "atimeMs": 1582149188629.266,
                    "mtimeMs": 1582149188629.786,
                    "ctimeMs": 1582149188629.786,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.629Z",
                    "mtime": "2020-02-19T21:53:08.630Z",
                    "ctime": "2020-02-19T21:53:08.630Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Verolqiloli_itik\" data-name=\"Verolqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 77.05322 69.88477\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "001 Akaqilotontli/00051-t__a__!_v.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/001 Akaqilotontli/00051-t__a__!_v.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 619998,
                    "size": 435,
                    "blocks": 1,
                    "atimeMs": 1582149188630.897,
                    "mtimeMs": 1582149188631.396,
                    "ctimeMs": 1582149188631.396,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.631Z",
                    "mtime": "2020-02-19T21:53:08.631Z",
                    "ctime": "2020-02-19T21:53:08.631Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Verolqiloli_itik\" data-name=\"Verolqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 77.05322 69.88477\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "001 Akaqilotontli/00052-t__a__ks.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/001 Akaqilotontli/00052-t__a__ks.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 620004,
                    "size": 437,
                    "blocks": 1,
                    "atimeMs": 1582149188631.9373,
                    "mtimeMs": 1582149188632.3652,
                    "ctimeMs": 1582149188632.3652,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.632Z",
                    "mtime": "2020-02-19T21:53:08.632Z",
                    "ctime": "2020-02-19T21:53:08.632Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Ksimalqiloli_itik\" data-name=\"Ksimalqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 76.99609 68.44629\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "001 Akaqilotontli/00053-t__a__!_ks.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/001 Akaqilotontli/00053-t__a__!_ks.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 620005,
                    "size": 437,
                    "blocks": 1,
                    "atimeMs": 1582149188633.375,
                    "mtimeMs": 1582149188633.8213,
                    "ctimeMs": 1582149188633.8213,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.633Z",
                    "mtime": "2020-02-19T21:53:08.634Z",
                    "ctime": "2020-02-19T21:53:08.634Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Ksimalqiloli_itik\" data-name=\"Ksimalqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 76.99609 68.44629\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "001 Akaqilotontli/00054-t__a__ny.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/001 Akaqilotontli/00054-t__a__ny.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 620006,
                    "size": 437,
                    "blocks": 1,
                    "atimeMs": 1582149188634.4253,
                    "mtimeMs": 1582149188634.8618,
                    "ctimeMs": 1582149188634.8618,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.634Z",
                    "mtime": "2020-02-19T21:53:08.635Z",
                    "ctime": "2020-02-19T21:53:08.635Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Nyonilqiloli_itik\" data-name=\"Nyonilqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 75.40479 65.39844\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "001 Akaqilotontli/00055-t__a__!_ny.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/001 Akaqilotontli/00055-t__a__!_ny.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 620007,
                    "size": 437,
                    "blocks": 1,
                    "atimeMs": 1582149188635.8467,
                    "mtimeMs": 1582149188636.267,
                    "ctimeMs": 1582149188636.267,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.636Z",
                    "mtime": "2020-02-19T21:53:08.636Z",
                    "ctime": "2020-02-19T21:53:08.636Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Nyonilqiloli_itik\" data-name=\"Nyonilqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 75.40479 65.39844\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "002 Otonqilotontli/00056-t_e.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/002 Otonqilotontli/00056-t_e.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 620014,
                    "size": 401,
                    "blocks": 1,
                    "atimeMs": 1582149112327.552,
                    "mtimeMs": 1582149188636.837,
                    "ctimeMs": 1582149188636.837,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:51:52.328Z",
                    "mtime": "2020-02-19T21:53:08.637Z",
                    "ctime": "2020-02-19T21:53:08.637Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Tekolqiloli\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 90.71191 98.22174\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "002 Otonqilotontli/00057-t_e'.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/002 Otonqilotontli/00057-t_e'.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 620015,
                    "size": 425,
                    "blocks": 1,
                    "atimeMs": 1582149188637.4424,
                    "mtimeMs": 1582149188637.8171,
                    "ctimeMs": 1582149188637.8171,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.637Z",
                    "mtime": "2020-02-19T21:53:08.638Z",
                    "ctime": "2020-02-19T21:53:08.638Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Hekalqilotontli_1\" data-name=\"Hekalqilotontli 1\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 33 33\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "002 Otonqilotontli/00058-t_e!.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/002 Otonqilotontli/00058-t_e!.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 620034,
                    "size": 416,
                    "blocks": 1,
                    "atimeMs": 1582149188638.3792,
                    "mtimeMs": 1582149188638.738,
                    "ctimeMs": 1582149188638.738,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.638Z",
                    "mtime": "2020-02-19T21:53:08.639Z",
                    "ctime": "2020-02-19T21:53:08.639Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Weyakqiloli_1\" data-name=\"Weyakqiloli 1\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 80 6\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "002 Otonqilotontli/00059-t_e!_'.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/002 Otonqilotontli/00059-t_e!_'.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 620036,
                    "size": 425,
                    "blocks": 1,
                    "atimeMs": 1582149188639.7314,
                    "mtimeMs": 1582149188640.127,
                    "ctimeMs": 1582149188640.127,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.640Z",
                    "mtime": "2020-02-19T21:53:08.640Z",
                    "ctime": "2020-02-19T21:53:08.640Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Hekalqilotontli_1\" data-name=\"Hekalqilotontli 1\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 33 33\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "002 Otonqilotontli/00060-t__e__m.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/002 Otonqilotontli/00060-t__e__m.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 620037,
                    "size": 433,
                    "blocks": 1,
                    "atimeMs": 1582149188640.6992,
                    "mtimeMs": 1582149188641.1455,
                    "ctimeMs": 1582149188641.1455,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.641Z",
                    "mtime": "2020-02-19T21:53:08.641Z",
                    "ctime": "2020-02-19T21:53:08.641Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Mailqiloli_itik\" data-name=\"Mailqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 75.41407 65.86328\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "002 Otonqilotontli/00061-t__e__!_m.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/002 Otonqilotontli/00061-t__e__!_m.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 620038,
                    "size": 433,
                    "blocks": 1,
                    "atimeMs": 1582149188642.3645,
                    "mtimeMs": 1582149188642.8196,
                    "ctimeMs": 1582149188642.8196,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.642Z",
                    "mtime": "2020-02-19T21:53:08.643Z",
                    "ctime": "2020-02-19T21:53:08.643Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Mailqiloli_itik\" data-name=\"Mailqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 75.41407 65.86328\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "002 Otonqilotontli/00062-t__e__n.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/002 Otonqilotontli/00062-t__e__n.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 620039,
                    "size": 434,
                    "blocks": 1,
                    "atimeMs": 1582149188643.422,
                    "mtimeMs": 1582149188643.8308,
                    "ctimeMs": 1582149188643.8308,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.643Z",
                    "mtime": "2020-02-19T21:53:08.644Z",
                    "ctime": "2020-02-19T21:53:08.644Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Neliwqiloli_itik\" data-name=\"Neliwqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 65.4248 65.38281\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "002 Otonqilotontli/00063-t__e__!_n.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/002 Otonqilotontli/00063-t__e__!_n.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 620044,
                    "size": 434,
                    "blocks": 1,
                    "atimeMs": 1582149188644.8643,
                    "mtimeMs": 1582149188645.273,
                    "ctimeMs": 1582149188645.273,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.645Z",
                    "mtime": "2020-02-19T21:53:08.645Z",
                    "ctime": "2020-02-19T21:53:08.645Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Neliwqiloli_itik\" data-name=\"Neliwqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 65.4248 65.38281\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "002 Otonqilotontli/00064-t__e__p.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/002 Otonqilotontli/00064-t__e__p.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 620045,
                    "size": 435,
                    "blocks": 1,
                    "atimeMs": 1582149188645.8533,
                    "mtimeMs": 1582149188646.2778,
                    "ctimeMs": 1582149188646.2778,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.646Z",
                    "mtime": "2020-02-19T21:53:08.646Z",
                    "ctime": "2020-02-19T21:53:08.646Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Pokalqiloli_itik\" data-name=\"Pokalqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 55.57676 69.98151\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "002 Otonqilotontli/00065-t__e__!_p.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/002 Otonqilotontli/00065-t__e__!_p.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 620046,
                    "size": 435,
                    "blocks": 1,
                    "atimeMs": 1582149188647.377,
                    "mtimeMs": 1582149188647.8113,
                    "ctimeMs": 1582149188647.8113,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.647Z",
                    "mtime": "2020-02-19T21:53:08.648Z",
                    "ctime": "2020-02-19T21:53:08.648Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Pokalqiloli_itik\" data-name=\"Pokalqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 55.57676 69.98151\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "002 Otonqilotontli/00066-t__e__t.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/002 Otonqilotontli/00066-t__e__t.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 620047,
                    "size": 435,
                    "blocks": 1,
                    "atimeMs": 1582149188648.4114,
                    "mtimeMs": 1582149188648.8123,
                    "ctimeMs": 1582149188648.8123,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.648Z",
                    "mtime": "2020-02-19T21:53:08.649Z",
                    "ctime": "2020-02-19T21:53:08.649Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Tekolqiloli_itik\" data-name=\"Tekolqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 70.61133 68.50488\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "002 Otonqilotontli/00067-t__e__!_t.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/002 Otonqilotontli/00067-t__e__!_t.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 620050,
                    "size": 435,
                    "blocks": 1,
                    "atimeMs": 1582149188649.8618,
                    "mtimeMs": 1582149188650.2605,
                    "ctimeMs": 1582149188650.2605,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.650Z",
                    "mtime": "2020-02-19T21:53:08.650Z",
                    "ctime": "2020-02-19T21:53:08.650Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Tekolqiloli_itik\" data-name=\"Tekolqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 70.61133 68.50488\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "002 Otonqilotontli/00068-t__e__tl.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/002 Otonqilotontli/00068-t__e__tl.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 620053,
                    "size": 439,
                    "blocks": 1,
                    "atimeMs": 1582149188650.8381,
                    "mtimeMs": 1582149188651.2766,
                    "ctimeMs": 1582149188651.2766,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.651Z",
                    "mtime": "2020-02-19T21:53:08.651Z",
                    "ctime": "2020-02-19T21:53:08.651Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Tlachalqiloli_itik\" data-name=\"Tlachalqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 73.98493 69.98828\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "002 Otonqilotontli/00069-t__e__!_tl.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/002 Otonqilotontli/00069-t__e__!_tl.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 620054,
                    "size": 439,
                    "blocks": 1,
                    "atimeMs": 1582149188652.2998,
                    "mtimeMs": 1582149188652.7214,
                    "ctimeMs": 1582149188652.7214,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.652Z",
                    "mtime": "2020-02-19T21:53:08.653Z",
                    "ctime": "2020-02-19T21:53:08.653Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Tlachalqiloli_itik\" data-name=\"Tlachalqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 73.98493 69.98828\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "002 Otonqilotontli/00070-t__e__x.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/002 Otonqilotontli/00070-t__e__x.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 620055,
                    "size": 435,
                    "blocks": 1,
                    "atimeMs": 1582149188653.3298,
                    "mtimeMs": 1582149188653.756,
                    "ctimeMs": 1582149188653.756,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.653Z",
                    "mtime": "2020-02-19T21:53:08.654Z",
                    "ctime": "2020-02-19T21:53:08.654Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Xocilqiloli_itik\" data-name=\"Xocilqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 58.64222 66.89453\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "002 Otonqilotontli/00071-t__e__!_x.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/002 Otonqilotontli/00071-t__e__!_x.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 620059,
                    "size": 435,
                    "blocks": 1,
                    "atimeMs": 1582149188654.7434,
                    "mtimeMs": 1582149188655.166,
                    "ctimeMs": 1582149188655.166,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.655Z",
                    "mtime": "2020-02-19T21:53:08.655Z",
                    "ctime": "2020-02-19T21:53:08.655Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Xocilqiloli_itik\" data-name=\"Xocilqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 58.64222 66.89453\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "002 Otonqilotontli/00072-t__e__c.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/002 Otonqilotontli/00072-t__e__c.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 620060,
                    "size": 437,
                    "blocks": 1,
                    "atimeMs": 1582149188655.724,
                    "mtimeMs": 1582149188656.2554,
                    "ctimeMs": 1582149188656.2554,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.656Z",
                    "mtime": "2020-02-19T21:53:08.656Z",
                    "ctime": "2020-02-19T21:53:08.656Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Chakalqiloli_itik\" data-name=\"Chakalqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 58.00381 69.90327\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "002 Otonqilotontli/00073-t__e__!_c.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/002 Otonqilotontli/00073-t__e__!_c.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 620061,
                    "size": 437,
                    "blocks": 1,
                    "atimeMs": 1582149188657.3062,
                    "mtimeMs": 1582149188657.74,
                    "ctimeMs": 1582149188657.74,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.657Z",
                    "mtime": "2020-02-19T21:53:08.658Z",
                    "ctime": "2020-02-19T21:53:08.658Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Chakalqiloli_itik\" data-name=\"Chakalqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 58.00381 69.90327\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "002 Otonqilotontli/00074-t__e__s.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/002 Otonqilotontli/00074-t__e__s.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 620062,
                    "size": 437,
                    "blocks": 1,
                    "atimeMs": 1582149188658.3132,
                    "mtimeMs": 1582149188658.704,
                    "ctimeMs": 1582149188658.704,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.658Z",
                    "mtime": "2020-02-19T21:53:08.659Z",
                    "ctime": "2020-02-19T21:53:08.659Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Senzonqiloli_itik\" data-name=\"Senzonqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 77.00781 67.02148\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "002 Otonqilotontli/00075-t__e__!_s.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/002 Otonqilotontli/00075-t__e__!_s.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 620069,
                    "size": 437,
                    "blocks": 1,
                    "atimeMs": 1582149188659.948,
                    "mtimeMs": 1582149188660.3296,
                    "ctimeMs": 1582149188660.3296,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.660Z",
                    "mtime": "2020-02-19T21:53:08.660Z",
                    "ctime": "2020-02-19T21:53:08.660Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Senzonqiloli_itik\" data-name=\"Senzonqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 77.00781 67.02148\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "002 Otonqilotontli/00076-t__e__z.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/002 Otonqilotontli/00076-t__e__z.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 620144,
                    "size": 433,
                    "blocks": 1,
                    "atimeMs": 1582149188660.8977,
                    "mtimeMs": 1582149188661.3213,
                    "ctimeMs": 1582149188661.3213,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.661Z",
                    "mtime": "2020-02-19T21:53:08.661Z",
                    "ctime": "2020-02-19T21:53:08.661Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Zopilqiloli_itik\" data-name=\"Zopilqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 77.00781 68.625\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "002 Otonqilotontli/00077-t__e__!_z.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/002 Otonqilotontli/00077-t__e__!_z.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 620160,
                    "size": 433,
                    "blocks": 1,
                    "atimeMs": 1582149188662.3462,
                    "mtimeMs": 1582149188662.7537,
                    "ctimeMs": 1582149188662.7537,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.662Z",
                    "mtime": "2020-02-19T21:53:08.663Z",
                    "ctime": "2020-02-19T21:53:08.663Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Zopilqiloli_itik\" data-name=\"Zopilqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 77.00781 68.625\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "002 Otonqilotontli/00078-t__e__k.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/002 Otonqilotontli/00078-t__e__k.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 620384,
                    "size": 433,
                    "blocks": 1,
                    "atimeMs": 1582149188664.3525,
                    "mtimeMs": 1582149188664.817,
                    "ctimeMs": 1582149188664.817,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.664Z",
                    "mtime": "2020-02-19T21:53:08.665Z",
                    "ctime": "2020-02-19T21:53:08.665Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Coalqiloli_itik\" data-name=\"Coalqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 68.84783 69.99839\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "002 Otonqilotontli/00079-t__e__!_k.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/002 Otonqilotontli/00079-t__e__!_k.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 620431,
                    "size": 433,
                    "blocks": 1,
                    "atimeMs": 1582149188665.8953,
                    "mtimeMs": 1582149188666.3079,
                    "ctimeMs": 1582149188666.3079,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.666Z",
                    "mtime": "2020-02-19T21:53:08.666Z",
                    "ctime": "2020-02-19T21:53:08.666Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Coalqiloli_itik\" data-name=\"Coalqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 68.84783 69.99839\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "002 Otonqilotontli/00080-t__e__q.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/002 Otonqilotontli/00080-t__e__q.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 620699,
                    "size": 435,
                    "blocks": 1,
                    "atimeMs": 1582149188666.8958,
                    "mtimeMs": 1582149188667.3066,
                    "ctimeMs": 1582149188667.3066,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.667Z",
                    "mtime": "2020-02-19T21:53:08.667Z",
                    "ctime": "2020-02-19T21:53:08.667Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Qaqawqiloli_itik\" data-name=\"Qaqawqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 67.13916 68.67383\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "002 Otonqilotontli/00081-t__e__!_q.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/002 Otonqilotontli/00081-t__e__!_q.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 620704,
                    "size": 435,
                    "blocks": 1,
                    "atimeMs": 1582149188668.342,
                    "mtimeMs": 1582149188668.7422,
                    "ctimeMs": 1582149188668.7422,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.668Z",
                    "mtime": "2020-02-19T21:53:08.669Z",
                    "ctime": "2020-02-19T21:53:08.669Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Qaqawqiloli_itik\" data-name=\"Qaqawqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 67.13916 68.67383\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "002 Otonqilotontli/00082-t__e__h.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/002 Otonqilotontli/00082-t__e__h.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 620841,
                    "size": 433,
                    "blocks": 1,
                    "atimeMs": 1582149188669.3467,
                    "mtimeMs": 1582149188669.767,
                    "ctimeMs": 1582149188669.767,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.669Z",
                    "mtime": "2020-02-19T21:53:08.670Z",
                    "ctime": "2020-02-19T21:53:08.670Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Hekalqiloli_itik\" data-name=\"Hekalqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 60.0387 63.1527\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "002 Otonqilotontli/00083-t__e__!_h.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/002 Otonqilotontli/00083-t__e__!_h.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 620998,
                    "size": 433,
                    "blocks": 1,
                    "atimeMs": 1582149188671.1636,
                    "mtimeMs": 1582149188671.6074,
                    "ctimeMs": 1582149188671.6074,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.671Z",
                    "mtime": "2020-02-19T21:53:08.672Z",
                    "ctime": "2020-02-19T21:53:08.672Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Hekalqiloli_itik\" data-name=\"Hekalqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 60.0387 63.1527\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "002 Otonqilotontli/00084-t__e__l.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/002 Otonqilotontli/00084-t__e__l.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 620999,
                    "size": 435,
                    "blocks": 1,
                    "atimeMs": 1582149188672.2583,
                    "mtimeMs": 1582149188672.668,
                    "ctimeMs": 1582149188672.668,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.672Z",
                    "mtime": "2020-02-19T21:53:08.673Z",
                    "ctime": "2020-02-19T21:53:08.673Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Limonqiloli_itik\" data-name=\"Limonqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 56.86193 67.47461\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "002 Otonqilotontli/00085-t__e__!_l.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/002 Otonqilotontli/00085-t__e__!_l.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 621024,
                    "size": 435,
                    "blocks": 1,
                    "atimeMs": 1582149188673.6938,
                    "mtimeMs": 1582149188674.0925,
                    "ctimeMs": 1582149188674.0925,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.674Z",
                    "mtime": "2020-02-19T21:53:08.674Z",
                    "ctime": "2020-02-19T21:53:08.674Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Limonqiloli_itik\" data-name=\"Limonqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 56.86193 67.47461\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "002 Otonqilotontli/00086-t__e__w.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/002 Otonqilotontli/00086-t__e__w.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 621084,
                    "size": 435,
                    "blocks": 1,
                    "atimeMs": 1582149188674.7446,
                    "mtimeMs": 1582149188675.2578,
                    "ctimeMs": 1582149188675.2578,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.675Z",
                    "mtime": "2020-02-19T21:53:08.675Z",
                    "ctime": "2020-02-19T21:53:08.675Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Weyilqiloli_itik\" data-name=\"Weyilqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 77.05322 69.88477\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "002 Otonqilotontli/00087-t__e__!_w.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/002 Otonqilotontli/00087-t__e__!_w.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 621101,
                    "size": 435,
                    "blocks": 1,
                    "atimeMs": 1582149188677.2583,
                    "mtimeMs": 1582149188678.0095,
                    "ctimeMs": 1582149188678.0095,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.677Z",
                    "mtime": "2020-02-19T21:53:08.678Z",
                    "ctime": "2020-02-19T21:53:08.678Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Weyilqiloli_itik\" data-name=\"Weyilqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 77.05322 69.88477\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "002 Otonqilotontli/00088-t__e__y.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/002 Otonqilotontli/00088-t__e__y.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 621102,
                    "size": 435,
                    "blocks": 1,
                    "atimeMs": 1582149188678.9685,
                    "mtimeMs": 1582149188679.7542,
                    "ctimeMs": 1582149188679.7542,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.679Z",
                    "mtime": "2020-02-19T21:53:08.680Z",
                    "ctime": "2020-02-19T21:53:08.680Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Yololqiloli_itik\" data-name=\"Yololqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 59.96725 67.44629\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "002 Otonqilotontli/00089-t__e__!_y.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/002 Otonqilotontli/00089-t__e__!_y.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 621103,
                    "size": 435,
                    "blocks": 1,
                    "atimeMs": 1582149188681.2966,
                    "mtimeMs": 1582149188681.9458,
                    "ctimeMs": 1582149188681.9458,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.681Z",
                    "mtime": "2020-02-19T21:53:08.682Z",
                    "ctime": "2020-02-19T21:53:08.682Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Yololqiloli_itik\" data-name=\"Yololqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 59.96725 67.44629\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "002 Otonqilotontli/00090-t__e__b.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/002 Otonqilotontli/00090-t__e__b.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 621105,
                    "size": 435,
                    "blocks": 1,
                    "atimeMs": 1582149188682.8362,
                    "mtimeMs": 1582149188683.569,
                    "ctimeMs": 1582149188683.569,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.683Z",
                    "mtime": "2020-02-19T21:53:08.684Z",
                    "ctime": "2020-02-19T21:53:08.684Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Balamqiloli_itik\" data-name=\"Balamqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 75.41407 65.86328\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "002 Otonqilotontli/00091-t__e__!_b.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/002 Otonqilotontli/00091-t__e__!_b.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 621108,
                    "size": 435,
                    "blocks": 1,
                    "atimeMs": 1582149188684.9817,
                    "mtimeMs": 1582149188685.8062,
                    "ctimeMs": 1582149188685.8062,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.685Z",
                    "mtime": "2020-02-19T21:53:08.686Z",
                    "ctime": "2020-02-19T21:53:08.686Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Balamqiloli_itik\" data-name=\"Balamqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 75.41407 65.86328\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "002 Otonqilotontli/00092-t__e__d.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/002 Otonqilotontli/00092-t__e__d.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 621109,
                    "size": 435,
                    "blocks": 1,
                    "atimeMs": 1582149188686.7263,
                    "mtimeMs": 1582149188687.4182,
                    "ctimeMs": 1582149188687.4182,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.687Z",
                    "mtime": "2020-02-19T21:53:08.687Z",
                    "ctime": "2020-02-19T21:53:08.687Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Dunilqiloli_itik\" data-name=\"Dunilqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 70.61133 68.50488\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "002 Otonqilotontli/00093-t__e__!_d.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/002 Otonqilotontli/00093-t__e__!_d.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 621110,
                    "size": 435,
                    "blocks": 1,
                    "atimeMs": 1582149188689.1287,
                    "mtimeMs": 1582149188689.5723,
                    "ctimeMs": 1582149188689.5723,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.689Z",
                    "mtime": "2020-02-19T21:53:08.690Z",
                    "ctime": "2020-02-19T21:53:08.690Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Dunilqiloli_itik\" data-name=\"Dunilqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 70.61133 68.50488\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "002 Otonqilotontli/00094-t__e__f.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/002 Otonqilotontli/00094-t__e__f.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 621111,
                    "size": 437,
                    "blocks": 1,
                    "atimeMs": 1582149188690.145,
                    "mtimeMs": 1582149188690.5837,
                    "ctimeMs": 1582149188690.5837,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.690Z",
                    "mtime": "2020-02-19T21:53:08.691Z",
                    "ctime": "2020-02-19T21:53:08.691Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Thamilqiloli_itik\" data-name=\"Thamilqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 55.57676 69.98151\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "002 Otonqilotontli/00095-t__e__!_f.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/002 Otonqilotontli/00095-t__e__!_f.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 621138,
                    "size": 437,
                    "blocks": 1,
                    "atimeMs": 1582149188691.5583,
                    "mtimeMs": 1582149188691.9873,
                    "ctimeMs": 1582149188691.9873,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.692Z",
                    "mtime": "2020-02-19T21:53:08.692Z",
                    "ctime": "2020-02-19T21:53:08.692Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Thamilqiloli_itik\" data-name=\"Thamilqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 55.57676 69.98151\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "002 Otonqilotontli/00096-t__e__g.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/002 Otonqilotontli/00096-t__e__g.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 621139,
                    "size": 435,
                    "blocks": 1,
                    "atimeMs": 1582149188692.53,
                    "mtimeMs": 1582149188692.956,
                    "ctimeMs": 1582149188692.956,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.693Z",
                    "mtime": "2020-02-19T21:53:08.693Z",
                    "ctime": "2020-02-19T21:53:08.693Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Galilqiloli_itik\" data-name=\"Galilqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 68.84685 69.99854\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "002 Otonqilotontli/00097-t__e__!_g.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/002 Otonqilotontli/00097-t__e__!_g.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 621140,
                    "size": 435,
                    "blocks": 1,
                    "atimeMs": 1582149188693.928,
                    "mtimeMs": 1582149188694.3206,
                    "ctimeMs": 1582149188694.3206,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.694Z",
                    "mtime": "2020-02-19T21:53:08.694Z",
                    "ctime": "2020-02-19T21:53:08.694Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Galilqiloli_itik\" data-name=\"Galilqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 68.84685 69.99854\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "002 Otonqilotontli/00098-t__e__kh.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/002 Otonqilotontli/00098-t__e__kh.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 621141,
                    "size": 437,
                    "blocks": 1,
                    "atimeMs": 1582149188695.0676,
                    "mtimeMs": 1582149188695.6223,
                    "ctimeMs": 1582149188695.6223,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.695Z",
                    "mtime": "2020-02-19T21:53:08.696Z",
                    "ctime": "2020-02-19T21:53:08.696Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Khamalqiloli_itik\" data-name=\"Khamalqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 65.60791 63.15566\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "002 Otonqilotontli/00099-t__e__!_kh.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/002 Otonqilotontli/00099-t__e__!_kh.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 621152,
                    "size": 437,
                    "blocks": 1,
                    "atimeMs": 1582149188696.6116,
                    "mtimeMs": 1582149188697.0276,
                    "ctimeMs": 1582149188697.0276,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.697Z",
                    "mtime": "2020-02-19T21:53:08.697Z",
                    "ctime": "2020-02-19T21:53:08.697Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Khamalqiloli_itik\" data-name=\"Khamalqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 65.60791 63.15566\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "002 Otonqilotontli/00100-t__e__j.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/002 Otonqilotontli/00100-t__e__j.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 621153,
                    "size": 435,
                    "blocks": 1,
                    "atimeMs": 1582149188697.6238,
                    "mtimeMs": 1582149188698.034,
                    "ctimeMs": 1582149188698.034,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.698Z",
                    "mtime": "2020-02-19T21:53:08.698Z",
                    "ctime": "2020-02-19T21:53:08.698Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Jokilqiloli_itik\" data-name=\"Jokilqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 66.64662 67.44434\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "002 Otonqilotontli/00101-t__e__!_j.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/002 Otonqilotontli/00101-t__e__!_j.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 621154,
                    "size": 435,
                    "blocks": 1,
                    "atimeMs": 1582149188699.5012,
                    "mtimeMs": 1582149188699.9497,
                    "ctimeMs": 1582149188699.9497,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.700Z",
                    "mtime": "2020-02-19T21:53:08.700Z",
                    "ctime": "2020-02-19T21:53:08.700Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Jokilqiloli_itik\" data-name=\"Jokilqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 66.64662 67.44434\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "002 Otonqilotontli/00102-t__e__r.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/002 Otonqilotontli/00102-t__e__r.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 621237,
                    "size": 435,
                    "blocks": 1,
                    "atimeMs": 1582149188700.5576,
                    "mtimeMs": 1582149188706.8032,
                    "ctimeMs": 1582149188706.8032,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.701Z",
                    "mtime": "2020-02-19T21:53:08.707Z",
                    "ctime": "2020-02-19T21:53:08.707Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Runalqiloli_itik\" data-name=\"Runalqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 69.46289 67.47461\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "002 Otonqilotontli/00103-t__e__!_r.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/002 Otonqilotontli/00103-t__e__!_r.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 621238,
                    "size": 435,
                    "blocks": 1,
                    "atimeMs": 1582149188708.2676,
                    "mtimeMs": 1582149188708.8672,
                    "ctimeMs": 1582149188708.8672,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.708Z",
                    "mtime": "2020-02-19T21:53:08.709Z",
                    "ctime": "2020-02-19T21:53:08.709Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Runalqiloli_itik\" data-name=\"Runalqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 69.46289 67.47461\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "002 Otonqilotontli/00104-t__e__z'.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/002 Otonqilotontli/00104-t__e__z'.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 621239,
                    "size": 435,
                    "blocks": 1,
                    "atimeMs": 1582149188709.6414,
                    "mtimeMs": 1582149188710.2048,
                    "ctimeMs": 1582149188710.2048,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.710Z",
                    "mtime": "2020-02-19T21:53:08.710Z",
                    "ctime": "2020-02-19T21:53:08.710Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Zawilqiloli_itik\" data-name=\"Zawilqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 76.99609 66.84766\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "002 Otonqilotontli/00105-t__e__!_z'.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/002 Otonqilotontli/00105-t__e__!_z'.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 621240,
                    "size": 435,
                    "blocks": 1,
                    "atimeMs": 1582149188711.5847,
                    "mtimeMs": 1582149188712.1482,
                    "ctimeMs": 1582149188712.1482,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.712Z",
                    "mtime": "2020-02-19T21:53:08.712Z",
                    "ctime": "2020-02-19T21:53:08.712Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Zawilqiloli_itik\" data-name=\"Zawilqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 76.99609 66.84766\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "002 Otonqilotontli/00106-t__e__v.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/002 Otonqilotontli/00106-t__e__v.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 621243,
                    "size": 435,
                    "blocks": 1,
                    "atimeMs": 1582149188712.9077,
                    "mtimeMs": 1582149188713.6194,
                    "ctimeMs": 1582149188713.6194,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.713Z",
                    "mtime": "2020-02-19T21:53:08.714Z",
                    "ctime": "2020-02-19T21:53:08.714Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Verolqiloli_itik\" data-name=\"Verolqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 77.05322 69.88477\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "002 Otonqilotontli/00107-t__e__!_v.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/002 Otonqilotontli/00107-t__e__!_v.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 621815,
                    "size": 435,
                    "blocks": 1,
                    "atimeMs": 1582149188715.0388,
                    "mtimeMs": 1582149188715.6538,
                    "ctimeMs": 1582149188715.6538,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.715Z",
                    "mtime": "2020-02-19T21:53:08.716Z",
                    "ctime": "2020-02-19T21:53:08.716Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Verolqiloli_itik\" data-name=\"Verolqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 77.05322 69.88477\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "002 Otonqilotontli/00108-t__e__ks.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/002 Otonqilotontli/00108-t__e__ks.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 621816,
                    "size": 437,
                    "blocks": 1,
                    "atimeMs": 1582149188716.459,
                    "mtimeMs": 1582149188717.065,
                    "ctimeMs": 1582149188717.065,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.716Z",
                    "mtime": "2020-02-19T21:53:08.717Z",
                    "ctime": "2020-02-19T21:53:08.717Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Ksimalqiloli_itik\" data-name=\"Ksimalqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 76.99609 68.44629\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "002 Otonqilotontli/00109-t__e__!_ks.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/002 Otonqilotontli/00109-t__e__!_ks.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 621818,
                    "size": 437,
                    "blocks": 1,
                    "atimeMs": 1582149188718.776,
                    "mtimeMs": 1582149188719.453,
                    "ctimeMs": 1582149188719.453,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.719Z",
                    "mtime": "2020-02-19T21:53:08.719Z",
                    "ctime": "2020-02-19T21:53:08.719Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Ksimalqiloli_itik\" data-name=\"Ksimalqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 76.99609 68.44629\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "002 Otonqilotontli/00110-t__e__ny.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/002 Otonqilotontli/00110-t__e__ny.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 621846,
                    "size": 437,
                    "blocks": 1,
                    "atimeMs": 1582149188720.248,
                    "mtimeMs": 1582149188720.874,
                    "ctimeMs": 1582149188720.874,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.720Z",
                    "mtime": "2020-02-19T21:53:08.721Z",
                    "ctime": "2020-02-19T21:53:08.721Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Nyonilqiloli_itik\" data-name=\"Nyonilqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 75.40479 65.39844\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "002 Otonqilotontli/00111-t__e__!_ny.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/002 Otonqilotontli/00111-t__e__!_ny.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 621847,
                    "size": 437,
                    "blocks": 1,
                    "atimeMs": 1582149188722.13,
                    "mtimeMs": 1582149188722.5273,
                    "ctimeMs": 1582149188722.5273,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.722Z",
                    "mtime": "2020-02-19T21:53:08.723Z",
                    "ctime": "2020-02-19T21:53:08.723Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Nyonilqiloli_itik\" data-name=\"Nyonilqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 75.40479 65.39844\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "003 Imezqilotontli/00112-t_o.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/003 Imezqilotontli/00112-t_o.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 621874,
                    "size": 401,
                    "blocks": 1,
                    "atimeMs": 1582149112419.2722,
                    "mtimeMs": 1582149188723.052,
                    "ctimeMs": 1582149188723.052,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:51:52.419Z",
                    "mtime": "2020-02-19T21:53:08.723Z",
                    "ctime": "2020-02-19T21:53:08.723Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Tekolqiloli\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 90.71191 98.22174\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "003 Imezqilotontli/00113-t_o'.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/003 Imezqilotontli/00113-t_o'.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 621875,
                    "size": 425,
                    "blocks": 1,
                    "atimeMs": 1582149188723.5803,
                    "mtimeMs": 1582149188723.934,
                    "ctimeMs": 1582149188723.934,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.724Z",
                    "mtime": "2020-02-19T21:53:08.724Z",
                    "ctime": "2020-02-19T21:53:08.724Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Hekalqilotontli_1\" data-name=\"Hekalqilotontli 1\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 33 33\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "003 Imezqilotontli/00114-t_o!.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/003 Imezqilotontli/00114-t_o!.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 621876,
                    "size": 416,
                    "blocks": 1,
                    "atimeMs": 1582149188724.4487,
                    "mtimeMs": 1582149188724.7983,
                    "ctimeMs": 1582149188724.7983,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.724Z",
                    "mtime": "2020-02-19T21:53:08.725Z",
                    "ctime": "2020-02-19T21:53:08.725Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Weyakqiloli_1\" data-name=\"Weyakqiloli 1\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 80 6\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "003 Imezqilotontli/00115-t_o!_'.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/003 Imezqilotontli/00115-t_o!_'.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 621879,
                    "size": 425,
                    "blocks": 1,
                    "atimeMs": 1582149188725.8176,
                    "mtimeMs": 1582149188726.1707,
                    "ctimeMs": 1582149188726.1707,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.726Z",
                    "mtime": "2020-02-19T21:53:08.726Z",
                    "ctime": "2020-02-19T21:53:08.726Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Hekalqilotontli_1\" data-name=\"Hekalqilotontli 1\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 33 33\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "003 Imezqilotontli/00116-t__o__m.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/003 Imezqilotontli/00116-t__o__m.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 621880,
                    "size": 433,
                    "blocks": 1,
                    "atimeMs": 1582149188726.684,
                    "mtimeMs": 1582149188727.107,
                    "ctimeMs": 1582149188727.107,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.727Z",
                    "mtime": "2020-02-19T21:53:08.727Z",
                    "ctime": "2020-02-19T21:53:08.727Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Mailqiloli_itik\" data-name=\"Mailqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 75.41407 65.86328\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "003 Imezqilotontli/00117-t__o__!_m.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/003 Imezqilotontli/00117-t__o__!_m.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 621881,
                    "size": 433,
                    "blocks": 1,
                    "atimeMs": 1582149188728.0298,
                    "mtimeMs": 1582149188728.4475,
                    "ctimeMs": 1582149188728.4475,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.728Z",
                    "mtime": "2020-02-19T21:53:08.728Z",
                    "ctime": "2020-02-19T21:53:08.728Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Mailqiloli_itik\" data-name=\"Mailqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 75.41407 65.86328\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "003 Imezqilotontli/00118-t__o__n.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/003 Imezqilotontli/00118-t__o__n.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 621882,
                    "size": 434,
                    "blocks": 1,
                    "atimeMs": 1582149188728.9539,
                    "mtimeMs": 1582149188729.3472,
                    "ctimeMs": 1582149188729.3472,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.729Z",
                    "mtime": "2020-02-19T21:53:08.729Z",
                    "ctime": "2020-02-19T21:53:08.729Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Neliwqiloli_itik\" data-name=\"Neliwqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 65.4248 65.38281\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "003 Imezqilotontli/00119-t__o__!_n.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/003 Imezqilotontli/00119-t__o__!_n.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 621883,
                    "size": 434,
                    "blocks": 1,
                    "atimeMs": 1582149188730.2727,
                    "mtimeMs": 1582149188730.643,
                    "ctimeMs": 1582149188730.643,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.730Z",
                    "mtime": "2020-02-19T21:53:08.731Z",
                    "ctime": "2020-02-19T21:53:08.731Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Neliwqiloli_itik\" data-name=\"Neliwqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 65.4248 65.38281\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "003 Imezqilotontli/00120-t__o__p.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/003 Imezqilotontli/00120-t__o__p.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 621884,
                    "size": 435,
                    "blocks": 1,
                    "atimeMs": 1582149188731.1548,
                    "mtimeMs": 1582149188731.5547,
                    "ctimeMs": 1582149188731.5547,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.731Z",
                    "mtime": "2020-02-19T21:53:08.732Z",
                    "ctime": "2020-02-19T21:53:08.732Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Pokalqiloli_itik\" data-name=\"Pokalqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 55.57676 69.98151\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "003 Imezqilotontli/00121-t__o__!_p.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/003 Imezqilotontli/00121-t__o__!_p.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 621885,
                    "size": 435,
                    "blocks": 1,
                    "atimeMs": 1582149188732.4695,
                    "mtimeMs": 1582149188732.8638,
                    "ctimeMs": 1582149188732.8638,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.732Z",
                    "mtime": "2020-02-19T21:53:08.733Z",
                    "ctime": "2020-02-19T21:53:08.733Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Pokalqiloli_itik\" data-name=\"Pokalqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 55.57676 69.98151\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "003 Imezqilotontli/00122-t__o__t.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/003 Imezqilotontli/00122-t__o__t.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 621886,
                    "size": 435,
                    "blocks": 1,
                    "atimeMs": 1582149188733.3865,
                    "mtimeMs": 1582149188733.7603,
                    "ctimeMs": 1582149188733.7603,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.733Z",
                    "mtime": "2020-02-19T21:53:08.734Z",
                    "ctime": "2020-02-19T21:53:08.734Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Tekolqiloli_itik\" data-name=\"Tekolqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 70.61133 68.50488\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "003 Imezqilotontli/00123-t__o__!_t.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/003 Imezqilotontli/00123-t__o__!_t.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 621887,
                    "size": 435,
                    "blocks": 1,
                    "atimeMs": 1582149188734.677,
                    "mtimeMs": 1582149188735.0486,
                    "ctimeMs": 1582149188735.0486,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.735Z",
                    "mtime": "2020-02-19T21:53:08.735Z",
                    "ctime": "2020-02-19T21:53:08.735Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Tekolqiloli_itik\" data-name=\"Tekolqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 70.61133 68.50488\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "003 Imezqilotontli/00124-t__o__tl.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/003 Imezqilotontli/00124-t__o__tl.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 621888,
                    "size": 439,
                    "blocks": 1,
                    "atimeMs": 1582149188735.5547,
                    "mtimeMs": 1582149188735.9585,
                    "ctimeMs": 1582149188735.9585,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.736Z",
                    "mtime": "2020-02-19T21:53:08.736Z",
                    "ctime": "2020-02-19T21:53:08.736Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Tlachalqiloli_itik\" data-name=\"Tlachalqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 73.98493 69.98828\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "003 Imezqilotontli/00125-t__o__!_tl.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/003 Imezqilotontli/00125-t__o__!_tl.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 621889,
                    "size": 439,
                    "blocks": 1,
                    "atimeMs": 1582149188736.8823,
                    "mtimeMs": 1582149188737.2854,
                    "ctimeMs": 1582149188737.2854,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.737Z",
                    "mtime": "2020-02-19T21:53:08.737Z",
                    "ctime": "2020-02-19T21:53:08.737Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Tlachalqiloli_itik\" data-name=\"Tlachalqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 73.98493 69.98828\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "003 Imezqilotontli/00126-t__o__x.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/003 Imezqilotontli/00126-t__o__x.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 621890,
                    "size": 435,
                    "blocks": 1,
                    "atimeMs": 1582149188737.7966,
                    "mtimeMs": 1582149188738.207,
                    "ctimeMs": 1582149188738.207,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.738Z",
                    "mtime": "2020-02-19T21:53:08.738Z",
                    "ctime": "2020-02-19T21:53:08.738Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Xocilqiloli_itik\" data-name=\"Xocilqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 58.64222 66.89453\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "003 Imezqilotontli/00127-t__o__!_x.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/003 Imezqilotontli/00127-t__o__!_x.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 621891,
                    "size": 435,
                    "blocks": 1,
                    "atimeMs": 1582149188739.1294,
                    "mtimeMs": 1582149188739.5293,
                    "ctimeMs": 1582149188739.5293,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.739Z",
                    "mtime": "2020-02-19T21:53:08.740Z",
                    "ctime": "2020-02-19T21:53:08.740Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Xocilqiloli_itik\" data-name=\"Xocilqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 58.64222 66.89453\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "003 Imezqilotontli/00128-t__o__c.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/003 Imezqilotontli/00128-t__o__c.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 621892,
                    "size": 437,
                    "blocks": 1,
                    "atimeMs": 1582149188740.0442,
                    "mtimeMs": 1582149188740.4531,
                    "ctimeMs": 1582149188740.4531,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.740Z",
                    "mtime": "2020-02-19T21:53:08.740Z",
                    "ctime": "2020-02-19T21:53:08.740Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Chakalqiloli_itik\" data-name=\"Chakalqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 58.00381 69.90327\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "003 Imezqilotontli/00129-t__o__!_c.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/003 Imezqilotontli/00129-t__o__!_c.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 621893,
                    "size": 437,
                    "blocks": 1,
                    "atimeMs": 1582149188741.379,
                    "mtimeMs": 1582149188741.778,
                    "ctimeMs": 1582149188741.778,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.741Z",
                    "mtime": "2020-02-19T21:53:08.742Z",
                    "ctime": "2020-02-19T21:53:08.742Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Chakalqiloli_itik\" data-name=\"Chakalqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 58.00381 69.90327\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "003 Imezqilotontli/00130-t__o__s.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/003 Imezqilotontli/00130-t__o__s.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 621894,
                    "size": 437,
                    "blocks": 1,
                    "atimeMs": 1582149188742.292,
                    "mtimeMs": 1582149188742.6724,
                    "ctimeMs": 1582149188742.6724,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.742Z",
                    "mtime": "2020-02-19T21:53:08.743Z",
                    "ctime": "2020-02-19T21:53:08.743Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Senzonqiloli_itik\" data-name=\"Senzonqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 77.00781 67.02148\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "003 Imezqilotontli/00131-t__o__!_s.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/003 Imezqilotontli/00131-t__o__!_s.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 621895,
                    "size": 437,
                    "blocks": 1,
                    "atimeMs": 1582149188744.1335,
                    "mtimeMs": 1582149188744.5623,
                    "ctimeMs": 1582149188744.5623,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.744Z",
                    "mtime": "2020-02-19T21:53:08.745Z",
                    "ctime": "2020-02-19T21:53:08.745Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Senzonqiloli_itik\" data-name=\"Senzonqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 77.00781 67.02148\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "003 Imezqilotontli/00132-t__o__z.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/003 Imezqilotontli/00132-t__o__z.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 621896,
                    "size": 433,
                    "blocks": 1,
                    "atimeMs": 1582149188745.1274,
                    "mtimeMs": 1582149188745.583,
                    "ctimeMs": 1582149188745.583,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.745Z",
                    "mtime": "2020-02-19T21:53:08.746Z",
                    "ctime": "2020-02-19T21:53:08.746Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Zopilqiloli_itik\" data-name=\"Zopilqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 77.00781 68.625\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "003 Imezqilotontli/00133-t__o__!_z.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/003 Imezqilotontli/00133-t__o__!_z.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 621897,
                    "size": 433,
                    "blocks": 1,
                    "atimeMs": 1582149188746.8645,
                    "mtimeMs": 1582149188747.2336,
                    "ctimeMs": 1582149188747.2336,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.747Z",
                    "mtime": "2020-02-19T21:53:08.747Z",
                    "ctime": "2020-02-19T21:53:08.747Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Zopilqiloli_itik\" data-name=\"Zopilqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 77.00781 68.625\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "003 Imezqilotontli/00134-t__o__k.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/003 Imezqilotontli/00134-t__o__k.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 621898,
                    "size": 433,
                    "blocks": 1,
                    "atimeMs": 1582149188747.744,
                    "mtimeMs": 1582149188748.1294,
                    "ctimeMs": 1582149188748.1294,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.748Z",
                    "mtime": "2020-02-19T21:53:08.748Z",
                    "ctime": "2020-02-19T21:53:08.748Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Coalqiloli_itik\" data-name=\"Coalqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 68.84783 69.99839\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "003 Imezqilotontli/00135-t__o__!_k.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/003 Imezqilotontli/00135-t__o__!_k.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 621899,
                    "size": 433,
                    "blocks": 1,
                    "atimeMs": 1582149188749.0437,
                    "mtimeMs": 1582149188749.4597,
                    "ctimeMs": 1582149188749.4597,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.749Z",
                    "mtime": "2020-02-19T21:53:08.749Z",
                    "ctime": "2020-02-19T21:53:08.749Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Coalqiloli_itik\" data-name=\"Coalqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 68.84783 69.99839\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "003 Imezqilotontli/00136-t__o__q.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/003 Imezqilotontli/00136-t__o__q.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 621900,
                    "size": 435,
                    "blocks": 1,
                    "atimeMs": 1582149188749.9663,
                    "mtimeMs": 1582149188750.344,
                    "ctimeMs": 1582149188750.344,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.750Z",
                    "mtime": "2020-02-19T21:53:08.750Z",
                    "ctime": "2020-02-19T21:53:08.750Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Qaqawqiloli_itik\" data-name=\"Qaqawqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 67.13916 68.67383\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "003 Imezqilotontli/00137-t__o__!_q.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/003 Imezqilotontli/00137-t__o__!_q.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 621901,
                    "size": 435,
                    "blocks": 1,
                    "atimeMs": 1582149188751.2917,
                    "mtimeMs": 1582149188751.6626,
                    "ctimeMs": 1582149188751.6626,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.751Z",
                    "mtime": "2020-02-19T21:53:08.752Z",
                    "ctime": "2020-02-19T21:53:08.752Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Qaqawqiloli_itik\" data-name=\"Qaqawqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 67.13916 68.67383\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "003 Imezqilotontli/00138-t__o__h.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/003 Imezqilotontli/00138-t__o__h.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 621902,
                    "size": 433,
                    "blocks": 1,
                    "atimeMs": 1582149188752.8965,
                    "mtimeMs": 1582149188753.2856,
                    "ctimeMs": 1582149188753.2856,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.753Z",
                    "mtime": "2020-02-19T21:53:08.753Z",
                    "ctime": "2020-02-19T21:53:08.753Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Hekalqiloli_itik\" data-name=\"Hekalqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 60.0387 63.1527\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "003 Imezqilotontli/00139-t__o__!_h.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/003 Imezqilotontli/00139-t__o__!_h.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 621903,
                    "size": 433,
                    "blocks": 1,
                    "atimeMs": 1582149188754.2058,
                    "mtimeMs": 1582149188754.5803,
                    "ctimeMs": 1582149188754.5803,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.754Z",
                    "mtime": "2020-02-19T21:53:08.755Z",
                    "ctime": "2020-02-19T21:53:08.755Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Hekalqiloli_itik\" data-name=\"Hekalqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 60.0387 63.1527\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "003 Imezqilotontli/00140-t__o__l.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/003 Imezqilotontli/00140-t__o__l.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 621904,
                    "size": 435,
                    "blocks": 1,
                    "atimeMs": 1582149188755.0847,
                    "mtimeMs": 1582149188755.4712,
                    "ctimeMs": 1582149188755.4712,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.755Z",
                    "mtime": "2020-02-19T21:53:08.755Z",
                    "ctime": "2020-02-19T21:53:08.755Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Limonqiloli_itik\" data-name=\"Limonqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 56.86193 67.47461\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "003 Imezqilotontli/00141-t__o__!_l.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/003 Imezqilotontli/00141-t__o__!_l.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 621905,
                    "size": 435,
                    "blocks": 1,
                    "atimeMs": 1582149188756.414,
                    "mtimeMs": 1582149188756.792,
                    "ctimeMs": 1582149188756.792,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.756Z",
                    "mtime": "2020-02-19T21:53:08.757Z",
                    "ctime": "2020-02-19T21:53:08.757Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Limonqiloli_itik\" data-name=\"Limonqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 56.86193 67.47461\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "003 Imezqilotontli/00142-t__o__w.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/003 Imezqilotontli/00142-t__o__w.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 621906,
                    "size": 435,
                    "blocks": 1,
                    "atimeMs": 1582149188757.3306,
                    "mtimeMs": 1582149188757.7068,
                    "ctimeMs": 1582149188757.7068,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.757Z",
                    "mtime": "2020-02-19T21:53:08.758Z",
                    "ctime": "2020-02-19T21:53:08.758Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Weyilqiloli_itik\" data-name=\"Weyilqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 77.05322 69.88477\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "003 Imezqilotontli/00143-t__o__!_w.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/003 Imezqilotontli/00143-t__o__!_w.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 621908,
                    "size": 435,
                    "blocks": 1,
                    "atimeMs": 1582149188758.951,
                    "mtimeMs": 1582149188759.302,
                    "ctimeMs": 1582149188759.302,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.759Z",
                    "mtime": "2020-02-19T21:53:08.759Z",
                    "ctime": "2020-02-19T21:53:08.759Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Weyilqiloli_itik\" data-name=\"Weyilqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 77.05322 69.88477\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "003 Imezqilotontli/00144-t__o__y.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/003 Imezqilotontli/00144-t__o__y.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 621909,
                    "size": 435,
                    "blocks": 1,
                    "atimeMs": 1582149188759.8235,
                    "mtimeMs": 1582149188760.2085,
                    "ctimeMs": 1582149188760.2085,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.760Z",
                    "mtime": "2020-02-19T21:53:08.760Z",
                    "ctime": "2020-02-19T21:53:08.760Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Yololqiloli_itik\" data-name=\"Yololqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 59.96725 67.44629\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "003 Imezqilotontli/00145-t__o__!_y.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/003 Imezqilotontli/00145-t__o__!_y.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 621910,
                    "size": 435,
                    "blocks": 1,
                    "atimeMs": 1582149188761.0764,
                    "mtimeMs": 1582149188761.4783,
                    "ctimeMs": 1582149188761.4783,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.761Z",
                    "mtime": "2020-02-19T21:53:08.761Z",
                    "ctime": "2020-02-19T21:53:08.761Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Yololqiloli_itik\" data-name=\"Yololqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 59.96725 67.44629\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "003 Imezqilotontli/00146-t__o__b.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/003 Imezqilotontli/00146-t__o__b.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 621911,
                    "size": 435,
                    "blocks": 1,
                    "atimeMs": 1582149188762.0056,
                    "mtimeMs": 1582149188762.4375,
                    "ctimeMs": 1582149188762.4375,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.762Z",
                    "mtime": "2020-02-19T21:53:08.762Z",
                    "ctime": "2020-02-19T21:53:08.762Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Balamqiloli_itik\" data-name=\"Balamqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 75.41407 65.86328\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "003 Imezqilotontli/00147-t__o__!_b.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/003 Imezqilotontli/00147-t__o__!_b.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 621913,
                    "size": 435,
                    "blocks": 1,
                    "atimeMs": 1582149188763.3232,
                    "mtimeMs": 1582149188763.7517,
                    "ctimeMs": 1582149188763.7517,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.763Z",
                    "mtime": "2020-02-19T21:53:08.764Z",
                    "ctime": "2020-02-19T21:53:08.764Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Balamqiloli_itik\" data-name=\"Balamqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 75.41407 65.86328\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "003 Imezqilotontli/00148-t__o__d.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/003 Imezqilotontli/00148-t__o__d.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 621914,
                    "size": 435,
                    "blocks": 1,
                    "atimeMs": 1582149188764.2407,
                    "mtimeMs": 1582149188764.6238,
                    "ctimeMs": 1582149188764.6238,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.764Z",
                    "mtime": "2020-02-19T21:53:08.765Z",
                    "ctime": "2020-02-19T21:53:08.765Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Dunilqiloli_itik\" data-name=\"Dunilqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 70.61133 68.50488\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "003 Imezqilotontli/00149-t__o__!_d.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/003 Imezqilotontli/00149-t__o__!_d.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 621915,
                    "size": 435,
                    "blocks": 1,
                    "atimeMs": 1582149188765.602,
                    "mtimeMs": 1582149188765.9797,
                    "ctimeMs": 1582149188765.9797,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.766Z",
                    "mtime": "2020-02-19T21:53:08.766Z",
                    "ctime": "2020-02-19T21:53:08.766Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Dunilqiloli_itik\" data-name=\"Dunilqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 70.61133 68.50488\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "003 Imezqilotontli/00150-t__o__f.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/003 Imezqilotontli/00150-t__o__f.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 621916,
                    "size": 437,
                    "blocks": 1,
                    "atimeMs": 1582149188766.4683,
                    "mtimeMs": 1582149188766.932,
                    "ctimeMs": 1582149188766.932,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.766Z",
                    "mtime": "2020-02-19T21:53:08.767Z",
                    "ctime": "2020-02-19T21:53:08.767Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Thamilqiloli_itik\" data-name=\"Thamilqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 55.57676 69.98151\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "003 Imezqilotontli/00151-t__o__!_f.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/003 Imezqilotontli/00151-t__o__!_f.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 621917,
                    "size": 437,
                    "blocks": 1,
                    "atimeMs": 1582149188767.8262,
                    "mtimeMs": 1582149188768.2231,
                    "ctimeMs": 1582149188768.2231,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.768Z",
                    "mtime": "2020-02-19T21:53:08.768Z",
                    "ctime": "2020-02-19T21:53:08.768Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Thamilqiloli_itik\" data-name=\"Thamilqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 55.57676 69.98151\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "003 Imezqilotontli/00152-t__o__g.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/003 Imezqilotontli/00152-t__o__g.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 621918,
                    "size": 435,
                    "blocks": 1,
                    "atimeMs": 1582149188768.715,
                    "mtimeMs": 1582149188769.1086,
                    "ctimeMs": 1582149188769.1086,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.769Z",
                    "mtime": "2020-02-19T21:53:08.769Z",
                    "ctime": "2020-02-19T21:53:08.769Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Galilqiloli_itik\" data-name=\"Galilqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 68.84685 69.99854\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "003 Imezqilotontli/00153-t__o__!_g.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/003 Imezqilotontli/00153-t__o__!_g.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 621919,
                    "size": 435,
                    "blocks": 1,
                    "atimeMs": 1582149188770.0505,
                    "mtimeMs": 1582149188770.4326,
                    "ctimeMs": 1582149188770.4326,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.770Z",
                    "mtime": "2020-02-19T21:53:08.770Z",
                    "ctime": "2020-02-19T21:53:08.770Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Galilqiloli_itik\" data-name=\"Galilqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 68.84685 69.99854\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "003 Imezqilotontli/00154-t__o__kh.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/003 Imezqilotontli/00154-t__o__kh.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 621920,
                    "size": 437,
                    "blocks": 1,
                    "atimeMs": 1582149188770.9312,
                    "mtimeMs": 1582149188771.3564,
                    "ctimeMs": 1582149188771.3564,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.771Z",
                    "mtime": "2020-02-19T21:53:08.771Z",
                    "ctime": "2020-02-19T21:53:08.771Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Khamalqiloli_itik\" data-name=\"Khamalqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 65.60791 63.15566\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "003 Imezqilotontli/00155-t__o__!_kh.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/003 Imezqilotontli/00155-t__o__!_kh.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 621921,
                    "size": 437,
                    "blocks": 1,
                    "atimeMs": 1582149188772.2415,
                    "mtimeMs": 1582149188772.6316,
                    "ctimeMs": 1582149188772.6316,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.772Z",
                    "mtime": "2020-02-19T21:53:08.773Z",
                    "ctime": "2020-02-19T21:53:08.773Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Khamalqiloli_itik\" data-name=\"Khamalqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 65.60791 63.15566\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "003 Imezqilotontli/00156-t__o__j.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/003 Imezqilotontli/00156-t__o__j.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 621922,
                    "size": 435,
                    "blocks": 1,
                    "atimeMs": 1582149188773.1233,
                    "mtimeMs": 1582149188773.5598,
                    "ctimeMs": 1582149188773.5598,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.773Z",
                    "mtime": "2020-02-19T21:53:08.774Z",
                    "ctime": "2020-02-19T21:53:08.774Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Jokilqiloli_itik\" data-name=\"Jokilqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 66.64662 67.44434\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "003 Imezqilotontli/00157-t__o__!_j.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/003 Imezqilotontli/00157-t__o__!_j.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 621923,
                    "size": 435,
                    "blocks": 1,
                    "atimeMs": 1582149188774.444,
                    "mtimeMs": 1582149188774.8723,
                    "ctimeMs": 1582149188774.8723,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.774Z",
                    "mtime": "2020-02-19T21:53:08.775Z",
                    "ctime": "2020-02-19T21:53:08.775Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Jokilqiloli_itik\" data-name=\"Jokilqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 66.64662 67.44434\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "003 Imezqilotontli/00158-t__o__r.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/003 Imezqilotontli/00158-t__o__r.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 621924,
                    "size": 435,
                    "blocks": 1,
                    "atimeMs": 1582149188775.3672,
                    "mtimeMs": 1582149188775.7966,
                    "ctimeMs": 1582149188775.7966,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.775Z",
                    "mtime": "2020-02-19T21:53:08.776Z",
                    "ctime": "2020-02-19T21:53:08.776Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Runalqiloli_itik\" data-name=\"Runalqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 69.46289 67.47461\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "003 Imezqilotontli/00159-t__o__!_r.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/003 Imezqilotontli/00159-t__o__!_r.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 621925,
                    "size": 435,
                    "blocks": 1,
                    "atimeMs": 1582149188776.6987,
                    "mtimeMs": 1582149188777.0908,
                    "ctimeMs": 1582149188777.0908,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.777Z",
                    "mtime": "2020-02-19T21:53:08.777Z",
                    "ctime": "2020-02-19T21:53:08.777Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Runalqiloli_itik\" data-name=\"Runalqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 69.46289 67.47461\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "003 Imezqilotontli/00160-t__o__z'.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/003 Imezqilotontli/00160-t__o__z'.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 621926,
                    "size": 435,
                    "blocks": 1,
                    "atimeMs": 1582149188777.606,
                    "mtimeMs": 1582149188778.0237,
                    "ctimeMs": 1582149188778.0237,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.778Z",
                    "mtime": "2020-02-19T21:53:08.778Z",
                    "ctime": "2020-02-19T21:53:08.778Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Zawilqiloli_itik\" data-name=\"Zawilqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 76.99609 66.84766\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "003 Imezqilotontli/00161-t__o__!_z'.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/003 Imezqilotontli/00161-t__o__!_z'.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 621927,
                    "size": 435,
                    "blocks": 1,
                    "atimeMs": 1582149188778.9067,
                    "mtimeMs": 1582149188779.28,
                    "ctimeMs": 1582149188779.28,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.779Z",
                    "mtime": "2020-02-19T21:53:08.779Z",
                    "ctime": "2020-02-19T21:53:08.779Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Zawilqiloli_itik\" data-name=\"Zawilqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 76.99609 66.84766\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "003 Imezqilotontli/00162-t__o__v.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/003 Imezqilotontli/00162-t__o__v.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 621928,
                    "size": 435,
                    "blocks": 1,
                    "atimeMs": 1582149188779.7737,
                    "mtimeMs": 1582149188780.1533,
                    "ctimeMs": 1582149188780.1533,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.780Z",
                    "mtime": "2020-02-19T21:53:08.780Z",
                    "ctime": "2020-02-19T21:53:08.780Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Verolqiloli_itik\" data-name=\"Verolqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 77.05322 69.88477\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "003 Imezqilotontli/00163-t__o__!_v.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/003 Imezqilotontli/00163-t__o__!_v.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 621929,
                    "size": 435,
                    "blocks": 1,
                    "atimeMs": 1582149188781.0413,
                    "mtimeMs": 1582149188782.4084,
                    "ctimeMs": 1582149188782.4084,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.781Z",
                    "mtime": "2020-02-19T21:53:08.782Z",
                    "ctime": "2020-02-19T21:53:08.782Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Verolqiloli_itik\" data-name=\"Verolqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 77.05322 69.88477\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "003 Imezqilotontli/00164-t__o__ks.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/003 Imezqilotontli/00164-t__o__ks.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 621930,
                    "size": 437,
                    "blocks": 1,
                    "atimeMs": 1582149188783.2236,
                    "mtimeMs": 1582149188783.6277,
                    "ctimeMs": 1582149188783.6277,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.783Z",
                    "mtime": "2020-02-19T21:53:08.784Z",
                    "ctime": "2020-02-19T21:53:08.784Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Ksimalqiloli_itik\" data-name=\"Ksimalqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 76.99609 68.44629\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "003 Imezqilotontli/00165-t__o__!_ks.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/003 Imezqilotontli/00165-t__o__!_ks.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 621931,
                    "size": 437,
                    "blocks": 1,
                    "atimeMs": 1582149188784.5566,
                    "mtimeMs": 1582149188784.952,
                    "ctimeMs": 1582149188784.952,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.785Z",
                    "mtime": "2020-02-19T21:53:08.785Z",
                    "ctime": "2020-02-19T21:53:08.785Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Ksimalqiloli_itik\" data-name=\"Ksimalqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 76.99609 68.44629\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "003 Imezqilotontli/00166-t__o__ny.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/003 Imezqilotontli/00166-t__o__ny.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 621932,
                    "size": 437,
                    "blocks": 1,
                    "atimeMs": 1582149188785.4822,
                    "mtimeMs": 1582149188785.8806,
                    "ctimeMs": 1582149188785.8806,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.785Z",
                    "mtime": "2020-02-19T21:53:08.786Z",
                    "ctime": "2020-02-19T21:53:08.786Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Nyonilqiloli_itik\" data-name=\"Nyonilqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 75.40479 65.39844\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "003 Imezqilotontli/00167-t__o__!_ny.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/003 Imezqilotontli/00167-t__o__!_ny.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 621933,
                    "size": 437,
                    "blocks": 1,
                    "atimeMs": 1582149188792.6753,
                    "mtimeMs": 1582149188793.1804,
                    "ctimeMs": 1582149188793.1804,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.793Z",
                    "mtime": "2020-02-19T21:53:08.793Z",
                    "ctime": "2020-02-19T21:53:08.793Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Nyonilqiloli_itik\" data-name=\"Nyonilqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 75.40479 65.39844\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "004 Elaqilotontli/00168-t_i.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/004 Elaqilotontli/00168-t_i.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 621935,
                    "size": 401,
                    "blocks": 1,
                    "atimeMs": 1582149112492.6338,
                    "mtimeMs": 1582149188793.867,
                    "ctimeMs": 1582149188793.867,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:51:52.493Z",
                    "mtime": "2020-02-19T21:53:08.794Z",
                    "ctime": "2020-02-19T21:53:08.794Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Tekolqiloli\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 90.71191 98.22174\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "004 Elaqilotontli/00169-t_i'.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/004 Elaqilotontli/00169-t_i'.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 621936,
                    "size": 425,
                    "blocks": 1,
                    "atimeMs": 1582149188794.416,
                    "mtimeMs": 1582149188794.778,
                    "ctimeMs": 1582149188794.778,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.794Z",
                    "mtime": "2020-02-19T21:53:08.795Z",
                    "ctime": "2020-02-19T21:53:08.795Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Hekalqilotontli_1\" data-name=\"Hekalqilotontli 1\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 33 33\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "004 Elaqilotontli/00170-t_i!.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/004 Elaqilotontli/00170-t_i!.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 621937,
                    "size": 416,
                    "blocks": 1,
                    "atimeMs": 1582149188795.3135,
                    "mtimeMs": 1582149188795.6643,
                    "ctimeMs": 1582149188795.6643,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.795Z",
                    "mtime": "2020-02-19T21:53:08.796Z",
                    "ctime": "2020-02-19T21:53:08.796Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Weyakqiloli_1\" data-name=\"Weyakqiloli 1\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 80 6\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "004 Elaqilotontli/00171-t_i!_'.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/004 Elaqilotontli/00171-t_i!_'.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 621938,
                    "size": 425,
                    "blocks": 1,
                    "atimeMs": 1582149188796.6963,
                    "mtimeMs": 1582149188797.041,
                    "ctimeMs": 1582149188797.041,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.797Z",
                    "mtime": "2020-02-19T21:53:08.797Z",
                    "ctime": "2020-02-19T21:53:08.797Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Hekalqilotontli_1\" data-name=\"Hekalqilotontli 1\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 33 33\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "004 Elaqilotontli/00172-t__i__m.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/004 Elaqilotontli/00172-t__i__m.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 621939,
                    "size": 433,
                    "blocks": 1,
                    "atimeMs": 1582149188797.6199,
                    "mtimeMs": 1582149188798.0364,
                    "ctimeMs": 1582149188798.0364,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.798Z",
                    "mtime": "2020-02-19T21:53:08.798Z",
                    "ctime": "2020-02-19T21:53:08.798Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Mailqiloli_itik\" data-name=\"Mailqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 75.41407 65.86328\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "004 Elaqilotontli/00173-t__i__!_m.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/004 Elaqilotontli/00173-t__i__!_m.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 621940,
                    "size": 433,
                    "blocks": 1,
                    "atimeMs": 1582149188798.9353,
                    "mtimeMs": 1582149188799.3467,
                    "ctimeMs": 1582149188799.3467,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.799Z",
                    "mtime": "2020-02-19T21:53:08.799Z",
                    "ctime": "2020-02-19T21:53:08.799Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Mailqiloli_itik\" data-name=\"Mailqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 75.41407 65.86328\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "004 Elaqilotontli/00174-t__i__n.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/004 Elaqilotontli/00174-t__i__n.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 621941,
                    "size": 434,
                    "blocks": 1,
                    "atimeMs": 1582149188799.8464,
                    "mtimeMs": 1582149188800.2148,
                    "ctimeMs": 1582149188800.2148,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.800Z",
                    "mtime": "2020-02-19T21:53:08.800Z",
                    "ctime": "2020-02-19T21:53:08.800Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Neliwqiloli_itik\" data-name=\"Neliwqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 65.4248 65.38281\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "004 Elaqilotontli/00175-t__i__!_n.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/004 Elaqilotontli/00175-t__i__!_n.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 621942,
                    "size": 434,
                    "blocks": 1,
                    "atimeMs": 1582149188801.1118,
                    "mtimeMs": 1582149188801.4797,
                    "ctimeMs": 1582149188801.4797,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.801Z",
                    "mtime": "2020-02-19T21:53:08.801Z",
                    "ctime": "2020-02-19T21:53:08.801Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Neliwqiloli_itik\" data-name=\"Neliwqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 65.4248 65.38281\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "004 Elaqilotontli/00176-t__i__p.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/004 Elaqilotontli/00176-t__i__p.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 621943,
                    "size": 435,
                    "blocks": 1,
                    "atimeMs": 1582149188801.979,
                    "mtimeMs": 1582149188802.3647,
                    "ctimeMs": 1582149188802.3647,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.802Z",
                    "mtime": "2020-02-19T21:53:08.802Z",
                    "ctime": "2020-02-19T21:53:08.802Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Pokalqiloli_itik\" data-name=\"Pokalqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 55.57676 69.98151\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "004 Elaqilotontli/00177-t__i__!_p.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/004 Elaqilotontli/00177-t__i__!_p.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 621944,
                    "size": 435,
                    "blocks": 1,
                    "atimeMs": 1582149188803.2634,
                    "mtimeMs": 1582149188803.6418,
                    "ctimeMs": 1582149188803.6418,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.803Z",
                    "mtime": "2020-02-19T21:53:08.804Z",
                    "ctime": "2020-02-19T21:53:08.804Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Pokalqiloli_itik\" data-name=\"Pokalqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 55.57676 69.98151\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "004 Elaqilotontli/00178-t__i__t.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/004 Elaqilotontli/00178-t__i__t.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 621945,
                    "size": 435,
                    "blocks": 1,
                    "atimeMs": 1582149188804.1575,
                    "mtimeMs": 1582149188805.4084,
                    "ctimeMs": 1582149188805.4084,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.804Z",
                    "mtime": "2020-02-19T21:53:08.805Z",
                    "ctime": "2020-02-19T21:53:08.805Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Tekolqiloli_itik\" data-name=\"Tekolqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 70.61133 68.50488\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "004 Elaqilotontli/00179-t__i__!_t.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/004 Elaqilotontli/00179-t__i__!_t.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 621946,
                    "size": 435,
                    "blocks": 1,
                    "atimeMs": 1582149188806.3926,
                    "mtimeMs": 1582149188806.7644,
                    "ctimeMs": 1582149188806.7644,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.806Z",
                    "mtime": "2020-02-19T21:53:08.807Z",
                    "ctime": "2020-02-19T21:53:08.807Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Tekolqiloli_itik\" data-name=\"Tekolqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 70.61133 68.50488\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "004 Elaqilotontli/00180-t__i__tl.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/004 Elaqilotontli/00180-t__i__tl.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 621947,
                    "size": 439,
                    "blocks": 1,
                    "atimeMs": 1582149188807.297,
                    "mtimeMs": 1582149188807.708,
                    "ctimeMs": 1582149188807.708,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.807Z",
                    "mtime": "2020-02-19T21:53:08.808Z",
                    "ctime": "2020-02-19T21:53:08.808Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Tlachalqiloli_itik\" data-name=\"Tlachalqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 73.98493 69.98828\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "004 Elaqilotontli/00181-t__i__!_tl.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/004 Elaqilotontli/00181-t__i__!_tl.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 621948,
                    "size": 439,
                    "blocks": 1,
                    "atimeMs": 1582149188808.646,
                    "mtimeMs": 1582149188809.0454,
                    "ctimeMs": 1582149188809.0454,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.809Z",
                    "mtime": "2020-02-19T21:53:08.809Z",
                    "ctime": "2020-02-19T21:53:08.809Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Tlachalqiloli_itik\" data-name=\"Tlachalqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 73.98493 69.98828\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "004 Elaqilotontli/00182-t__i__x.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/004 Elaqilotontli/00182-t__i__x.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 621949,
                    "size": 435,
                    "blocks": 1,
                    "atimeMs": 1582149188809.8403,
                    "mtimeMs": 1582149188810.304,
                    "ctimeMs": 1582149188810.304,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.810Z",
                    "mtime": "2020-02-19T21:53:08.810Z",
                    "ctime": "2020-02-19T21:53:08.810Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Xocilqiloli_itik\" data-name=\"Xocilqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 58.64222 66.89453\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "004 Elaqilotontli/00183-t__i__!_x.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/004 Elaqilotontli/00183-t__i__!_x.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 621950,
                    "size": 435,
                    "blocks": 1,
                    "atimeMs": 1582149188811.2424,
                    "mtimeMs": 1582149188811.6538,
                    "ctimeMs": 1582149188811.6538,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.811Z",
                    "mtime": "2020-02-19T21:53:08.812Z",
                    "ctime": "2020-02-19T21:53:08.812Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Xocilqiloli_itik\" data-name=\"Xocilqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 58.64222 66.89453\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "004 Elaqilotontli/00184-t__i__c.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/004 Elaqilotontli/00184-t__i__c.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 621951,
                    "size": 437,
                    "blocks": 1,
                    "atimeMs": 1582149188812.1929,
                    "mtimeMs": 1582149188812.6143,
                    "ctimeMs": 1582149188812.6143,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.812Z",
                    "mtime": "2020-02-19T21:53:08.813Z",
                    "ctime": "2020-02-19T21:53:08.813Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Chakalqiloli_itik\" data-name=\"Chakalqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 58.00381 69.90327\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "004 Elaqilotontli/00185-t__i__!_c.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/004 Elaqilotontli/00185-t__i__!_c.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 621952,
                    "size": 437,
                    "blocks": 1,
                    "atimeMs": 1582149188813.565,
                    "mtimeMs": 1582149188813.9807,
                    "ctimeMs": 1582149188813.9807,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.814Z",
                    "mtime": "2020-02-19T21:53:08.814Z",
                    "ctime": "2020-02-19T21:53:08.814Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Chakalqiloli_itik\" data-name=\"Chakalqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 58.00381 69.90327\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "004 Elaqilotontli/00186-t__i__s.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/004 Elaqilotontli/00186-t__i__s.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 621953,
                    "size": 437,
                    "blocks": 1,
                    "atimeMs": 1582149188814.4976,
                    "mtimeMs": 1582149188815.0234,
                    "ctimeMs": 1582149188815.0234,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.814Z",
                    "mtime": "2020-02-19T21:53:08.815Z",
                    "ctime": "2020-02-19T21:53:08.815Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Senzonqiloli_itik\" data-name=\"Senzonqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 77.00781 67.02148\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "004 Elaqilotontli/00187-t__i__!_s.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/004 Elaqilotontli/00187-t__i__!_s.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 621954,
                    "size": 437,
                    "blocks": 1,
                    "atimeMs": 1582149188816.186,
                    "mtimeMs": 1582149188816.5615,
                    "ctimeMs": 1582149188816.5615,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.816Z",
                    "mtime": "2020-02-19T21:53:08.817Z",
                    "ctime": "2020-02-19T21:53:08.817Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Senzonqiloli_itik\" data-name=\"Senzonqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 77.00781 67.02148\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "004 Elaqilotontli/00188-t__i__z.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/004 Elaqilotontli/00188-t__i__z.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 621955,
                    "size": 433,
                    "blocks": 1,
                    "atimeMs": 1582149188817.1023,
                    "mtimeMs": 1582149188817.5059,
                    "ctimeMs": 1582149188817.5059,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.817Z",
                    "mtime": "2020-02-19T21:53:08.818Z",
                    "ctime": "2020-02-19T21:53:08.818Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Zopilqiloli_itik\" data-name=\"Zopilqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 77.00781 68.625\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "004 Elaqilotontli/00189-t__i__!_z.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/004 Elaqilotontli/00189-t__i__!_z.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 621956,
                    "size": 433,
                    "blocks": 1,
                    "atimeMs": 1582149188818.4543,
                    "mtimeMs": 1582149188818.8384,
                    "ctimeMs": 1582149188818.8384,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.818Z",
                    "mtime": "2020-02-19T21:53:08.819Z",
                    "ctime": "2020-02-19T21:53:08.819Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Zopilqiloli_itik\" data-name=\"Zopilqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 77.00781 68.625\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "004 Elaqilotontli/00190-t__i__k.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/004 Elaqilotontli/00190-t__i__k.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 621957,
                    "size": 433,
                    "blocks": 1,
                    "atimeMs": 1582149188819.3667,
                    "mtimeMs": 1582149188819.7556,
                    "ctimeMs": 1582149188819.7556,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.819Z",
                    "mtime": "2020-02-19T21:53:08.820Z",
                    "ctime": "2020-02-19T21:53:08.820Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Coalqiloli_itik\" data-name=\"Coalqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 68.84783 69.99839\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "004 Elaqilotontli/00191-t__i__!_k.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/004 Elaqilotontli/00191-t__i__!_k.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 621958,
                    "size": 433,
                    "blocks": 1,
                    "atimeMs": 1582149188821.1807,
                    "mtimeMs": 1582149188821.5825,
                    "ctimeMs": 1582149188821.5825,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.821Z",
                    "mtime": "2020-02-19T21:53:08.822Z",
                    "ctime": "2020-02-19T21:53:08.822Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Coalqiloli_itik\" data-name=\"Coalqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 68.84783 69.99839\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "004 Elaqilotontli/00192-t__i__q.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/004 Elaqilotontli/00192-t__i__q.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 621959,
                    "size": 435,
                    "blocks": 1,
                    "atimeMs": 1582149188822.1233,
                    "mtimeMs": 1582149188822.5112,
                    "ctimeMs": 1582149188822.5112,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.822Z",
                    "mtime": "2020-02-19T21:53:08.823Z",
                    "ctime": "2020-02-19T21:53:08.823Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Qaqawqiloli_itik\" data-name=\"Qaqawqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 67.13916 68.67383\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "004 Elaqilotontli/00193-t__i__!_q.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/004 Elaqilotontli/00193-t__i__!_q.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 621960,
                    "size": 435,
                    "blocks": 1,
                    "atimeMs": 1582149188823.4524,
                    "mtimeMs": 1582149188823.835,
                    "ctimeMs": 1582149188823.835,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.823Z",
                    "mtime": "2020-02-19T21:53:08.824Z",
                    "ctime": "2020-02-19T21:53:08.824Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Qaqawqiloli_itik\" data-name=\"Qaqawqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 67.13916 68.67383\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "004 Elaqilotontli/00194-t__i__h.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/004 Elaqilotontli/00194-t__i__h.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 621961,
                    "size": 433,
                    "blocks": 1,
                    "atimeMs": 1582149188824.3562,
                    "mtimeMs": 1582149188825.0159,
                    "ctimeMs": 1582149188825.0159,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.824Z",
                    "mtime": "2020-02-19T21:53:08.825Z",
                    "ctime": "2020-02-19T21:53:08.825Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Hekalqiloli_itik\" data-name=\"Hekalqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 60.0387 63.1527\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "004 Elaqilotontli/00195-t__i__!_h.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/004 Elaqilotontli/00195-t__i__!_h.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 621962,
                    "size": 433,
                    "blocks": 1,
                    "atimeMs": 1582149188825.986,
                    "mtimeMs": 1582149188826.371,
                    "ctimeMs": 1582149188826.371,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.826Z",
                    "mtime": "2020-02-19T21:53:08.826Z",
                    "ctime": "2020-02-19T21:53:08.826Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Hekalqiloli_itik\" data-name=\"Hekalqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 60.0387 63.1527\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "004 Elaqilotontli/00196-t__i__l.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/004 Elaqilotontli/00196-t__i__l.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 621963,
                    "size": 435,
                    "blocks": 1,
                    "atimeMs": 1582149188826.8938,
                    "mtimeMs": 1582149188827.2883,
                    "ctimeMs": 1582149188827.2883,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.827Z",
                    "mtime": "2020-02-19T21:53:08.827Z",
                    "ctime": "2020-02-19T21:53:08.827Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Limonqiloli_itik\" data-name=\"Limonqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 56.86193 67.47461\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "004 Elaqilotontli/00197-t__i__!_l.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/004 Elaqilotontli/00197-t__i__!_l.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 621964,
                    "size": 435,
                    "blocks": 1,
                    "atimeMs": 1582149188828.235,
                    "mtimeMs": 1582149188828.6172,
                    "ctimeMs": 1582149188828.6172,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.828Z",
                    "mtime": "2020-02-19T21:53:08.829Z",
                    "ctime": "2020-02-19T21:53:08.829Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Limonqiloli_itik\" data-name=\"Limonqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 56.86193 67.47461\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "004 Elaqilotontli/00198-t__i__w.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/004 Elaqilotontli/00198-t__i__w.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 621977,
                    "size": 435,
                    "blocks": 1,
                    "atimeMs": 1582149188829.1404,
                    "mtimeMs": 1582149188829.6833,
                    "ctimeMs": 1582149188829.6833,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.829Z",
                    "mtime": "2020-02-19T21:53:08.830Z",
                    "ctime": "2020-02-19T21:53:08.830Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Weyilqiloli_itik\" data-name=\"Weyilqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 77.05322 69.88477\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "004 Elaqilotontli/00199-t__i__!_w.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/004 Elaqilotontli/00199-t__i__!_w.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 621978,
                    "size": 435,
                    "blocks": 1,
                    "atimeMs": 1582149188830.6887,
                    "mtimeMs": 1582149188831.053,
                    "ctimeMs": 1582149188831.053,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.831Z",
                    "mtime": "2020-02-19T21:53:08.831Z",
                    "ctime": "2020-02-19T21:53:08.831Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Weyilqiloli_itik\" data-name=\"Weyilqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 77.05322 69.88477\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "004 Elaqilotontli/00200-t__i__y.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/004 Elaqilotontli/00200-t__i__y.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 621979,
                    "size": 435,
                    "blocks": 1,
                    "atimeMs": 1582149188831.8164,
                    "mtimeMs": 1582149188832.2263,
                    "ctimeMs": 1582149188832.2263,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.832Z",
                    "mtime": "2020-02-19T21:53:08.832Z",
                    "ctime": "2020-02-19T21:53:08.832Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Yololqiloli_itik\" data-name=\"Yololqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 59.96725 67.44629\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "004 Elaqilotontli/00201-t__i__!_y.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/004 Elaqilotontli/00201-t__i__!_y.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 621980,
                    "size": 435,
                    "blocks": 1,
                    "atimeMs": 1582149188833.1946,
                    "mtimeMs": 1582149188833.5862,
                    "ctimeMs": 1582149188833.5862,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.833Z",
                    "mtime": "2020-02-19T21:53:08.834Z",
                    "ctime": "2020-02-19T21:53:08.834Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Yololqiloli_itik\" data-name=\"Yololqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 59.96725 67.44629\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "004 Elaqilotontli/00202-t__i__b.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/004 Elaqilotontli/00202-t__i__b.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 621981,
                    "size": 435,
                    "blocks": 1,
                    "atimeMs": 1582149188834.305,
                    "mtimeMs": 1582149188834.8167,
                    "ctimeMs": 1582149188834.8167,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.834Z",
                    "mtime": "2020-02-19T21:53:08.835Z",
                    "ctime": "2020-02-19T21:53:08.835Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Balamqiloli_itik\" data-name=\"Balamqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 75.41407 65.86328\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "004 Elaqilotontli/00203-t__i__!_b.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/004 Elaqilotontli/00203-t__i__!_b.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 621982,
                    "size": 435,
                    "blocks": 1,
                    "atimeMs": 1582149188835.751,
                    "mtimeMs": 1582149188836.1946,
                    "ctimeMs": 1582149188836.1946,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.836Z",
                    "mtime": "2020-02-19T21:53:08.836Z",
                    "ctime": "2020-02-19T21:53:08.836Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Balamqiloli_itik\" data-name=\"Balamqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 75.41407 65.86328\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "004 Elaqilotontli/00204-t__i__d.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/004 Elaqilotontli/00204-t__i__d.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 621983,
                    "size": 435,
                    "blocks": 1,
                    "atimeMs": 1582149188836.7058,
                    "mtimeMs": 1582149188837.1,
                    "ctimeMs": 1582149188837.1,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.837Z",
                    "mtime": "2020-02-19T21:53:08.837Z",
                    "ctime": "2020-02-19T21:53:08.837Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Dunilqiloli_itik\" data-name=\"Dunilqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 70.61133 68.50488\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "004 Elaqilotontli/00205-t__i__!_d.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/004 Elaqilotontli/00205-t__i__!_d.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 621984,
                    "size": 435,
                    "blocks": 1,
                    "atimeMs": 1582149188838.0347,
                    "mtimeMs": 1582149188838.4236,
                    "ctimeMs": 1582149188838.4236,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.838Z",
                    "mtime": "2020-02-19T21:53:08.838Z",
                    "ctime": "2020-02-19T21:53:08.838Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Dunilqiloli_itik\" data-name=\"Dunilqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 70.61133 68.50488\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "004 Elaqilotontli/00206-t__i__f.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/004 Elaqilotontli/00206-t__i__f.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 621993,
                    "size": 437,
                    "blocks": 1,
                    "atimeMs": 1582149188838.963,
                    "mtimeMs": 1582149188839.3884,
                    "ctimeMs": 1582149188839.3884,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.839Z",
                    "mtime": "2020-02-19T21:53:08.839Z",
                    "ctime": "2020-02-19T21:53:08.839Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Thamilqiloli_itik\" data-name=\"Thamilqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 55.57676 69.98151\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "004 Elaqilotontli/00207-t__i__!_f.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/004 Elaqilotontli/00207-t__i__!_f.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 622009,
                    "size": 437,
                    "blocks": 1,
                    "atimeMs": 1582149188840.303,
                    "mtimeMs": 1582149188840.7102,
                    "ctimeMs": 1582149188840.7102,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.840Z",
                    "mtime": "2020-02-19T21:53:08.841Z",
                    "ctime": "2020-02-19T21:53:08.841Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Thamilqiloli_itik\" data-name=\"Thamilqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 55.57676 69.98151\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "004 Elaqilotontli/00208-t__i__g.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/004 Elaqilotontli/00208-t__i__g.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 622010,
                    "size": 435,
                    "blocks": 1,
                    "atimeMs": 1582149188841.2217,
                    "mtimeMs": 1582149188841.6262,
                    "ctimeMs": 1582149188841.6262,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.841Z",
                    "mtime": "2020-02-19T21:53:08.842Z",
                    "ctime": "2020-02-19T21:53:08.842Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Galilqiloli_itik\" data-name=\"Galilqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 68.84685 69.99854\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "004 Elaqilotontli/00209-t__i__!_g.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/004 Elaqilotontli/00209-t__i__!_g.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 622011,
                    "size": 435,
                    "blocks": 1,
                    "atimeMs": 1582149188842.6724,
                    "mtimeMs": 1582149188843.1768,
                    "ctimeMs": 1582149188843.1768,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.843Z",
                    "mtime": "2020-02-19T21:53:08.843Z",
                    "ctime": "2020-02-19T21:53:08.843Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Galilqiloli_itik\" data-name=\"Galilqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 68.84685 69.99854\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "004 Elaqilotontli/00210-t__i__kh.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/004 Elaqilotontli/00210-t__i__kh.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 622012,
                    "size": 437,
                    "blocks": 1,
                    "atimeMs": 1582149188843.9153,
                    "mtimeMs": 1582149188844.3232,
                    "ctimeMs": 1582149188844.3232,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.844Z",
                    "mtime": "2020-02-19T21:53:08.844Z",
                    "ctime": "2020-02-19T21:53:08.844Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Khamalqiloli_itik\" data-name=\"Khamalqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 65.60791 63.15566\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "004 Elaqilotontli/00211-t__i__!_kh.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/004 Elaqilotontli/00211-t__i__!_kh.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 622013,
                    "size": 437,
                    "blocks": 1,
                    "atimeMs": 1582149188845.8904,
                    "mtimeMs": 1582149188846.2974,
                    "ctimeMs": 1582149188846.2974,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.846Z",
                    "mtime": "2020-02-19T21:53:08.846Z",
                    "ctime": "2020-02-19T21:53:08.846Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Khamalqiloli_itik\" data-name=\"Khamalqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 65.60791 63.15566\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "004 Elaqilotontli/00212-t__i__j.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/004 Elaqilotontli/00212-t__i__j.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 622014,
                    "size": 435,
                    "blocks": 1,
                    "atimeMs": 1582149188846.8118,
                    "mtimeMs": 1582149188847.2205,
                    "ctimeMs": 1582149188847.2205,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.847Z",
                    "mtime": "2020-02-19T21:53:08.847Z",
                    "ctime": "2020-02-19T21:53:08.847Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Jokilqiloli_itik\" data-name=\"Jokilqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 66.64662 67.44434\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "004 Elaqilotontli/00213-t__i__!_j.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/004 Elaqilotontli/00213-t__i__!_j.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 622015,
                    "size": 435,
                    "blocks": 1,
                    "atimeMs": 1582149188848.1345,
                    "mtimeMs": 1582149188848.531,
                    "ctimeMs": 1582149188848.531,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.848Z",
                    "mtime": "2020-02-19T21:53:08.849Z",
                    "ctime": "2020-02-19T21:53:08.849Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Jokilqiloli_itik\" data-name=\"Jokilqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 66.64662 67.44434\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "004 Elaqilotontli/00214-t__i__r.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/004 Elaqilotontli/00214-t__i__r.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 622016,
                    "size": 435,
                    "blocks": 1,
                    "atimeMs": 1582149188849.041,
                    "mtimeMs": 1582149188849.4524,
                    "ctimeMs": 1582149188849.4524,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.849Z",
                    "mtime": "2020-02-19T21:53:08.849Z",
                    "ctime": "2020-02-19T21:53:08.849Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Runalqiloli_itik\" data-name=\"Runalqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 69.46289 67.47461\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "004 Elaqilotontli/00215-t__i__!_r.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/004 Elaqilotontli/00215-t__i__!_r.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 622017,
                    "size": 435,
                    "blocks": 1,
                    "atimeMs": 1582149188850.3652,
                    "mtimeMs": 1582149188850.7498,
                    "ctimeMs": 1582149188850.7498,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.850Z",
                    "mtime": "2020-02-19T21:53:08.851Z",
                    "ctime": "2020-02-19T21:53:08.851Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Runalqiloli_itik\" data-name=\"Runalqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 69.46289 67.47461\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "004 Elaqilotontli/00216-t__i__z'.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/004 Elaqilotontli/00216-t__i__z'.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 622018,
                    "size": 435,
                    "blocks": 1,
                    "atimeMs": 1582149188851.2405,
                    "mtimeMs": 1582149188851.6228,
                    "ctimeMs": 1582149188851.6228,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.851Z",
                    "mtime": "2020-02-19T21:53:08.852Z",
                    "ctime": "2020-02-19T21:53:08.852Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Zawilqiloli_itik\" data-name=\"Zawilqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 76.99609 66.84766\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "004 Elaqilotontli/00217-t__i__!_z'.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/004 Elaqilotontli/00217-t__i__!_z'.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 622019,
                    "size": 435,
                    "blocks": 1,
                    "atimeMs": 1582149188853.2278,
                    "mtimeMs": 1582149188853.703,
                    "ctimeMs": 1582149188853.703,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.853Z",
                    "mtime": "2020-02-19T21:53:08.854Z",
                    "ctime": "2020-02-19T21:53:08.854Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Zawilqiloli_itik\" data-name=\"Zawilqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 76.99609 66.84766\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "004 Elaqilotontli/00218-t__i__v.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/004 Elaqilotontli/00218-t__i__v.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 622020,
                    "size": 435,
                    "blocks": 1,
                    "atimeMs": 1582149188854.2134,
                    "mtimeMs": 1582149188854.5918,
                    "ctimeMs": 1582149188854.5918,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.854Z",
                    "mtime": "2020-02-19T21:53:08.855Z",
                    "ctime": "2020-02-19T21:53:08.855Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Verolqiloli_itik\" data-name=\"Verolqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 77.05322 69.88477\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "004 Elaqilotontli/00219-t__i__!_v.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/004 Elaqilotontli/00219-t__i__!_v.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 622021,
                    "size": 435,
                    "blocks": 1,
                    "atimeMs": 1582149188855.5356,
                    "mtimeMs": 1582149188855.9038,
                    "ctimeMs": 1582149188855.9038,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.856Z",
                    "mtime": "2020-02-19T21:53:08.856Z",
                    "ctime": "2020-02-19T21:53:08.856Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Verolqiloli_itik\" data-name=\"Verolqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 77.05322 69.88477\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "004 Elaqilotontli/00220-t__i__ks.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/004 Elaqilotontli/00220-t__i__ks.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 622022,
                    "size": 437,
                    "blocks": 1,
                    "atimeMs": 1582149188856.4329,
                    "mtimeMs": 1582149188856.795,
                    "ctimeMs": 1582149188856.795,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.856Z",
                    "mtime": "2020-02-19T21:53:08.857Z",
                    "ctime": "2020-02-19T21:53:08.857Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Ksimalqiloli_itik\" data-name=\"Ksimalqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 76.99609 68.44629\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "004 Elaqilotontli/00221-t__i__!_ks.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/004 Elaqilotontli/00221-t__i__!_ks.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 622023,
                    "size": 437,
                    "blocks": 1,
                    "atimeMs": 1582149188857.6836,
                    "mtimeMs": 1582149188858.0413,
                    "ctimeMs": 1582149188858.0413,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.858Z",
                    "mtime": "2020-02-19T21:53:08.858Z",
                    "ctime": "2020-02-19T21:53:08.858Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Ksimalqiloli_itik\" data-name=\"Ksimalqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 76.99609 68.44629\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "004 Elaqilotontli/00222-t__i__ny.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/004 Elaqilotontli/00222-t__i__ny.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 622050,
                    "size": 437,
                    "blocks": 1,
                    "atimeMs": 1582149188858.885,
                    "mtimeMs": 1582149188859.2688,
                    "ctimeMs": 1582149188859.2688,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.859Z",
                    "mtime": "2020-02-19T21:53:08.859Z",
                    "ctime": "2020-02-19T21:53:08.859Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Nyonilqiloli_itik\" data-name=\"Nyonilqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 75.40479 65.39844\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "004 Elaqilotontli/00223-t__i__!_ny.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/004 Elaqilotontli/00223-t__i__!_ny.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 622084,
                    "size": 437,
                    "blocks": 1,
                    "atimeMs": 1582149188860.1458,
                    "mtimeMs": 1582149188860.517,
                    "ctimeMs": 1582149188860.517,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.860Z",
                    "mtime": "2020-02-19T21:53:08.861Z",
                    "ctime": "2020-02-19T21:53:08.861Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Nyonilqiloli_itik\" data-name=\"Nyonilqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 75.40479 65.39844\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "005 Mokawaqilotontli/00224-t~.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/005 Mokawaqilotontli/00224-t~.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 622948,
                    "size": 401,
                    "blocks": 1,
                    "atimeMs": 1582149112577.1533,
                    "mtimeMs": 1582149188860.978,
                    "ctimeMs": 1582149188860.978,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:51:52.577Z",
                    "mtime": "2020-02-19T21:53:08.861Z",
                    "ctime": "2020-02-19T21:53:08.861Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Tekolqiloli\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 90.71191 98.22174\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "006 Unaliqilotontli/00225-t_u.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/006 Unaliqilotontli/00225-t_u.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 622950,
                    "size": 401,
                    "blocks": 1,
                    "atimeMs": 1582149112577.9463,
                    "mtimeMs": 1582149188861.5122,
                    "ctimeMs": 1582149188861.5122,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:51:52.578Z",
                    "mtime": "2020-02-19T21:53:08.862Z",
                    "ctime": "2020-02-19T21:53:08.862Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Tekolqiloli\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 90.71191 98.22174\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "006 Unaliqilotontli/00226-t_u'.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/006 Unaliqilotontli/00226-t_u'.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 622951,
                    "size": 425,
                    "blocks": 1,
                    "atimeMs": 1582149188862.3628,
                    "mtimeMs": 1582149188862.7175,
                    "ctimeMs": 1582149188862.7175,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.862Z",
                    "mtime": "2020-02-19T21:53:08.863Z",
                    "ctime": "2020-02-19T21:53:08.863Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Hekalqilotontli_1\" data-name=\"Hekalqilotontli 1\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 33 33\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "006 Unaliqilotontli/00227-t_u!.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/006 Unaliqilotontli/00227-t_u!.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 622952,
                    "size": 416,
                    "blocks": 1,
                    "atimeMs": 1582149188863.2764,
                    "mtimeMs": 1582149188863.6123,
                    "ctimeMs": 1582149188863.6123,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.863Z",
                    "mtime": "2020-02-19T21:53:08.864Z",
                    "ctime": "2020-02-19T21:53:08.864Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Weyakqiloli_1\" data-name=\"Weyakqiloli 1\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 80 6\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "006 Unaliqilotontli/00228-t_u!_'.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/006 Unaliqilotontli/00228-t_u!_'.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 622953,
                    "size": 425,
                    "blocks": 1,
                    "atimeMs": 1582149188864.5967,
                    "mtimeMs": 1582149188864.991,
                    "ctimeMs": 1582149188864.991,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.865Z",
                    "mtime": "2020-02-19T21:53:08.865Z",
                    "ctime": "2020-02-19T21:53:08.865Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Hekalqilotontli_1\" data-name=\"Hekalqilotontli 1\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 33 33\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "006 Unaliqilotontli/00229-t__u__m.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/006 Unaliqilotontli/00229-t__u__m.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 622954,
                    "size": 433,
                    "blocks": 1,
                    "atimeMs": 1582149188865.97,
                    "mtimeMs": 1582149188866.75,
                    "ctimeMs": 1582149188866.75,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.866Z",
                    "mtime": "2020-02-19T21:53:08.867Z",
                    "ctime": "2020-02-19T21:53:08.867Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Mailqiloli_itik\" data-name=\"Mailqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 75.41407 65.86328\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "006 Unaliqilotontli/00230-t__u__!_m.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/006 Unaliqilotontli/00230-t__u__!_m.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 622955,
                    "size": 433,
                    "blocks": 1,
                    "atimeMs": 1582149188883.6626,
                    "mtimeMs": 1582149188884.2007,
                    "ctimeMs": 1582149188884.2007,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.884Z",
                    "mtime": "2020-02-19T21:53:08.884Z",
                    "ctime": "2020-02-19T21:53:08.884Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Mailqiloli_itik\" data-name=\"Mailqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 75.41407 65.86328\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "006 Unaliqilotontli/00231-t__u__n.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/006 Unaliqilotontli/00231-t__u__n.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 622956,
                    "size": 434,
                    "blocks": 1,
                    "atimeMs": 1582149188884.8452,
                    "mtimeMs": 1582149188885.2932,
                    "ctimeMs": 1582149188885.2932,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.885Z",
                    "mtime": "2020-02-19T21:53:08.885Z",
                    "ctime": "2020-02-19T21:53:08.885Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Neliwqiloli_itik\" data-name=\"Neliwqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 65.4248 65.38281\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "006 Unaliqilotontli/00232-t__u__!_n.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/006 Unaliqilotontli/00232-t__u__!_n.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 622957,
                    "size": 434,
                    "blocks": 1,
                    "atimeMs": 1582149188886.4016,
                    "mtimeMs": 1582149188886.8604,
                    "ctimeMs": 1582149188886.8604,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.886Z",
                    "mtime": "2020-02-19T21:53:08.887Z",
                    "ctime": "2020-02-19T21:53:08.887Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Neliwqiloli_itik\" data-name=\"Neliwqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 65.4248 65.38281\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "006 Unaliqilotontli/00233-t__u__p.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/006 Unaliqilotontli/00233-t__u__p.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 622958,
                    "size": 435,
                    "blocks": 1,
                    "atimeMs": 1582149188887.491,
                    "mtimeMs": 1582149188888.359,
                    "ctimeMs": 1582149188888.359,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.887Z",
                    "mtime": "2020-02-19T21:53:08.888Z",
                    "ctime": "2020-02-19T21:53:08.888Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Pokalqiloli_itik\" data-name=\"Pokalqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 55.57676 69.98151\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "006 Unaliqilotontli/00234-t__u__!_p.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/006 Unaliqilotontli/00234-t__u__!_p.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 622959,
                    "size": 435,
                    "blocks": 1,
                    "atimeMs": 1582149188889.44,
                    "mtimeMs": 1582149188889.8408,
                    "ctimeMs": 1582149188889.8408,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.889Z",
                    "mtime": "2020-02-19T21:53:08.890Z",
                    "ctime": "2020-02-19T21:53:08.890Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Pokalqiloli_itik\" data-name=\"Pokalqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 55.57676 69.98151\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "006 Unaliqilotontli/00235-t__u__t.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/006 Unaliqilotontli/00235-t__u__t.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 622960,
                    "size": 435,
                    "blocks": 1,
                    "atimeMs": 1582149188890.399,
                    "mtimeMs": 1582149188890.7888,
                    "ctimeMs": 1582149188890.7888,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.890Z",
                    "mtime": "2020-02-19T21:53:08.891Z",
                    "ctime": "2020-02-19T21:53:08.891Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Tekolqiloli_itik\" data-name=\"Tekolqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 70.61133 68.50488\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "006 Unaliqilotontli/00236-t__u__!_t.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/006 Unaliqilotontli/00236-t__u__!_t.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 622962,
                    "size": 435,
                    "blocks": 1,
                    "atimeMs": 1582149188891.7375,
                    "mtimeMs": 1582149188892.1038,
                    "ctimeMs": 1582149188892.1038,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.892Z",
                    "mtime": "2020-02-19T21:53:08.892Z",
                    "ctime": "2020-02-19T21:53:08.892Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Tekolqiloli_itik\" data-name=\"Tekolqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 70.61133 68.50488\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "006 Unaliqilotontli/00237-t__u__tl.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/006 Unaliqilotontli/00237-t__u__tl.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 622976,
                    "size": 439,
                    "blocks": 1,
                    "atimeMs": 1582149188892.645,
                    "mtimeMs": 1582149188893.0547,
                    "ctimeMs": 1582149188893.0547,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.893Z",
                    "mtime": "2020-02-19T21:53:08.893Z",
                    "ctime": "2020-02-19T21:53:08.893Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Tlachalqiloli_itik\" data-name=\"Tlachalqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 73.98493 69.98828\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "006 Unaliqilotontli/00238-t__u__!_tl.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/006 Unaliqilotontli/00238-t__u__!_tl.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 622985,
                    "size": 439,
                    "blocks": 1,
                    "atimeMs": 1582149188894.4568,
                    "mtimeMs": 1582149188894.8655,
                    "ctimeMs": 1582149188894.8655,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.894Z",
                    "mtime": "2020-02-19T21:53:08.895Z",
                    "ctime": "2020-02-19T21:53:08.895Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Tlachalqiloli_itik\" data-name=\"Tlachalqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 73.98493 69.98828\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "006 Unaliqilotontli/00239-t__u__x.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/006 Unaliqilotontli/00239-t__u__x.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 623096,
                    "size": 435,
                    "blocks": 1,
                    "atimeMs": 1582149188895.4285,
                    "mtimeMs": 1582149188895.834,
                    "ctimeMs": 1582149188895.834,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.895Z",
                    "mtime": "2020-02-19T21:53:08.896Z",
                    "ctime": "2020-02-19T21:53:08.896Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Xocilqiloli_itik\" data-name=\"Xocilqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 58.64222 66.89453\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "006 Unaliqilotontli/00240-t__u__!_x.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/006 Unaliqilotontli/00240-t__u__!_x.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 623104,
                    "size": 435,
                    "blocks": 1,
                    "atimeMs": 1582149188896.8025,
                    "mtimeMs": 1582149188897.2048,
                    "ctimeMs": 1582149188897.2048,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.897Z",
                    "mtime": "2020-02-19T21:53:08.897Z",
                    "ctime": "2020-02-19T21:53:08.897Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Xocilqiloli_itik\" data-name=\"Xocilqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 58.64222 66.89453\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "006 Unaliqilotontli/00241-t__u__c.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/006 Unaliqilotontli/00241-t__u__c.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 623336,
                    "size": 437,
                    "blocks": 1,
                    "atimeMs": 1582149188897.788,
                    "mtimeMs": 1582149188898.2202,
                    "ctimeMs": 1582149188898.2202,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.898Z",
                    "mtime": "2020-02-19T21:53:08.898Z",
                    "ctime": "2020-02-19T21:53:08.898Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Chakalqiloli_itik\" data-name=\"Chakalqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 58.00381 69.90327\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "006 Unaliqilotontli/00242-t__u__!_c.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/006 Unaliqilotontli/00242-t__u__!_c.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 623337,
                    "size": 437,
                    "blocks": 1,
                    "atimeMs": 1582149188899.208,
                    "mtimeMs": 1582149188899.751,
                    "ctimeMs": 1582149188899.751,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.899Z",
                    "mtime": "2020-02-19T21:53:08.900Z",
                    "ctime": "2020-02-19T21:53:08.900Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Chakalqiloli_itik\" data-name=\"Chakalqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 58.00381 69.90327\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "006 Unaliqilotontli/00243-t__u__s.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/006 Unaliqilotontli/00243-t__u__s.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 623338,
                    "size": 437,
                    "blocks": 1,
                    "atimeMs": 1582149188900.4038,
                    "mtimeMs": 1582149188900.7812,
                    "ctimeMs": 1582149188900.7812,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.900Z",
                    "mtime": "2020-02-19T21:53:08.901Z",
                    "ctime": "2020-02-19T21:53:08.901Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Senzonqiloli_itik\" data-name=\"Senzonqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 77.00781 67.02148\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "006 Unaliqilotontli/00244-t__u__!_s.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/006 Unaliqilotontli/00244-t__u__!_s.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 623354,
                    "size": 437,
                    "blocks": 1,
                    "atimeMs": 1582149188901.7568,
                    "mtimeMs": 1582149188902.1113,
                    "ctimeMs": 1582149188902.1113,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.902Z",
                    "mtime": "2020-02-19T21:53:08.902Z",
                    "ctime": "2020-02-19T21:53:08.902Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Senzonqiloli_itik\" data-name=\"Senzonqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 77.00781 67.02148\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "006 Unaliqilotontli/00245-t__u__z.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/006 Unaliqilotontli/00245-t__u__z.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 623355,
                    "size": 433,
                    "blocks": 1,
                    "atimeMs": 1582149188902.6807,
                    "mtimeMs": 1582149188903.5732,
                    "ctimeMs": 1582149188903.5732,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.903Z",
                    "mtime": "2020-02-19T21:53:08.904Z",
                    "ctime": "2020-02-19T21:53:08.904Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Zopilqiloli_itik\" data-name=\"Zopilqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 77.00781 68.625\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "006 Unaliqilotontli/00246-t__u__!_z.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/006 Unaliqilotontli/00246-t__u__!_z.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 623356,
                    "size": 433,
                    "blocks": 1,
                    "atimeMs": 1582149188904.6953,
                    "mtimeMs": 1582149188905.0732,
                    "ctimeMs": 1582149188905.0732,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.905Z",
                    "mtime": "2020-02-19T21:53:08.905Z",
                    "ctime": "2020-02-19T21:53:08.905Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Zopilqiloli_itik\" data-name=\"Zopilqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 77.00781 68.625\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "006 Unaliqilotontli/00247-t__u__k.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/006 Unaliqilotontli/00247-t__u__k.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 623362,
                    "size": 433,
                    "blocks": 1,
                    "atimeMs": 1582149188905.6433,
                    "mtimeMs": 1582149188906.1086,
                    "ctimeMs": 1582149188906.1086,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.906Z",
                    "mtime": "2020-02-19T21:53:08.906Z",
                    "ctime": "2020-02-19T21:53:08.906Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Coalqiloli_itik\" data-name=\"Coalqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 68.84783 69.99839\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "006 Unaliqilotontli/00248-t__u__!_k.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/006 Unaliqilotontli/00248-t__u__!_k.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 623363,
                    "size": 433,
                    "blocks": 1,
                    "atimeMs": 1582149188907.097,
                    "mtimeMs": 1582149188907.4536,
                    "ctimeMs": 1582149188907.4536,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.907Z",
                    "mtime": "2020-02-19T21:53:08.907Z",
                    "ctime": "2020-02-19T21:53:08.907Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Coalqiloli_itik\" data-name=\"Coalqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 68.84783 69.99839\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "006 Unaliqilotontli/00249-t__u__q.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/006 Unaliqilotontli/00249-t__u__q.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 623364,
                    "size": 435,
                    "blocks": 1,
                    "atimeMs": 1582149188908.021,
                    "mtimeMs": 1582149188908.3816,
                    "ctimeMs": 1582149188908.3816,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.908Z",
                    "mtime": "2020-02-19T21:53:08.908Z",
                    "ctime": "2020-02-19T21:53:08.908Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Qaqawqiloli_itik\" data-name=\"Qaqawqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 67.13916 68.67383\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "006 Unaliqilotontli/00250-t__u__!_q.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/006 Unaliqilotontli/00250-t__u__!_q.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 623585,
                    "size": 435,
                    "blocks": 1,
                    "atimeMs": 1582149188909.3823,
                    "mtimeMs": 1582149188909.7314,
                    "ctimeMs": 1582149188909.7314,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.909Z",
                    "mtime": "2020-02-19T21:53:08.910Z",
                    "ctime": "2020-02-19T21:53:08.910Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Qaqawqiloli_itik\" data-name=\"Qaqawqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 67.13916 68.67383\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "006 Unaliqilotontli/00251-t__u__h.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/006 Unaliqilotontli/00251-t__u__h.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 623586,
                    "size": 433,
                    "blocks": 1,
                    "atimeMs": 1582149188910.302,
                    "mtimeMs": 1582149188910.6675,
                    "ctimeMs": 1582149188910.6675,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.910Z",
                    "mtime": "2020-02-19T21:53:08.911Z",
                    "ctime": "2020-02-19T21:53:08.911Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Hekalqiloli_itik\" data-name=\"Hekalqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 60.0387 63.1527\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "006 Unaliqilotontli/00252-t__u__!_h.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/006 Unaliqilotontli/00252-t__u__!_h.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 623696,
                    "size": 433,
                    "blocks": 1,
                    "atimeMs": 1582149188911.7554,
                    "mtimeMs": 1582149188912.1262,
                    "ctimeMs": 1582149188912.1262,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.912Z",
                    "mtime": "2020-02-19T21:53:08.912Z",
                    "ctime": "2020-02-19T21:53:08.912Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Hekalqiloli_itik\" data-name=\"Hekalqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 60.0387 63.1527\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "006 Unaliqilotontli/00253-t__u__l.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/006 Unaliqilotontli/00253-t__u__l.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 623698,
                    "size": 435,
                    "blocks": 1,
                    "atimeMs": 1582149188912.6914,
                    "mtimeMs": 1582149188913.0737,
                    "ctimeMs": 1582149188913.0737,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.913Z",
                    "mtime": "2020-02-19T21:53:08.913Z",
                    "ctime": "2020-02-19T21:53:08.913Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Limonqiloli_itik\" data-name=\"Limonqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 56.86193 67.47461\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "006 Unaliqilotontli/00254-t__u__!_l.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/006 Unaliqilotontli/00254-t__u__!_l.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 623699,
                    "size": 435,
                    "blocks": 1,
                    "atimeMs": 1582149188914.0264,
                    "mtimeMs": 1582149188914.406,
                    "ctimeMs": 1582149188914.406,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.914Z",
                    "mtime": "2020-02-19T21:53:08.914Z",
                    "ctime": "2020-02-19T21:53:08.914Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Limonqiloli_itik\" data-name=\"Limonqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 56.86193 67.47461\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "006 Unaliqilotontli/00255-t__u__w.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/006 Unaliqilotontli/00255-t__u__w.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 623929,
                    "size": 435,
                    "blocks": 1,
                    "atimeMs": 1582149188914.9368,
                    "mtimeMs": 1582149188915.296,
                    "ctimeMs": 1582149188915.296,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.915Z",
                    "mtime": "2020-02-19T21:53:08.915Z",
                    "ctime": "2020-02-19T21:53:08.915Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Weyilqiloli_itik\" data-name=\"Weyilqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 77.05322 69.88477\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "006 Unaliqilotontli/00256-t__u__!_w.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/006 Unaliqilotontli/00256-t__u__!_w.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 623930,
                    "size": 435,
                    "blocks": 1,
                    "atimeMs": 1582149188916.5056,
                    "mtimeMs": 1582149188916.8616,
                    "ctimeMs": 1582149188916.8616,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.917Z",
                    "mtime": "2020-02-19T21:53:08.917Z",
                    "ctime": "2020-02-19T21:53:08.917Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Weyilqiloli_itik\" data-name=\"Weyilqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 77.05322 69.88477\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "006 Unaliqilotontli/00257-t__u__y.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/006 Unaliqilotontli/00257-t__u__y.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 623931,
                    "size": 435,
                    "blocks": 1,
                    "atimeMs": 1582149188917.4187,
                    "mtimeMs": 1582149188917.8232,
                    "ctimeMs": 1582149188917.8232,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.917Z",
                    "mtime": "2020-02-19T21:53:08.918Z",
                    "ctime": "2020-02-19T21:53:08.918Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Yololqiloli_itik\" data-name=\"Yololqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 59.96725 67.44629\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "006 Unaliqilotontli/00258-t__u__!_y.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/006 Unaliqilotontli/00258-t__u__!_y.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 623939,
                    "size": 435,
                    "blocks": 1,
                    "atimeMs": 1582149188918.7646,
                    "mtimeMs": 1582149188919.1152,
                    "ctimeMs": 1582149188919.1152,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.919Z",
                    "mtime": "2020-02-19T21:53:08.919Z",
                    "ctime": "2020-02-19T21:53:08.919Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Yololqiloli_itik\" data-name=\"Yololqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 59.96725 67.44629\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "006 Unaliqilotontli/00259-t__u__b.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/006 Unaliqilotontli/00259-t__u__b.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 623941,
                    "size": 435,
                    "blocks": 1,
                    "atimeMs": 1582149188919.6775,
                    "mtimeMs": 1582149188920.2268,
                    "ctimeMs": 1582149188920.2268,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.920Z",
                    "mtime": "2020-02-19T21:53:08.920Z",
                    "ctime": "2020-02-19T21:53:08.920Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Balamqiloli_itik\" data-name=\"Balamqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 75.41407 65.86328\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "006 Unaliqilotontli/00260-t__u__!_b.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/006 Unaliqilotontli/00260-t__u__!_b.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 623955,
                    "size": 435,
                    "blocks": 1,
                    "atimeMs": 1582149188921.325,
                    "mtimeMs": 1582149188921.764,
                    "ctimeMs": 1582149188921.764,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.921Z",
                    "mtime": "2020-02-19T21:53:08.922Z",
                    "ctime": "2020-02-19T21:53:08.922Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Balamqiloli_itik\" data-name=\"Balamqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 75.41407 65.86328\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "006 Unaliqilotontli/00261-t__u__d.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/006 Unaliqilotontli/00261-t__u__d.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 623957,
                    "size": 435,
                    "blocks": 1,
                    "atimeMs": 1582149188922.323,
                    "mtimeMs": 1582149188922.7192,
                    "ctimeMs": 1582149188922.7192,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.922Z",
                    "mtime": "2020-02-19T21:53:08.923Z",
                    "ctime": "2020-02-19T21:53:08.923Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Dunilqiloli_itik\" data-name=\"Dunilqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 70.61133 68.50488\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "006 Unaliqilotontli/00262-t__u__!_d.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/006 Unaliqilotontli/00262-t__u__!_d.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 624120,
                    "size": 435,
                    "blocks": 1,
                    "atimeMs": 1582149188923.9692,
                    "mtimeMs": 1582149188924.3535,
                    "ctimeMs": 1582149188924.3535,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.924Z",
                    "mtime": "2020-02-19T21:53:08.924Z",
                    "ctime": "2020-02-19T21:53:08.924Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Dunilqiloli_itik\" data-name=\"Dunilqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 70.61133 68.50488\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "006 Unaliqilotontli/00263-t__u__f.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/006 Unaliqilotontli/00263-t__u__f.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 624121,
                    "size": 437,
                    "blocks": 1,
                    "atimeMs": 1582149188924.8955,
                    "mtimeMs": 1582149188925.4043,
                    "ctimeMs": 1582149188925.4043,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.925Z",
                    "mtime": "2020-02-19T21:53:08.925Z",
                    "ctime": "2020-02-19T21:53:08.925Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Thamilqiloli_itik\" data-name=\"Thamilqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 55.57676 69.98151\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "006 Unaliqilotontli/00264-t__u__!_f.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/006 Unaliqilotontli/00264-t__u__!_f.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 624122,
                    "size": 437,
                    "blocks": 1,
                    "atimeMs": 1582149188926.3848,
                    "mtimeMs": 1582149188926.7795,
                    "ctimeMs": 1582149188926.7795,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.926Z",
                    "mtime": "2020-02-19T21:53:08.927Z",
                    "ctime": "2020-02-19T21:53:08.927Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Thamilqiloli_itik\" data-name=\"Thamilqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 55.57676 69.98151\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "006 Unaliqilotontli/00265-t__u__g.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/006 Unaliqilotontli/00265-t__u__g.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 632146,
                    "size": 435,
                    "blocks": 1,
                    "atimeMs": 1582149188927.3457,
                    "mtimeMs": 1582149188927.7686,
                    "ctimeMs": 1582149188927.7686,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.927Z",
                    "mtime": "2020-02-19T21:53:08.928Z",
                    "ctime": "2020-02-19T21:53:08.928Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Galilqiloli_itik\" data-name=\"Galilqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 68.84685 69.99854\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "006 Unaliqilotontli/00266-t__u__!_g.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/006 Unaliqilotontli/00266-t__u__!_g.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 644256,
                    "size": 435,
                    "blocks": 1,
                    "atimeMs": 1582149188928.955,
                    "mtimeMs": 1582149188929.5054,
                    "ctimeMs": 1582149188929.5054,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.929Z",
                    "mtime": "2020-02-19T21:53:08.930Z",
                    "ctime": "2020-02-19T21:53:08.930Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Galilqiloli_itik\" data-name=\"Galilqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 68.84685 69.99854\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "006 Unaliqilotontli/00267-t__u__kh.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/006 Unaliqilotontli/00267-t__u__kh.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 644257,
                    "size": 437,
                    "blocks": 1,
                    "atimeMs": 1582149188930.3635,
                    "mtimeMs": 1582149188930.9956,
                    "ctimeMs": 1582149188930.9956,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.930Z",
                    "mtime": "2020-02-19T21:53:08.931Z",
                    "ctime": "2020-02-19T21:53:08.931Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Khamalqiloli_itik\" data-name=\"Khamalqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 65.60791 63.15566\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "006 Unaliqilotontli/00268-t__u__!_kh.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/006 Unaliqilotontli/00268-t__u__!_kh.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 644258,
                    "size": 437,
                    "blocks": 1,
                    "atimeMs": 1582149188932.2566,
                    "mtimeMs": 1582149188932.8086,
                    "ctimeMs": 1582149188932.8086,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.932Z",
                    "mtime": "2020-02-19T21:53:08.933Z",
                    "ctime": "2020-02-19T21:53:08.933Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Khamalqiloli_itik\" data-name=\"Khamalqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 65.60791 63.15566\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "006 Unaliqilotontli/00269-t__u__j.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/006 Unaliqilotontli/00269-t__u__j.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 644319,
                    "size": 435,
                    "blocks": 1,
                    "atimeMs": 1582149188933.7056,
                    "mtimeMs": 1582149188934.1382,
                    "ctimeMs": 1582149188934.1382,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.934Z",
                    "mtime": "2020-02-19T21:53:08.934Z",
                    "ctime": "2020-02-19T21:53:08.934Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Jokilqiloli_itik\" data-name=\"Jokilqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 66.64662 67.44434\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "006 Unaliqilotontli/00270-t__u__!_j.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/006 Unaliqilotontli/00270-t__u__!_j.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 644320,
                    "size": 435,
                    "blocks": 1,
                    "atimeMs": 1582149188935.3748,
                    "mtimeMs": 1582149188935.8247,
                    "ctimeMs": 1582149188935.8247,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.935Z",
                    "mtime": "2020-02-19T21:53:08.936Z",
                    "ctime": "2020-02-19T21:53:08.936Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Jokilqiloli_itik\" data-name=\"Jokilqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 66.64662 67.44434\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "006 Unaliqilotontli/00271-t__u__r.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/006 Unaliqilotontli/00271-t__u__r.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 644321,
                    "size": 435,
                    "blocks": 1,
                    "atimeMs": 1582149188936.4297,
                    "mtimeMs": 1582149188936.8928,
                    "ctimeMs": 1582149188936.8928,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.936Z",
                    "mtime": "2020-02-19T21:53:08.937Z",
                    "ctime": "2020-02-19T21:53:08.937Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Runalqiloli_itik\" data-name=\"Runalqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 69.46289 67.47461\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "006 Unaliqilotontli/00272-t__u__!_r.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/006 Unaliqilotontli/00272-t__u__!_r.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 667654,
                    "size": 435,
                    "blocks": 1,
                    "atimeMs": 1582149188937.9758,
                    "mtimeMs": 1582149188938.3635,
                    "ctimeMs": 1582149188938.3635,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.938Z",
                    "mtime": "2020-02-19T21:53:08.938Z",
                    "ctime": "2020-02-19T21:53:08.938Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Runalqiloli_itik\" data-name=\"Runalqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 69.46289 67.47461\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "006 Unaliqilotontli/00273-t__u__z'.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/006 Unaliqilotontli/00273-t__u__z'.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 667655,
                    "size": 435,
                    "blocks": 1,
                    "atimeMs": 1582149188938.9346,
                    "mtimeMs": 1582149188939.2998,
                    "ctimeMs": 1582149188939.2998,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.939Z",
                    "mtime": "2020-02-19T21:53:08.939Z",
                    "ctime": "2020-02-19T21:53:08.939Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Zawilqiloli_itik\" data-name=\"Zawilqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 76.99609 66.84766\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "006 Unaliqilotontli/00274-t__u__!_z'.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/006 Unaliqilotontli/00274-t__u__!_z'.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 667656,
                    "size": 435,
                    "blocks": 1,
                    "atimeMs": 1582149188940.2224,
                    "mtimeMs": 1582149188940.612,
                    "ctimeMs": 1582149188940.612,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.940Z",
                    "mtime": "2020-02-19T21:53:08.941Z",
                    "ctime": "2020-02-19T21:53:08.941Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Zawilqiloli_itik\" data-name=\"Zawilqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 76.99609 66.84766\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "006 Unaliqilotontli/00275-t__u__v.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/006 Unaliqilotontli/00275-t__u__v.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 686722,
                    "size": 435,
                    "blocks": 1,
                    "atimeMs": 1582149188941.1592,
                    "mtimeMs": 1582149188941.5505,
                    "ctimeMs": 1582149188941.5505,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.941Z",
                    "mtime": "2020-02-19T21:53:08.942Z",
                    "ctime": "2020-02-19T21:53:08.942Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Verolqiloli_itik\" data-name=\"Verolqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 77.05322 69.88477\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "006 Unaliqilotontli/00276-t__u__!_v.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/006 Unaliqilotontli/00276-t__u__!_v.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 686731,
                    "size": 435,
                    "blocks": 1,
                    "atimeMs": 1582149188942.621,
                    "mtimeMs": 1582149188942.9902,
                    "ctimeMs": 1582149188942.9902,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.943Z",
                    "mtime": "2020-02-19T21:53:08.943Z",
                    "ctime": "2020-02-19T21:53:08.943Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Verolqiloli_itik\" data-name=\"Verolqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 77.05322 69.88477\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "006 Unaliqilotontli/00277-t__u__ks.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/006 Unaliqilotontli/00277-t__u__ks.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 686739,
                    "size": 437,
                    "blocks": 1,
                    "atimeMs": 1582149188943.601,
                    "mtimeMs": 1582149188944.0088,
                    "ctimeMs": 1582149188944.0088,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.944Z",
                    "mtime": "2020-02-19T21:53:08.944Z",
                    "ctime": "2020-02-19T21:53:08.944Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Ksimalqiloli_itik\" data-name=\"Ksimalqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 76.99609 68.44629\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "006 Unaliqilotontli/00278-t__u__!_ks.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/006 Unaliqilotontli/00278-t__u__!_ks.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 686803,
                    "size": 437,
                    "blocks": 1,
                    "atimeMs": 1582149188945.01,
                    "mtimeMs": 1582149188945.439,
                    "ctimeMs": 1582149188945.439,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.945Z",
                    "mtime": "2020-02-19T21:53:08.945Z",
                    "ctime": "2020-02-19T21:53:08.945Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Ksimalqiloli_itik\" data-name=\"Ksimalqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 76.99609 68.44629\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "006 Unaliqilotontli/00279-t__u__ny.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/006 Unaliqilotontli/00279-t__u__ny.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 686822,
                    "size": 437,
                    "blocks": 1,
                    "atimeMs": 1582149188946.0413,
                    "mtimeMs": 1582149188946.486,
                    "ctimeMs": 1582149188946.486,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.946Z",
                    "mtime": "2020-02-19T21:53:08.946Z",
                    "ctime": "2020-02-19T21:53:08.946Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Nyonilqiloli_itik\" data-name=\"Nyonilqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 75.40479 65.39844\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }, {
                "fileName": "006 Unaliqilotontli/00280-t__u__!_ny.svg",
                "filePath": "/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/T/006 Unaliqilotontli/00280-t__u__!_ny.svg",
                "stat": {
                    "dev": 2065,
                    "mode": 33279,
                    "nlink": 1,
                    "uid": 0,
                    "gid": 0,
                    "rdev": 0,
                    "blksize": 4096,
                    "ino": 686825,
                    "size": 437,
                    "blocks": 1,
                    "atimeMs": 1582149188947.506,
                    "mtimeMs": 1582149188947.89,
                    "ctimeMs": 1582149188947.89,
                    "birthtimeMs": 0,
                    "atime": "2020-02-19T21:53:08.948Z",
                    "mtime": "2020-02-19T21:53:08.948Z",
                    "ctime": "2020-02-19T21:53:08.948Z",
                    "birthtime": "1970-01-01T00:00:00.000Z"
                },
                "content": "<svg id=\"Nyonilqiloli_itik\" data-name=\"Nyonilqiloli itik\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 75.40479 65.39844\"><style type=\"text/css\">\r\n.st0{display:none;}\r\n.st1{display:inline;fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}\r\n.st2{display:inline;fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}\r\n.st3{display:inline;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\r</style></svg>"
            }];
            const results = xachama.formCharacterSets('t', fullSets);

            assert.strictEqual(results.length, expectedResult.length);

            for (const i of results)
                assert.deepEqual(results[i], expectedResult[i]);
        });
    });

    suiteTeardown(() => {
        xachama.setDir('/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/');
        xachama.removeDir()
    });
});
