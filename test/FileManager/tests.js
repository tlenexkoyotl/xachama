const assert = require('chai').assert;
const FileManager = require('../../src/FileManager');
const dir = 'data/Base';
const fm = new FileManager(dir);

suite('FileManager tests', () => {
    suiteSetup(() => {
        const oldPath = fm.getDir();
        const newPath = oldPath.replace('data/Base', 'test/data/Base');

        fm.setDir(newPath);
        fm.removeDir();
        fm.copyDir(oldPath, newPath);
    });

    suite('Make path absolute', () => {
        test('Case got correct absolute path', () => {
            const absolutePath = '/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/FileManager/index.js';

            assert.strictEqual(fm.makePathAbsolute('test/FileManager/index.js'), absolutePath);
        });
    });

    suite('Write a file', () => {
        setup(() => fm.setDir('test/FileManager/notEmptyDir/'));

        test('Case file was written', () => {
            const expectedFile = {
                fileName: 'test.txt',
                filePath: 'test/FileManager/notEmptyDir/test.txt',
                content: 'This is test content'
            };

            fm.writeFile(expectedFile.filePath, expectedFile.content);

            const loadedFile = fm.readFile(expectedFile.fileName);

            assert.notStrictEqual(loadedFile.filePath.replace(expectedFile.filePath, ''), '');
            assert.strictEqual(loadedFile.fileName, expectedFile.fileName);
            assert.strictEqual(loadedFile.content, expectedFile.content);
        });

        teardown(() => fm.setDir(dir));
    });

    suite('Load a file', () => {
        test('Case file was loaded', () => {
            const expectedFile = {
                fileName: 't.svg',
                filePath: 'test/data/Base/t.svg'
            };
            const loadedFile = fm.loadFile(expectedFile.fileName);

            assert.notStrictEqual(loadedFile.filePath.replace(expectedFile.filePath, ''), '');
            assert.strictEqual(loadedFile.fileName, expectedFile.fileName);
        });
    });

    suite('Read a file', () => {
        test('Case file was read', () => {
            const expectedFile = {
                fileName: 't.svg',
                filePath: 'test/data/Base/t.svg',
                content: '<svg id="Tekolqiloli" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 200 200"><title>00078-t</title><path d="M118.47363,148.22266A61.89448,61.89448,0,0,1,104.419,112.7334c-1.13868-16.19629,3.96679-32.7583,13.65527-44.30225a50.35058,50.35058,0,0,1,6.03808-6.06543c-13.73437,1.28711-32.77587,3.2666-58.52636,6.30371l-1.17188-9.93115c68.43555-8.07226,89.32617-8.707,90.44141-8.73633a.1.1,0,0,1,.02051-.00049l.25,9.99708c-.15625.00488-17.53028.72851-29.3916,14.86132-8.06153,9.605-12.30079,23.50147-11.34083,37.17237a52.56362,52.56362,0,0,0,11.72657,29.74511Z"/></svg>'
            };
            const readFile = fm.readFile(expectedFile.fileName);

            assert.notStrictEqual(readFile.filePath.replace(expectedFile.filePath, ''), '');
            assert.strictEqual(readFile.fileName, expectedFile.fileName);
            assert.strictEqual(readFile.content, expectedFile.content);
            assert.strictEqual(readFile.content,
                expectedFile.content);
        });
    });

    suite('Copy and remove a file', () => {
        test('Case file was removed', () => {
            const path = 'test/data/Base/';
            const originalFile = 'test/data/Base/t.svg';
            const fileCopyName = path + 'copy.svg';

            fm.copyFile(originalFile, fileCopyName);
            fm.removeFile(fileCopyName);

            try {
                fm.readFile(fileCopyName);
            } catch (e) {
                assert.notStrictEqual(e, undefined);
                assert.notStrictEqual(e, null);
                assert.strictEqual(e.errno, -2);
            }
        });
    });

    suite('Load, copy and empty folder contents', () => {
        test('All contents were loaded and copied', () => {
            const oldPath = fm.getDir();
            const oldFiles = fm.loadDir();
            const newPath = oldPath.replace('data/', 'test/data') + "_copy";

            fm.copyDir(oldPath, newPath);
            fm.setDir(newPath);

            const copiedFiles = fm.loadDir();

            assert.strictEqual(copiedFiles.length, oldFiles.length);
            fm.removeDirContents(fm.getDir());

            const deletedFiles = fm.loadDir();

            assert.strictEqual(deletedFiles.length, 0);
        });
    });

    suite('Get zero fill string', () => {
        test('Case: 8', () => assert.strictEqual(fm.getZeroFillString(8), '00008'));
        test('Case: 17', () => assert.strictEqual(fm.getZeroFillString(17), '00017'));
        test('Case: 157', () => assert.strictEqual(fm.getZeroFillString(157), '00157'));
        test('Case: 2157', () => assert.strictEqual(fm.getZeroFillString(2157), '02157'));
        test('Case: 32157', () => assert.strictEqual(fm.getZeroFillString(32157), '32157'));
    });

    suite('Find whether directory is empty', () => {
        test('Case directory is empty', () => {
            const emptyDir = 'test/FileManager/emptyDir';

            fm.setDir(emptyDir);
            assert.strictEqual(fm.isDirEmpty(fm.getDir()), true);
        });

        test('Case directory is not empty', () => {
            fm.setDir(dir);

            assert.strictEqual(fm.isDirEmpty(fm.getDir()), false)
        });
    });

    suiteTeardown(() => {
        fm.setDir('/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/');
        fm.removeDir()
    });
});
