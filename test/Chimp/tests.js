const assert = require('chai').assert;
const Chimp = require('../../src/Chimp');
let dir = 'data/Base';
const chimp = new Chimp(dir);

suite('Chimp tests', () => {
    suiteSetup(() => {
        let oldPath = chimp.getDir();
        let newPath = `${oldPath.replace('data/Base', 'test/data/Chimp')}`;

        chimp.makeDir(newPath);
        chimp.setDir(newPath);
        chimp.copyDir(oldPath, newPath);

        oldPath = chimp.getDir();
        newPath = `${oldPath.replace('Chimp', 'ChimpCombinations')}`;

        chimp.makeDir(newPath);
        chimp.setDir(newPath);
        chimp.copyDir(oldPath, newPath);
    });

    suite('Form syllable character', () => {
        test('Case t_o!_tl', () => {
            const combination = {
                result: 't_o!_tl.svg',
                components: [
                    't.svg',
                    'tl.svg',
                    '_!3.svg',
                    '_o_3.svg'
                ]
            };
            const resultFile = chimp.formCharacterCombination(combination);

            assert.strictEqual(resultFile.fileName, 't_o!_tl.svg');
        });

        test('Case t syllables', () => {
            const sets = [
                {
                    directory: '001 Akaqilotontli',
                    combinations: [
                        {
                            name: 't_a!',
                            components: [
                                't.svg',
                                '!3.svg'
                            ]
                        },
                        {
                            name: 't_a_m',
                            components: [
                                't_.svg',
                                '_m.svg'
                            ]
                        },
                        {
                            name: 't_a!_m',
                            components: [
                                't_.svg',
                                '_m.svg',
                                '_!3.svg'
                            ]
                        },
                        {
                            name: 't_a_n',
                            components: [
                                't_.svg',
                                '_n.svg'
                            ]
                        },
                        {
                            name: 't_a!_n',
                            components: [
                                't_.svg',
                                '_n.svg',
                                '_!3.svg'
                            ]
                        },
                        {
                            name: 't_a_p',
                            components: [
                                't_.svg',
                                '_p.svg'
                            ]
                        },
                        {
                            name: 't_a!_p',
                            components: [
                                't_.svg',
                                '_p.svg',
                                '_!3.svg'
                            ]
                        },
                        {
                            name: 't_a_t',
                            components: [
                                't_.svg',
                                '_t.svg'
                            ]
                        },
                        {
                            name: 't_a!_t',
                            components: [
                                't_.svg',
                                '_t.svg',
                                '_!3.svg'
                            ]
                        },
                        {
                            name: 't_a_tl',
                            components: [
                                't_.svg',
                                '_tl.svg'
                            ]
                        },
                        {
                            name: 't_a!_tl',
                            components: [
                                't_.svg',
                                '_tl.svg',
                                '_!3.svg'
                            ]
                        },
                        {
                            name: 't_a_x',
                            components: [
                                't_.svg',
                                '_x.svg'
                            ]
                        },
                        {
                            name: 't_a!_x',
                            components: [
                                't_.svg',
                                '_x.svg',
                                '_!3.svg'
                            ]
                        }
                    ]
                }
            ];
            const results = chimp.formCharacterCombinations('e', sets);

            assert.strictEqual(results.length, sets[0].combinations.length);
        });
    });

    suiteTeardown(() => {
        chimp.setDir('/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/');
        chimp.removeDir()
    });
});
