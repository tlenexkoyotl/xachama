const assert = require('chai').assert;
const Simp = require('../../src/Simp');
let dir = 'data/Base';
const simp = new Simp(dir);

suite('Simp tests', () => {
    suiteSetup(() => {
        const oldPath = simp.getDir();
        const newPath = oldPath.replace('data/Base', 'test/data/Base');

        simp.setDir(newPath);
        simp.removeDir();
        simp.copyDir(oldPath, newPath);
    });

    suite('File optimization', () => {
        test('Case file is optimized', () => {
            const fileName = 't.svg';
            const oldFile = simp.readFile(fileName);

            assert.isAbove(oldFile.stat.size, 500);

            const svgFile = simp.loadAndOptimizeFile(fileName, (element) => {
                element.attributes.id = fileName.replace(simp.svg.getFileFilter(), '');
            });

            assert.isAtLeast(svgFile.stat.size, 434);
            assert.isAtMost(svgFile.stat.size, 900);
        });

        test('Case files are optimized', () => {
            const path = 'test/data/Base';
            const file = 't.svg';
            const originalFile = `${path}/${file}`;
            const fileCopyName = `${path}/t_copy.svg`;

            simp.copyFile(originalFile, fileCopyName);

            dir = dir.replace('data/Base', 'test/data/Base');

            simp.setDir(dir);

            const unoptimizedFiles = simp.readDir();

            for (const unoptimizedFile of unoptimizedFiles)
                assert.isAbove(unoptimizedFile.stat.size, 80);

            let i = 0;
            const optimizedFiles = simp.optimizeFiles((element) => {
                element.attributes.id = unoptimizedFiles[i++].fileName.replace(simp.svg.getFileFilter(), '')
                    .replace(/\s/g, '-');
            });

            for (const optimizedFile of optimizedFiles) {
                assert.isAtLeast(optimizedFile.stat.size, 50);
                assert.isAtMost(optimizedFile.stat.size, 3000);
            }
        });
    });

    suite('Merge pictures', () => {
        test('Case two pictures merged', () => {
            const resultFileName = '042-t_a_m.svg';
            const fileName1 = 't_.svg';
            const fileName2 = '_m.svg';
            const expectedFile = {
                content: '<svg data-name="Tekolqiloli akopa" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 200 200"><title>t_a_m</title><path d="M116,93.50586a39.9167,39.9167,0,0,1-.31348-55.90674q.31788-.3252.64063-.63916C106.126,37.813,92.75,39.03369,75.50293,40.77734l-1.00586-9.94921c53.61523-5.42042,69.73633-5.813,70.39648-5.82715l.21387,9.998c-.11523.00342-13.35254.48291-22.27637,9.59619a30.10725,30.10725,0,0,0-8.30957,23.251,30.79466,30.79466,0,0,0,8.60743,18.64795Z"/><path d="M135.88184,175c-6.417.001-10.35547-4.62207-12.043-6.60254-3.01953-3.53223-4.50879-8.90234-5.12989-11.80371-2-.0918-3.95214-.1582-5.916-.16211-3.92774.002-7.833.124-11.60645.39551-2.624.1875-5.26416.31738-8.05908.45312-1.86914.0918-3.76416.18457-5.67236.29688l-2.11963.124c-3.29492.19238-6.70215.39063-9.92236.65821l-.82618-9.9668c3.34229-.27637,6.812-.47852,10.167-.67383l2.11035-.124c1.94336-.11426,3.87256-.209,5.7749-.30176,2.73682-.13379,5.32178-.26074,7.83154-.44043,4.01856-.28809,8.18067-.41016,12.34278-.4209,1.62011.00293,3.2373.0459,4.83984.1084-.17969-3.39746-.21875-6.81543-.13086-10.59863.23535-10.0752,3.03223-26.127,3.15137-26.80469l9.84961,1.72656c-.02735.15918-2.78614,15.99121-3.00293,25.31055-.09278,3.95117-.042,7.38476.1748,10.85547a69.00526,69.00526,0,0,1,10.583,1.42675c2.10449.46583,7.71582,1.70508,10.41113,6.77149,3.15821,5.93652-.32715,12.10547-.73535,12.78906A14.19563,14.19563,0,0,1,136.00488,175Zm-6.69825-17.82617a14.38822,14.38822,0,0,0,2.26172,4.73144c1.39942,1.64258,2.76758,3.124,4.46875,3.09473a4.3173,4.3173,0,0,0,3.44824-2.10059c.34961-.62011.89844-2.22363.499-2.97461-.24121-.45312-1.08789-1.11718-3.749-1.70605A54.76087,54.76087,0,0,0,129.18359,157.17383Z"/></svg>'
            };

            const resultFile = simp.mergeTwoPictures(resultFileName, fileName1, fileName2);

            assert.strictEqual(resultFile.content, expectedFile.content);
        });

        test('Case four pictures merged', () => {
            const fileName1 = 't_.svg';
            const fileName2 = '_m.svg';
            const fileName3 = '_!3.svg';
            const fileName4 = '_e_5.svg';
            const resultFileName = '043-t__e__!_m.svg';
            const expectedFile = {
                content: '<svg data-name="Elinqilotontli akopa 5" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 200 200"><title>t__e__!_m</title><path d="M75.22656,56.23193A35.70909,35.70909,0,0,1,63.0835,50.46729,29.078,29.078,0,0,1,55.02,41.48242l5.2168-2.96484A23.11436,23.11436,0,0,0,66.65723,45.648a29.75223,29.75223,0,0,0,10.11621,4.78711Z"/><path d="M61.59863,62.89893a35.717,35.717,0,0,1-12.144-5.76465,29.0655,29.0655,0,0,1-8.063-8.98584l5.2168-2.96387a23.10689,23.10689,0,0,0,6.42041,7.13037,29.71958,29.71958,0,0,0,10.11572,4.78613Z"/><rect x="90.5" y="14" width="43" height="6"/><path d="M116,93.50586a39.9167,39.9167,0,0,1-.31348-55.90674q.31788-.3252.64063-.63916C106.126,37.813,92.75,39.03369,75.50293,40.77734l-1.00586-9.94921c53.61523-5.42042,69.73633-5.813,70.39648-5.82715l.21387,9.998c-.11523.00342-13.35254.48291-22.27637,9.59619a30.10725,30.10725,0,0,0-8.30957,23.251,30.79466,30.79466,0,0,0,8.60743,18.64795Z"/><path d="M135.88184,175c-6.417.001-10.35547-4.62207-12.043-6.60254-3.01953-3.53223-4.50879-8.90234-5.12989-11.80371-2-.0918-3.95214-.1582-5.916-.16211-3.92774.002-7.833.124-11.60645.39551-2.624.1875-5.26416.31738-8.05908.45312-1.86914.0918-3.76416.18457-5.67236.29688l-2.11963.124c-3.29492.19238-6.70215.39063-9.92236.65821l-.82618-9.9668c3.34229-.27637,6.812-.47852,10.167-.67383l2.11035-.124c1.94336-.11426,3.87256-.209,5.7749-.30176,2.73682-.13379,5.32178-.26074,7.83154-.44043,4.01856-.28809,8.18067-.41016,12.34278-.4209,1.62011.00293,3.2373.0459,4.83984.1084-.17969-3.39746-.21875-6.81543-.13086-10.59863.23535-10.0752,3.03223-26.127,3.15137-26.80469l9.84961,1.72656c-.02735.15918-2.78614,15.99121-3.00293,25.31055-.09278,3.95117-.042,7.38476.1748,10.85547a69.00526,69.00526,0,0,1,10.583,1.42675c2.10449.46583,7.71582,1.70508,10.41113,6.77149,3.15821,5.93652-.32715,12.10547-.73535,12.78906A14.19563,14.19563,0,0,1,136.00488,175Zm-6.69825-17.82617a14.38822,14.38822,0,0,0,2.26172,4.73144c1.39942,1.64258,2.76758,3.124,4.46875,3.09473a4.3173,4.3173,0,0,0,3.44824-2.10059c.34961-.62011.89844-2.22363.499-2.97461-.24121-.45312-1.08789-1.11718-3.749-1.70605A54.76087,54.76087,0,0,0,129.18359,157.17383Z"/></svg>'
            };

            const resultFile = simp.mergePictures(resultFileName,
                fileName1,
                fileName2,
                fileName3,
                fileName4);

            assert.isAtLeast(resultFile.stat.size, 1200);
            assert.isAtMost(resultFile.stat.size, 2500);
            assert.strictEqual(resultFile.content, expectedFile.content);
        });
    });

    suiteTeardown(() => {
        simp.setDir('/media/rashidtn1419/DATA/Documents/Programming/Work/Atlatpix/xachama/test/data/');
        simp.removeDir()
    });
});
