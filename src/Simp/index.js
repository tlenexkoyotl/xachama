const Svg = require('../Svg');
const FileManager = require('../FileManager');

// Simp: SVG image manipulation program
module.exports = class Simp extends FileManager {
    constructor(dir) {
        super(dir);

        this.svg = new Svg();
    }

    loadAndOptimizeFile(fileName, callback = () => {
    }) {
        const file = this.readFile(fileName);

        return this.optimizeFile(file, callback);
    }

    writeSvg(file) {
        this.writeFile(file.filePath, this.svg.stringifySvg(file.content));

        return this.readFile(file.fileName);
    }

    optimizeFile(file, callback = () => {
    }) {
        file.content = this.svg.optimizeFile(file.content, callback);

        return this.writeSvg(file);
    }

    optimizeFiles(callback = () => {
    }) {
        const files = this.readDir();

        for (let i in files)
            files[i] = this.optimizeFile(files[i], callback);

        return files;
    }

    formResultName(resultFileName) {
        if (resultFileName !== '_')
            if (!/^_+/g.test(resultFileName) && !/_+.svg$/g.test(resultFileName))
                return resultFileName.replace(/_/g, '');

        return resultFileName;
    }

    removeDir(resultName) {
        resultName = this.formResultName(resultName).split('/');

        return resultName[resultName.length - 1];
    }

    formId(resultName) {
        return this.removeDir(resultName).replace('.svg', '');
    }

    removeStrokeColor(style) {
        return style.replace(/stroke:#[0-9abcdef]{1,6};/g, '');
    }

    removeChildrenStrokeColors(content) {
        for (const child of content.children)
            if (child.name !== 'title')
                if (child.attributes.style)
                    child.attributes.style = this.removeStrokeColor(child.attributes.style);
    }

    optimizePicture(resultFileName, resultFile) {
        delete resultFile.content.attributes.id;
        delete resultFile.content.attributes['data-name'];

        resultFile.content.attributes.id = this.formId(resultFileName);
        resultFile.content.attributes['data-name'] = resultFile.content.attributes.id;
        resultFile.content.children = [
            this.svg.getTitleElement(resultFile.content.attributes.id),
            ...resultFile.content.children
        ];

        this.removeChildrenStrokeColors(resultFile.content);

        return this.writeSvg(resultFile);
    }

    getPlainContent(fileName) {
        const file = this.readFile(fileName);
        file.content = this.svg.getPlainContent(file.content);

        return file;
    }

    mergeTwoPictures(resultFileName, fileName1, fileName2) {
        const file1 = this.getPlainContent(fileName1);
        const file2 = this.getPlainContent(fileName2);
        const resultFile = this.formFile(resultFileName);
        resultFile.content = Object.assign({}, file1.content);
        resultFile.content.children = resultFile.content.children.concat(file2.content.children);

        return this.optimizePicture(resultFileName, resultFile);
    }

    mergePictures(resultFileName, ...fileNames) {
        let resultFile;

        for (const i in fileNames)
            if (fileNames.length > 1) {
                if (parseInt(i) === 0)
                    resultFile = this.mergeTwoPictures(resultFileName,
                        fileNames[i],
                        fileNames[parseInt(i) + 1]);
                else if (fileNames[parseInt(i) + 1])
                    resultFile = this.mergeTwoPictures(resultFile.fileName,
                        resultFile.fileName,
                        fileNames[parseInt(i) + 1]);
            } else
                resultFile = this.optimizePicture(resultFileName,
                    this.getPlainContent(fileNames[i]));

        return resultFile;
    }
};
