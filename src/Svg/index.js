const svgson = require('svgson');

module.exports = class Svg {
    constructor() {
        let fileFilter = /\.svg$/;
        this.svgson = svgson;
        this.elements = {
            title: 'title',
            style: 'style',
            g: 'g'
        };

        this.getFileFilter = () => fileFilter;
    }

    parseSvg(content) {
        return this.svgson.parseSync(content);
    }

    stringifySvg(svgson) {
        return this.svgson.stringify(svgson);
    }

    isTitle(elementName) {
        return elementName === this.elements.title;
    }

    isStyle(elementName) {
        return elementName === this.elements.style;
    }

    isG(elementName) {
        return elementName === this.elements.g;
    }

    getTitleElement(title) {
        return {
            name: 'title',
            type: 'element',
            value: '',
            attributes: {},
            children: [
                {
                    name: '',
                    type: 'text',
                    value: title,
                    attributes: {},
                    children: []
                }
            ]
        };
    }

    optimizeFile(content) {
        const svg = this.parseSvg(content);

        svg.children = svg.children.filter(child => {
            if (this.isTitle(child.name))
                return false;
            else if (this.isStyle(child.name))
                return false;
            else if (this.isG(child.name))
                return false;
            else
                return true;
        });

        return svg;
    }

    getPlainContent(svgContent) {
        svgContent = this.optimizeFile(svgContent);

        return svgContent;
    }
};
