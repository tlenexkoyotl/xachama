const fsx = require('fs-extra');
const path = require('path');

module.exports = class FileManager {
    constructor(dir) {
        this.fsx = fsx;
        this.path = path;
        let _dir;
        this.getDir = () => _dir;
        this.setDir = dir => _dir = dir !== undefined ? this.makePathAbsolute(dir) : __dirname;
        _dir = this.setDir(dir);
    }

    makePathAbsolute(relativePath = this.getDir()) {
        return this.path.resolve(relativePath);
    }

    makeDir(dir) {
        if (!this.fsx.existsSync(dir))
            this.fsx.mkdirpSync(dir);
    }

    writeFile(filePath, content) {
        this.fsx.writeFileSync(filePath, content);
    }

    formFile(fileName) {
        const file = {fileName: fileName};
        file.filePath = this.path.join(this.getDir(), fileName);

        return file;
    }

    loadFile(fileName) {
        const loadedFile = this.formFile(fileName);
        loadedFile.stat = this.fsx.lstatSync(loadedFile.filePath);

        return loadedFile;
    }

    readFile(fileName) {
        const loadedFile = this.loadFile(fileName);
        loadedFile.content = this.fsx.readFileSync(loadedFile.filePath, 'utf8');

        return loadedFile;
    }

    copyFile(source, target) {
        this.fsx.copyFileSync(this.makePathAbsolute(source), this.makePathAbsolute(target));
    }

    removeFile(fileName) {
        this.fsx.unlinkSync(this.makePathAbsolute(fileName));
    }

    loadDir() {
        return this.fsx.readdirSync(this.getDir());
    }

    readDir() {
        const loadedFiles = this.loadDir();
        let readFiles = [];

        for (const file of loadedFiles) {
            readFiles = [...readFiles, this.readFile(file)];
        }

        return readFiles;
    }

    copyDir(source = this.getDir(), target = this.getDir() + '_copy') {
        this.fsx.copySync(this.makePathAbsolute(source), this.makePathAbsolute(target));
    }

    removeDirContents(folder = this.getDir()) {
        this.fsx.emptyDirSync(this.makePathAbsolute(folder));
    }

    removeDir(folder = this.getDir()) {
        this.removeDirContents(folder);

        if (this.isDirEmpty()) this.removeDirContents(folder);
    }

    getZeroFillString(index = 0) {
        if (index < 10) {
            return `0000${index}`;
        } else if (index >= 10 && index < 100) {
            return `000${index}`;
        } else if (index >= 100 && index < 1000) {
            return `00${index}`;
        } else if (index >= 1000 && index < 10000) {
            return `0${index}`;
        } else if (index >= 10000 && index < 100000) {
            return `${index}`;
        }
    }

    isDirEmpty(searchPath = this.getDir()) {
        let stat;

        try {
            stat = this.fsx.statSync(searchPath);
        } catch (e) {
            return true;
        }

        if (stat.isDirectory()) {
            const items = this.loadDir(searchPath);

            return !items || !items.length;
        } else {
            const file = this.readFile(searchPath);

            return !file || !file.length;
        }
    }
};
