const Chimp = require('../Chimp');

// Xachama: Xamix char maker
module.exports = class Xachama extends Chimp {
    constructor(dir) {
        super(dir);

        this.types = {
            all: [
                'a',
                'a_',
                '_a',
                '_a_',
                'o',
                'o_',
                '_o',
                '_o_',
                'i',
                'i_',
                '_i',
                '_i_',
                'e',
                'e_',
                '_e',
                '_e_',
                '\'',
                '!',
                '_!',
                'm',
                'm_',
                '_m',
                'n',
                'n_',
                '_n',
                'p',
                'p_',
                '_p',
                't',
                't_',
                '_t',
                'tl',
                'tl_',
                '_tl',
                'x',
                'x_',
                '_x',
                'c',
                'c_',
                '_c',
                's',
                's_',
                '_s',
                'z',
                'z_',
                '_z',
                'k',
                'k_',
                '_k',
                'q',
                'q_',
                '_q',
                'h',
                'h_',
                '_h',
                'l',
                'l_',
                '_l',
                'w',
                'w_',
                '_w',
                'y',
                'y_',
                '_y',
                '~',
                'b',
                'b_',
                '_b',
                'd',
                'd_',
                '_d',
                'f',
                'f_',
                '_f',
                'g',
                'g_',
                '_g',
                'kh',
                'kh_',
                '_kh',
                'j',
                'j_',
                '_j',
                'r',
                'r_',
                '_r',
                'z\'',
                'z\'_',
                '_z\'',
                'u',
                'u_',
                '_u',
                '_u_',
                'v',
                'v_',
                '_v',
                'ks',
                'ks_',
                '_ks',
                'ny',
                'ny_',
                '_ny'
            ],
            largeStandaloneVowel: [
                'a',
                'o',
                'i',
                'e'
            ],
            largeCombiningVowel: [
                '_a',
                '_o',
                '_i',
                '_e'
            ],
            upperStandaloneVowel: [
                'a_',
                'o_',
                'i_',
                'e_'
            ],
            upperCombiningVowel: [
                '_a_',
                '_o_',
                '_i_',
                '_e_'
            ],
            largeSkip: '\'',
            largeAccent: '!',
            upperAccent: '_!',
            largeConsonant: [
                'm',
                'n',
                'p',
                't',
                'tl',
                'x',
                'c',
                's',
                'z',
                'k',
                'q',
                'h',
                'l',
                'w',
                'y'
            ],
            upperConsonant: [
                'm_',
                'n_',
                'p_',
                't_',
                'tl_',
                'x_',
                'c_',
                's_',
                'z_',
                'k_',
                'q_',
                'h_',
                'l_',
                'w_',
                'y_'
            ],
            lowerConsonant: [
                '_m',
                '_n',
                '_p',
                '_t',
                '_tl',
                '_x',
                '_c',
                '_s',
                '_z',
                '_k',
                '_q',
                '_h',
                '_l',
                '_w',
                '_y'
            ],
            muteSign: '~',
            largeStandaloneU: 'u',
            largeCombiningU: '_u',
            upperStandaloneU: 'u_',
            upperCombiningU: '_u_',
            largeForeignConsonant: [
                'b',
                'd',
                'f',
                'g',
                'kh',
                'j',
                'r',
                'z\'',
                'v',
                'ks',
                'ny'
            ],
            upperForeignConsonant: [
                'b_',
                'd_',
                'f_',
                'g_',
                'kh_',
                'j_',
                'r_',
                'z\'_',
                'v_',
                'ks_',
                'ny_'
            ],
            lowerForeignConsonant: [
                '_b',
                '_d',
                '_f',
                '_g',
                '_kh',
                '_j',
                '_r',
                '_z\'',
                '_v',
                '_ks',
                '_ny'
            ]
        };
        this.types.vowel = [
            ...this.types.largeStandaloneVowel,
            ...this.types.largeCombiningVowel,
            ...this.types.upperStandaloneVowel,
            ...this.types.upperCombiningVowel,
            this.types.largeStandaloneU,
            this.types.largeCombiningU,
            this.types.upperStandaloneU,
            this.types.upperCombiningU
        ];
        this.types.consonant = [
            ...this.types.largeConsonant,
            ...this.types.upperConsonant,
            ...this.types.lowerConsonant,
            ...this.types.largeForeignConsonant,
            ...this.types.upperForeignConsonant,
            ...this.types.lowerForeignConsonant
        ];
        this.types.diacritic = [
            this.types.largeSkip,
            this.types.largeAccent,
            this.types.upperAccent,
            this.types.muteSign
        ];
        this.types.vowelSetU = [
            this.types.largeCombiningU,
            this.types.upperStandaloneU,
            this.types.upperCombiningU
        ];
        this.vowelDirs = [
            'a',
            'o',
            'i',
            'e'
        ];
        this.tiers = {
            baseVowel: [
                {
                    name: '1',
                    elements: [
                        'm',
                        'n',
                        'ny'
                    ]
                },
                {
                    name: '2',
                    elements: [
                        'p',
                        'tl',
                        'c',
                        's',
                        'z',
                        'k',
                        'q',
                        'w',
                        'b',
                        'f',
                        'g',
                        'z\'',
                        'v',
                        'ks'
                    ]
                },
                {
                    name: '3',
                    elements: [
                        't'
                    ]
                },
                {
                    name: '4',
                    elements: [
                        'x',
                        'y',
                        'd',
                        'j'
                    ]
                },
                {
                    name: '5',
                    elements: [
                        'h',
                        'l',
                        'kh',
                        'r'
                    ]
                }
            ],
            skip: [
                {
                    name: '1',
                    elements: [
                        'a',
                        'm',
                        'n',
                        'u',
                        'ny'
                    ]
                },
                {
                    name: '2a',
                    elements: [
                        'o',
                        'p',
                        'c',
                        's',
                        'k',
                        'h',
                        'f',
                        'g',
                        'kh'
                    ]
                },
                {
                    name: '2b',
                    elements: [
                        'p',
                        'c',
                        's',
                        'k',
                        'f',
                        'g'
                    ]
                },
                {
                    name: '3',
                    elements: [
                        'i',
                        'l',
                        'r'
                    ]
                },
                {
                    name: '4',
                    elements: [
                        'e'
                    ]
                },
                {
                    name: '5',
                    elements: [
                        't',
                        'tl',
                        'z',
                        'q',
                        'w',
                        'b',
                        'z\'',
                        'v',
                        'ks'
                    ]
                },
                {
                    name: '6a',
                    elements: [
                        'x',
                        'y',
                        'd',
                        'j'
                    ]
                },
                {
                    name: '6b',
                    elements: [
                        'x',
                        'y',
                        'd',
                        'j',
                    ]
                }
            ],
            accent: [
                {
                    name: '1',
                    elements: [
                        'm',
                        's',
                        'b'
                    ]
                },
                {
                    name: '2',
                    elements: [
                        'n',
                        'w',
                        'v',
                        'ny'
                    ]
                },
                {
                    name: '3',
                    elements: [
                        'e',
                        'p',
                        't',
                        'k',
                        'q',
                        'h',
                        'd',
                        'f',
                        'g',
                        'kh',
                        'r'
                    ]
                },
                {
                    name: '4',
                    elements: [
                        'a',
                        'tl',
                        'u'
                    ]
                },
                {
                    name: '5',
                    elements: [
                        'o',
                        'i',
                        'x',
                        'y',
                        'j'
                    ]
                },
                {
                    name: '6',
                    elements: [
                        'c',
                        'l',
                        'r'
                    ]
                },
                {
                    name: '7',
                    elements: [
                        'z',
                        'ks',
                        'z\''
                    ]
                }
            ],
            akopaVowel: [
                {
                    name: '1',
                    elements: [
                        'm_',
                        'n_',
                        'ny_'
                    ]
                },
                {
                    name: '2',
                    elements: [
                        'p_',
                        'c_',
                        'k_',
                        'f_',
                        'g_'
                    ]
                },
                {
                    name: '3',
                    elements: [
                        'tl_',
                        's_',
                        'z_',
                        'q_',
                        'w_',
                        'b_',
                        'z\'_',
                        'v_',
                        'ks_'
                    ]
                },
                {
                    name: '4',
                    elements: [
                        't_'
                    ]
                },
                {
                    name: '5',
                    elements: [
                        'x_',
                        'y_',
                        'j_'
                    ]
                },
                {
                    name: '6',
                    elements: [
                        'h_',
                        'l_',
                        'kh_',
                        'r_'
                    ]
                },
                {
                    name: '7',
                    elements: [
                        'd_'
                    ]
                }
            ],
            akopaAccent: [
                {
                    name: '1',
                    elements: [
                        'm',
                        's',
                        'b'
                    ]
                },
                {
                    name: '2',
                    elements: [
                        'n',
                        'w',
                        'v',
                        'ny'
                    ]
                },
                {
                    name: '3',
                    elements: [
                        'e',
                        'p',
                        't',
                        'k',
                        'q',
                        'h',
                        'd',
                        'f',
                        'g',
                        'kh',
                        'r'
                    ]
                },
                {
                    name: '4',
                    elements: [
                        'a',
                        'tl',
                        'u'
                    ]
                },
                {
                    name: '5',
                    elements: [
                        'o',
                        'i',
                        'x',
                        'y',
                        'j'
                    ]
                },
                {
                    name: '6',
                    elements: [
                        'c',
                        'l',
                        'r'
                    ]
                },
                {
                    name: '7',
                    elements: [
                        'z',
                        'ks',
                        'z\''
                    ]
                }
            ]
        };
        this.silentDir = '~';
        this.uDir = 'u';
        this.allLetters = [
            ...this.types.largeStandaloneVowel,
            ...this.types.largeConsonant,
            ...this.types.largeForeignConsonant,
            ...this.types.largeStandaloneU
        ];
    }

    isVowel(charName) {
        return this.types.vowel.indexOf(charName) !== -1;
    }

    isConsonant(charName) {
        return this.types.consonant.indexOf(charName) !== -1;
    }

    formCombination(name, components) {
        return {
            name: name,
            components: components.map(component => `${component}.svg`)
        };
    }

    addCombinationToSet(set, combinationName, ...components) {
        return [...set, this.formCombination(combinationName, components)];
    }

    formLargeStandaloneVowelSets(vowelName) {
        let set = this.addCombinationToSet([],
            vowelName,
            vowelName);

        let combinationName = `${vowelName}${this.types.largeSkip}`;
        set = this.addCombinationToSet(set,
            combinationName,
            vowelName,
            this.types.largeSkip);

        combinationName = `${vowelName}${this.types.largeAccent}`;
        set = this.addCombinationToSet(set,
            combinationName,
            vowelName,
            this.types.largeAccent);

        combinationName = `${vowelName}` +
            `${this.types.largeAccent}` +
            `${this.types.largeSkip}`;
        set = this.addCombinationToSet(set,
            combinationName,
            vowelName,
            this.types.largeAccent,
            this.types.largeSkip);

        return set;
    }

    formUpperStandaloneVowelSetsWithLowerConsonants(vowelName, lowerConsonants) {
        let combinationName;
        let set = [];
        vowelName = `${vowelName}_`;

        for (const lowerConsonant of lowerConsonants) {
            combinationName = `${vowelName}${lowerConsonant}`;
            set = this.addCombinationToSet(set,
                combinationName,
                vowelName,
                lowerConsonant);

            combinationName = `${vowelName}${this.types.upperAccent}${lowerConsonant}`;
            set = this.addCombinationToSet(set,
                combinationName,
                vowelName,
                this.types.upperAccent,
                lowerConsonant);
        }

        return set;
    }

    formUpperStandaloneVowelSets(vowelName) {
        return this.formUpperStandaloneVowelSetsWithLowerConsonants(vowelName,
            this.types.lowerConsonant);
    }

    formUpperStandaloneVowelSetsWithForeignConsonants(vowelName) {
        return this.formUpperStandaloneVowelSetsWithLowerConsonants(vowelName,
            this.types.lowerForeignConsonant);
    }

    formVowelSets(vowelName) {
        vowelName = vowelName.toLowerCase();
        let set = this.formLargeStandaloneVowelSets(vowelName);
        set = [...set, ...this.formUpperStandaloneVowelSets(vowelName)];
        set = [...set,
            ...this.formUpperStandaloneVowelSetsWithForeignConsonants(vowelName)];

        this.suffixSubsetCombinations(vowelName, '', set);

        return set;
    }

    formLargeStandaloneConsonantSets(consonantName, vowelName) {
        let combinationName;
        let set = [];
        consonantName = consonantName.toLowerCase();
        vowelName = vowelName.toLowerCase();

        combinationName = `${consonantName}${vowelName}`;
        set = this.addCombinationToSet(set,
            combinationName,
            consonantName,
            vowelName);

        combinationName = `${consonantName}${vowelName}${this.types.largeSkip}`;
        set = this.addCombinationToSet(set,
            combinationName,
            consonantName,
            vowelName,
            this.types.largeSkip);

        combinationName = `${consonantName}${vowelName}${this.types.largeAccent}`;
        set = this.addCombinationToSet(set,
            combinationName,
            consonantName,
            vowelName,
            this.types.largeAccent);

        combinationName = `${consonantName}` +
            `${vowelName}` +
            `${this.types.largeAccent}` +
            `${this.types.largeSkip}`;
        set = this.addCombinationToSet(set,
            combinationName,
            consonantName,
            vowelName,
            this.types.largeAccent,
            this.types.largeSkip);

        return set;
    }

    formUpperStandaloneConsonantSetsWithLowerConsonants(consonantName, vowelName, lowerConsonants) {
        let combinationName;
        let set = [];

        for (const lowerConsonant of lowerConsonants) {
            combinationName = `${consonantName}${vowelName}${lowerConsonant}`;
            set = this.addCombinationToSet(set,
                combinationName,
                consonantName,
                vowelName,
                lowerConsonant);

            combinationName = `${consonantName}${vowelName}${this.types.upperAccent}${lowerConsonant}`;
            set = this.addCombinationToSet(set,
                combinationName,
                consonantName,
                vowelName,
                this.types.upperAccent,
                lowerConsonant);
        }

        return set;
    }

    formUpperStandaloneConsonantSets(consonantName, vowelName) {
        return this.formUpperStandaloneConsonantSetsWithLowerConsonants(consonantName,
            vowelName,
            this.types.lowerConsonant);
    }

    formUpperStandaloneConsonantSetsWithForeignConsonants(consonantName, vowelName) {
        return this.formUpperStandaloneConsonantSetsWithLowerConsonants(consonantName,
            vowelName,
            this.types.lowerForeignConsonant);
    }

    getCharSetElement(primeSet, subSet, charName) {
        const index = primeSet.indexOf(charName);

        return subSet[index];
    }

    getVowelSetElement(vowelSet, vowelName) {
        return this.getCharSetElement(this.types.largeStandaloneVowel,
            vowelSet,
            vowelName);
    }

    getLargeCombiningVowel(vowelName) {
        return this.getVowelSetElement(this.types.largeCombiningVowel, vowelName);
    }

    getUpperStandaloneVowel(vowelName) {
        return this.getVowelSetElement(this.types.upperStandaloneVowel, vowelName);
    }

    getUpperCombiningVowel(vowelName) {
        return this.getVowelSetElement(this.types.upperCombiningVowel, vowelName);
    }

    getVowelSet(vowelName) {
        return [
            this.getLargeCombiningVowel(vowelName),
            this.getUpperStandaloneVowel(vowelName),
            this.getUpperCombiningVowel(vowelName)
        ];
    }

    formVowelSetConsonantSets(consonantSet, vowelSet) {
        let set = this.formLargeStandaloneConsonantSets(consonantSet[0], vowelSet[0]);
        set = [...set, ...this.formUpperStandaloneConsonantSets(consonantSet[1], vowelSet[2])];
        set = [...set,
            ...this.formUpperStandaloneConsonantSetsWithForeignConsonants(consonantSet[1],
                vowelSet[2])
        ];

        return set;
    }

    getConsonantSet(consonantName) {
        return [
            consonantName,
            `${consonantName}_`,
            `_${consonantName}`
        ];
    }

    formConsonantSets(consonantName, vowelName) {
        consonantName = consonantName.toLowerCase();
        vowelName = vowelName.toLowerCase();
        let vowelSet = this.getVowelSet(vowelName);
        const subSet = this.formConsonantSetsGivenVowelSet(consonantName, vowelSet);

        this.suffixSubsetCombinations(consonantName, vowelName, subSet);

        return subSet;
    }

    formConsonantSetsGivenVowelSet(consonantName, vowelSet) {
        let consonantSet = this.getConsonantSet(consonantName);

        return this.formVowelSetConsonantSets(consonantSet, vowelSet);
    }

    formSubset(directory, combinations) {
        return {
            directory: directory,
            combinations: combinations
        };
    }

    getVowelDir(vowelName) {
        return this.getCharSetElement(this.types.largeStandaloneVowel,
            this.vowelDirs,
            vowelName);
    }

    suffixSubsetCombinations(letterName, combiner, combinations) {
        const a = new RegExp(`${this.types.largeStandaloneVowel[0]}`);

        for (const combination of combinations)
            for (let i in combination.components)
                if (i > 0) {
                    const component = combination.components[i].replace(/.svg/g, '');
                    const baseVowel = new RegExp(`_${combiner}`, 'g');
                    const silent = new RegExp(`~`, 'g');
                    const akopaVowel = new RegExp(`_${combiner}_`, 'g');
                    let suffix = '';

                    if (!a.test(component) && this.isConsonant(letterName)) {
                        if (baseVowel.test(component) || silent.test(component))
                            suffix = this.tiers.baseVowel.find(tier =>
                                tier.elements.includes(letterName)).name;

                        if (akopaVowel.test(component))
                            suffix = this.tiers.akopaVowel.find(tier =>
                                tier.elements.includes(`${letterName}_`)).name
                    }

                    if (/!/g.test(component))
                        suffix = this.tiers.accent.find(tier =>
                            tier.elements.includes(letterName)).name;

                    if (/_!/g.test(component))
                        suffix = this.tiers.akopaAccent.find(tier =>
                            tier.elements.includes(letterName)).name;

                    if (/^'/g.test(component))
                        if (combiner && !a.test(combiner)) {
                            suffix = this.tiers.skip.find(tier =>
                                tier.elements.includes(letterName)).name;
                        } else
                            suffix = this.tiers.skip.find(tier =>
                                tier.elements.includes(letterName)).name;

                    let index = this.types.largeStandaloneVowel.findIndex(vowel =>
                        new RegExp(`${vowel}`).test(combiner));

                    if (index > 0) {
                        index = this.tiers.skip.findIndex(tier =>
                            tier.elements.includes(letterName));

                        if (this.tiers.skip.slice(index).findIndex(tier =>
                            tier.elements.includes(letterName)) > -1)
                            suffix = suffix.replace('a', 'b');
                    }

                    combination.components[i] = `${component}${suffix}.svg`;
                }
    }

    formConsonantFullSets(consonantName) {
        consonantName = consonantName.toLowerCase();
        let set = [];
        let subSet;
        let directory;

        for (const largeStandaloneVowel of this.types.largeStandaloneVowel) {
            directory = this.getVowelDir(largeStandaloneVowel);
            subSet = this.formSubset(directory,
                this.formConsonantSets(consonantName, largeStandaloneVowel));

            set = [...set, subSet];
        }

        subSet = this.formSubset(this.silentDir,
            [this.formCombination(`${consonantName}~`, [consonantName, '~'])]);

        this.suffixSubsetCombinations(consonantName, '~', subSet.combinations);

        set = [...set, subSet];

        subSet = this.formSubset(this.uDir,
            this.formConsonantSetsGivenVowelSet(consonantName, this.types.vowelSetU));

        this.suffixSubsetCombinations(consonantName, 'u', subSet.combinations);

        set = [...set, subSet];

        return set;
    }

    formVowelFullSets(vowelName) {
        return [this.formSubset(vowelName, this.formVowelSets(vowelName))];
    }

    formAllCharacterSets(organize = true) {
        let sets;

        for (let letter of this.allLetters) {
            if (organize)
                this.makeDir(`${this.getDir()}/${letter}`);

            if (this.isVowel(letter))
                sets = this.formVowelFullSets(letter);
            else if (this.isConsonant(letter))
                sets = this.formConsonantFullSets(letter);

            this.formCharacterSets(letter, sets, this.isConsonant(letter), organize);
        }
    }

    formCharacterSets(letter, fullSets = [], subDir, organize) {
        return this.formCharacterCombinations(letter, fullSets, subDir, organize);
    }
};
