const Simp = require('../Simp');

// Chimp: Character image manipulation program
module.exports = class Chimp extends Simp {
    constructor(dir) {
        super(dir);
    }

    formCharacterCombination(combination) {
        return this.mergePictures(combination.result, ...combination.components);
    }

    formCombinationName(name, directory) {
        if (!directory)
            return `${name}.svg`;
        else
            return `${directory}/${name}.svg`;
    }

    formCharacterCombinations(letter, sets, subDir = false, organize = true) {
        let results = [];

        for (const set of sets) {
            let dir;

            if (organize) {
                dir = `${this.getDir()}/${letter}${subDir ? `/${set.directory}/` : ''}`;
                this.makeDir(dir);
            } else
                dir = `${this.getDir()}/`;

            for (const combination of set.combinations) {
                set.resultName = this.formCombinationName(combination.name,
                    dir.replace(this.makePathAbsolute(), ''));
                set.resultName = this.formResultName(set.resultName);
                results = [...results,
                    this.mergePictures(set.resultName, ...combination.components)];
            }
        }

        return results;
    }
};
